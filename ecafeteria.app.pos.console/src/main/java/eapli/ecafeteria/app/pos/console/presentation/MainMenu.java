/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.pos.console.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.components.authz.console.presentation.MyUserMenuBuilder;
import eapli.ecafeteria.app.pos.console.presentation.delivery.DeliverAMealUI;
import eapli.ecafeteria.app.pos.console.presentation.delivery.OpenDeliveryStationUI;
import eapli.ecafeteria.app.pos.console.presentation.sales.RechargeUserCardUI;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Component
public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";
    private static final String NOT_IMPLEMENTED_YET_LABEL = "Not implemented yet";

    private static final int EXIT_OPTION = 0;

    // SALES
    private static final int RECHARGE_USER_CARD_OPTION = 1;
    private static final int LAST_MINUTE_RESERVATION_OPTION = 2;

    // DELIVERY STATION
    private static final int OPEN_DELIVERY_STATION_OPTION = 1;
    private static final int DELIVERY_OPTION = 2;
    private static final int CLOSE_DELIVERY_STATION_OPTION = 3;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int SALES_OPTION = 7;
    private static final int DELIVERY_STATION_OPTION = 8;

    @Autowired
    private AuthorizationService authz;

    @Autowired
    private MyUserMenuBuilder myUserMenuBuilder;

    @Autowired
    private RechargeUserCardUI rechargeUserCardUI;

    @Autowired
    private OpenDeliveryStationUI openDeliveryStationUI;

    @Autowired
    private DeliverAMealUI deliverAMealUI;

    @Value("${eapli.ecafeteria.ui.menu.layout:horizontal}")
    private String menuLayout;

    private MenuRenderer renderer;

    @Override
    public boolean show() {
        final Menu menu = buildMainMenu();
        renderer = getRenderer(menu);
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        return renderer.render();
    }

    private Boolean isMenuLayoutHorizontal() {
        return "horizontal".equalsIgnoreCase(menuLayout);
    }

    private MenuRenderer getRenderer(final Menu menu) {
        final MenuRenderer theRenderer;
        if (isMenuLayoutHorizontal()) {
            theRenderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            theRenderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return theRenderer;
    }

    @Override
    public String headline() {
        return authz.session().map(s -> "eCAFETERIA [ @" + s.authenticatedUser().identity() + " ]")
                .orElseThrow(IllegalStateException::new);
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = myUserMenuBuilder.withRole(CafeteriaRoles.CASHIER);
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator("--------------"));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(CafeteriaRoles.CASHIER)) {
            // POS
            mainMenu.addSubMenu(SALES_OPTION, buildCashierMenu());
            // DELIVERY STATION
            mainMenu.addSubMenu(DELIVERY_STATION_OPTION, buildDeliveryStationMenu());
        }

        if (!isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator("--------------"));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildCashierMenu() {
        final Menu cashierMenu = new Menu("Sales  >");

        cashierMenu.addItem(RECHARGE_USER_CARD_OPTION, "Recharge user card Credit",
                rechargeUserCardUI::show);
        cashierMenu.addItem(LAST_MINUTE_RESERVATION_OPTION, "Last minute reservation meal ",
                new ShowMessageAction(NOT_IMPLEMENTED_YET_LABEL));

        cashierMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return cashierMenu;
    }

    private Menu buildDeliveryStationMenu() {
        final Menu deliveryMenu = new Menu("Delivery Station  >");

        deliveryMenu.addItem(OPEN_DELIVERY_STATION_OPTION, "Open Delivery Station",
                openDeliveryStationUI::show);
        deliveryMenu.addItem(DELIVERY_OPTION, "Deliver a meal (booking)", deliverAMealUI::show);
        deliveryMenu.addItem(CLOSE_DELIVERY_STATION_OPTION, "Close Delivery Station ",
                new ShowMessageAction(NOT_IMPLEMENTED_YET_LABEL));

        deliveryMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return deliveryMenu;
    }
}
