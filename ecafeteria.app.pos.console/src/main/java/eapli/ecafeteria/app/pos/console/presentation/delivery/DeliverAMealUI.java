package eapli.ecafeteria.app.pos.console.presentation.delivery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.pos.console.ECafeteriaPOS;
import eapli.ecafeteria.delivery.application.DeliverAMealController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class DeliverAMealUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(DeliverAMealUI.class);

    @Autowired
    private DeliverAMealController theController;

    @Override
    protected boolean doShow() {
        final String token = Console.readLine("Token:");
        try {
            theController.deliver(ECafeteriaPOS.session(), token);
        } catch (ConcurrencyException | IntegrityViolationException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Deliver a meal";
    }
}
