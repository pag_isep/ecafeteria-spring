/**
 *
 */
package eapli.ecafeteria.app.pos.console.presentation.delivery;

import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.framework.visitor.Visitor;

/**
 * @fixme this class is duplicated of
 *        eapli.ecafeteria.app.backoffice.console.presentation.cafeteria
 * @author Paulo Gandra Sousa
 *
 */
@SuppressWarnings({ "squid:S106" })
public class DeliveryStationPrinter implements Visitor<DeliveryStation> {

    @Override
    public void visit(final DeliveryStation visitee) {
        System.out.printf("%-10s%-10s%-30s", visitee.cafeteria().name(), visitee.acronym(), visitee.description());
    }
}
