/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.pos.console.presentation.delivery;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.pos.console.ECafeteriaPOS;
import eapli.ecafeteria.delivery.application.OpenDeliveryStationController;
import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.EnumSelectWidget;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author arocha
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class OpenDeliveryStationUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(OpenDeliveryStationUI.class);

    @Autowired
    private OpenDeliveryStationController theController;

    @Override
    protected boolean doShow() {
        final DeliveryStation station = getThisPOS();

        // TODO: Suggest current day and MealType
        final EnumSelectWidget<MealType> selectorMealtType = new EnumSelectWidget<>("Select a meal type",
                MealType.class);
        selectorMealtType.show();
        final MealType aMealType = selectorMealtType.selectedElement();

        final Calendar day = Console.readCalendar("Day (dd-mm-yyyy):");

        try {
            final DeliveryStationWorkSession session = theController.openDeliveryStation(station, aMealType, day);
            ECafeteriaPOS.setWorkingSession(session);

            System.out.println("Station Opened. Now you can work.");
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }
        return false;
    }

    private DeliveryStation getThisPOS() {
        // TODO read the POS id from the application configuration file
        final SelectWidget<DeliveryStation> selectorPOS = new SelectWidget<>("Select a POS",
                theController.deliveryStations(), new DeliveryStationPrinter());
        selectorPOS.show();
        return selectorPOS.selectedElement();
    }

    @Override
    public String headline() {
        return "Open Delivery Station";
    }
}
