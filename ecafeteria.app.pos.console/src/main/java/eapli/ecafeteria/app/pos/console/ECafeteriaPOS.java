/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.app.pos.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import eapli.ecafeteria.app.components.authz.console.presentation.LoginUI;
import eapli.ecafeteria.app.components.base.console.presentation.ECafeteriaBaseApplication;
import eapli.ecafeteria.app.pos.console.presentation.MainMenu;
import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.infrastructure.pubsub.EventDispatcher;

/**
 * eCafeteria POS (Point of Sale) application to recharge the user cards and
 * deliver meals.
 *
 * @author Paulo Gandra Sousa
 */
@SpringBootApplication
@ComponentScan(basePackages = { "eapli.ecafeteria.spring", })
@SuppressWarnings("squid:S106")
public class ECafeteriaPOS extends ECafeteriaBaseApplication {

    private static DeliveryStationWorkSession session;

    public static void setWorkingSession(final DeliveryStationWorkSession aSession) {
        session = aSession;
    }

    public static DeliveryStationWorkSession session() {
        return session;
    }

    public boolean hasSession() {
        return session != null;
    }

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(ECafeteriaPOS.class, args);
    }

    @Autowired
    private LoginUI loginUI;
    @Autowired
    private MainMenu mainMenu;

    @Override
    protected void doMain(final String[] args) {
        // login and go to main menu
        if (loginUI.withRole(CafeteriaRoles.CASHIER).show()) {
            mainMenu.mainLoop();
        }
    }

    @Override
    protected String appTitle() {
        return "eCafeteria POS";
    }

    @Override
    protected String appGoodbye() {
        return "eCafeteria POS";
    }

    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
        // nothing to do
    }
}
