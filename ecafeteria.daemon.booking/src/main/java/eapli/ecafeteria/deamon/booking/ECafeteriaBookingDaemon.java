/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import eapli.ecafeteria.deamon.booking.presentation.CsvBookingProtocolServer;

/**
 * eCafeteria Booking daemon.
 *
 * @author Paulo Gandra Sousa 01/06/2020
 */
@SpringBootApplication
@ComponentScan(basePackages = { "eapli.ecafeteria.spring" })
@SuppressWarnings("squid:S106")
public class ECafeteriaBookingDaemon implements CommandLineRunner {

    private static final Logger LOGGER = LogManager.getLogger(ECafeteriaBookingDaemon.class);

    @Value("${eapli.daemon.booking.port:8899}")
    private int bookingPort;

    @Autowired
    private CsvBookingProtocolServer server;

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(ECafeteriaBookingDaemon.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        LOGGER.info("Configuring the daemon");

        LOGGER.info("Starting the server socket on port {}", bookingPort);
        server.start(bookingPort, false);
    }
}
