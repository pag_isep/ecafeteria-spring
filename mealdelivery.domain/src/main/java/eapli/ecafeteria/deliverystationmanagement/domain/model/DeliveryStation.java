/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deliverystationmanagement.domain.model;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Version;

import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.validations.Preconditions;

/**
 * A delivery station, i.e., a POS ("uma caixa").
 *
 * @TODO business identity should not be a Long but something like OU + CAFE +
 *       POS acronym
 *
 * @author Paulo Gandra de Sousa
 */
@Entity
public class DeliveryStation implements AggregateRoot<Long> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private Long version;

    @Embedded
    @AttributeOverride(name = "name", column = @Column(name = "acronym"))
    private Designation acronym;
    @ManyToOne
    private Cafeteria cafeteria;

    private String description;

    protected DeliveryStation() {
        // ORM
    }

    public DeliveryStation(final Cafeteria cafeteria, final Designation name,
            final String description) {
        Preconditions.noneNull(name, cafeteria);

        acronym = name;
        this.description = description;
        this.cafeteria = cafeteria;
    }

    public DeliveryStation(final Cafeteria cafeteria, final Designation name) {
        this(cafeteria, name, "");
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof DeliveryStation)) {
            return false;
        }

        final DeliveryStation that = (DeliveryStation) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity()) && description.equals(that.description)
                && cafeteria.equals(that.cafeteria);
    }

    @Override
    public Long identity() {
        return id;
    }

    public Designation acronym() {
        return acronym;
    }

    public String description() {
        return description;
    }

    public Cafeteria cafeteria() {
        return cafeteria;
    }
}
