/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * The delivery domain model.
 *
 * <p>
 * <img src="overview-delivery.svg"/>
 * </p>
 * <!--
 *
 * @startuml overview-delivery.svg
 *
 *           package aggr-CafeteriaShift <<Rectangle>> {
 *           class CafeteriaShift << (E,white) Root>> #cyan
 *           class CafeteriaShiftState << (V,white) >>
 *           class MealType << (V,white) >>
 *           class Date << (V,white) >>
 *           class OpeningTime << (V,white) >>
 *           class ClosingTime << (V,white) >>
 *           }
 *
 *           package aggr-POS <<Rectangle>> {
 *           class POS << (E,white) Root>> #cyan
 *           class POSName << (V,white) >>
 *           }
 *
 *           package aggr-POSShift <<Rectangle>> {
 *           class POSShift << (E,white) Root>> #cyan
 *           class POSShiftState << (V,white) >>
 *           }
 *
 *           package aggr-BookingDelivery <<Rectangle>> {
 *           class BookingDelivery << (E,white) Root>> #cyan
 *           }
 *
 *           package aggr-Booking <<Rectangle>> {
 *           class Booking << (E,white) Root>> #cyan
 *           class BookingState << (V,white) >>
 *           }
 *
 *           package aggr-Meal <<Rectangle>> {
 *           class Meal << (E,white) Root>> #cyan
 *           class " MealType" << (V,white) >>
 *           class " Date" << (V,white) >>
 *           }
 *
 *           package aggr-SystemUser <<Rectangle>> {
 *           class SystemUser<< (E,white) Root>> #cyan
 *           }
 *
 *           package aggr-Complaint <<Rectangle>> {
 *           class Complaint << (E,white) Root>> #cyan
 *           class "Date " << (V,white) >>
 *           }
 *
 *           package aggr-CafeteriaUser <<Rectangle>> {
 *           class CafeteriaUser << (E,white) Root>> #cyan
 *           }
 *
 *           CafeteriaShiftState <-- CafeteriaShift
 *           MealType <- CafeteriaShift
 *           Date <-- CafeteriaShift
 *           OpeningTime <-- CafeteriaShift
 *           ClosingTime <-- CafeteriaShift
 *           CafeteriaShift -[hidden]> POS
 *
 *           POSName "1" <- "1" POS
 *           POS "1" <- "*" POSShift
 *
 *           CafeteriaShift "1" <- "*" POSShift
 *           POSShiftState <-- POSShift
 *           SystemUser <-- POSShift
 *
 *           POSShift "1" <- "*" BookingDelivery
 *
 *           BookingDelivery "1" -> "1" Booking
 *           Booking "1" -> "1" BookingState
 *           Booking "1" -> "1" Meal
 *           Meal "1" -> "1" " MealType"
 *           "Date" "1" <-- "1" Meal
 *
 *           Meal "0..1" <-- Complaint
 *           "Date" <-- Complaint
 *           Complaint -> "0..1" CafeteriaUser
 *
 *
 *           class CafeteriaShiftStarted <<event>>
 *           class CafeteriaShiftFinished <<event>>
 *
 *           CafeteriaShift <.. CafeteriaShiftStarted
 *           CafeteriaShift <.. CafeteriaShiftFinished
 *
 *           CafeteriaShiftStarted -[hidden]-> CafeteriaShiftFinished
 *
 *           class POSShiftStarted <<event>>
 *           class POSShiftFinished <<event>>
 *           class BookingDelivered <<event>>
 *
 *           POSShift <.. POSShiftStarted
 *           POSShift <.. POSShiftFinished
 *           POSShift -[hidden]-> POSShiftFinished
 *
 *           BookingDelivery <.. BookingDelivered
 *
 *
 * @enduml
 *         -->
 */
package eapli.ecafeteria.delivery.domain.model;