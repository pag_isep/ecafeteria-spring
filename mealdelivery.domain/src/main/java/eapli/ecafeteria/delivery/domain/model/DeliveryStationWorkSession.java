/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.delivery.domain.model;

import java.io.Serializable;
import java.util.Calendar;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;

import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStationState;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.time.util.CurrentTimeCalendars;
import eapli.framework.validations.Preconditions;

/**
 * A work session (a shift - lunch or dinner - on a specific day) at a delivery
 * station.
 *
 * @author arocha
 */
@Entity
public class DeliveryStationWorkSession implements AggregateRoot<Long>, Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private MealType mealType;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "ofday")
    private Calendar day;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DeliveryStationState state;

    @ManyToOne(optional = false)
    private DeliveryStation station;

    @ManyToOne(optional = false)
    private SystemUser cashier;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "shift_start")
    private Calendar start;

    @Temporal(TemporalType.DATE)
    @Column(name = "shift_end")
    private Calendar end;

    protected DeliveryStationWorkSession() {
        // ORM
    }

    /**
     * Constructor.
     *
     * @param station
     * @param cashier
     * @param mealType
     * @param day
     */
    public DeliveryStationWorkSession(final DeliveryStation station, final SystemUser cashier,
            final MealType mealType,
            final Calendar day) {
        Preconditions.noneNull(station, cashier, mealType, day);

        this.station = station;
        this.cashier = cashier;
        this.mealType = mealType;
        this.day = day;
        state = DeliveryStationState.OPEN;
        start = CurrentTimeCalendars.now();
    }

    public DeliveryStation station() {
        return station;
    }

    public Boolean isOpen() {
        return state == DeliveryStationState.OPEN;
    }

    public void close() {
        if (state == DeliveryStationState.OPEN) {
            end = CurrentTimeCalendars.now();
            state = DeliveryStationState.CLOSED;
        }
        // simply ignore if we want to close an already closed worksession
    }

    public MealType mealType() {
        return mealType;
    }

    public Calendar day() {
        return day;
    }

    public SystemUser operator() {
        return cashier;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public String toString() {
        return "DeliveryStationWorkSession[ " + id + " ]";
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof DeliveryStationWorkSession)) {
            return false;
        }

        final DeliveryStationWorkSession that = (DeliveryStationWorkSession) other;
        if (this == that) {
            return true;
        }
        return new EqualsBuilder().append(mealType, that.mealType).append(day, that.day)
                .append(state, that.state)
                .append(station, that.station).append(cashier, that.cashier)
                .append(start, that.start)
                .append(end, that.end).isEquals();
    }

    @Override
    public Long identity() {
        return id;
    }
}
