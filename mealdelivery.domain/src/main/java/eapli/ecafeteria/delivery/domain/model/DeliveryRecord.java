/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.delivery.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;

import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Invariants;
import eapli.framework.validations.Preconditions;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Entity
public class DeliveryRecord implements AggregateRoot<Long> {
    private static final long serialVersionUID = -138438217603071147L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @ManyToOne(optional = false)
    private DeliveryStationWorkSession session;

    @OneToOne(optional = false)
    private Booking booking;

    protected DeliveryRecord() {
        // ORM
    }

    /**
     * Constructor
     *
     * @param session
     * @param booking
     */
    public DeliveryRecord(final DeliveryStationWorkSession session, final Booking booking) {
        Preconditions.noneNull(session, booking);

        Invariants.ensure(session.isOpen(), "POS Session must be open to process this operation");

        this.session = session;
        this.booking = booking;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof DeliveryRecord)) {
            return false;
        }

        final DeliveryRecord that = (DeliveryRecord) other;
        return new EqualsBuilder().append(session, that.session).append(booking, that.booking).isEquals();
    }

    @Override
    public Long identity() {
        return id;
    }
}
