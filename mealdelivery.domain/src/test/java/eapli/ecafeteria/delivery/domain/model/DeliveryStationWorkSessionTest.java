/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.delivery.domain.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

/**
 *
 * @author arocha
 */
class DeliveryStationWorkSessionTest {
    static Calendar aDate = Calendar.getInstance();

    private SystemUser aCashier;
    private DeliveryStation station;

    public static SystemUser dummyUser(final String username, final Role... roles) {
        // should we load from spring context?
        final SystemUserBuilder userBuilder = new SystemUserBuilder(new NilPasswordPolicy(),
                new PlainTextEncoder());
        return userBuilder.with(username, "duMMy1", "dummy", "dummy", "a@b.ro").withRoles(roles)
                .build();
    }

    @BeforeEach
    void setUp() {
        station = new DeliveryStation(
                new Cafeteria(new OrganicUnit(Designation.valueOf("OU1"), "O.U. One", "lorum ipsilon"),
                        Designation.valueOf("MAIN")),
                Designation.valueOf("POS001"));
        // dummy cashier
        aCashier = dummyUser("cash", CafeteriaRoles.CASHIER);
    }

    /**
     * Test of isOpen method, of class DeliveryStationWorkSession.
     */
    @Test
    void testIsOpen() {
        System.out.println("isOpen");
        final DeliveryStationWorkSession instance = new DeliveryStationWorkSession(station,
                aCashier, MealType.DINNER,
                aDate);
        final Boolean expResult = true;
        final Boolean result = instance.isOpen();
        assertEquals(expResult, result);
    }

    /**
     * Test of mealType method, of class DeliveryStationWorkSession.
     */
    @Test
    void testMealType() {
        System.out.println("mealType");
        final DeliveryStationWorkSession instance = new DeliveryStationWorkSession(station,
                aCashier, MealType.LUNCH,
                aDate);
        final MealType expResult = MealType.LUNCH;
        final MealType result = instance.mealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of day method, of class DeliveryStationWorkSession.
     */
    @Test
    void testDay() {
        System.out.println("day");
        final DeliveryStationWorkSession instance = new DeliveryStationWorkSession(station,
                aCashier, MealType.LUNCH,
                aDate);
        final Calendar expResult = aDate;
        final Calendar result = instance.day();
        assertEquals(expResult, result);
    }
}
