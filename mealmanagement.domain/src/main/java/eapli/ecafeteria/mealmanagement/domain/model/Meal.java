/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealmanagement.domain.model;

import java.util.Calendar;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.time.util.Calendars;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Version;

/**
 * A meal. An item of a menu.
 *
 * @author Paulo Gandra Sousa
 *
 */
@Entity
@Table(name = "MEAL")
public class Meal implements AggregateRoot<Long> {

	private static final long serialVersionUID = 1L;

	/**
	 * In this case we are showcasing the use of the database generated identity as
	 * an opaque business identity. This should be very well thought off as we are
	 * coupling a business concept with an implementation detail and it should be
	 * avoided. However, in scenarios where an opaque Identifier is needed it is one
	 * possible solution.
	 * <p>
	 * See also the use of UUID as an opaque identity for
	 * {@link eapli.ecafeteria.mealbooking.domain.Booking Booking}
	 */
	@Id
	@GeneratedValue
	private Long id;

	@Version
	private Long version;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private MealType mealType;

	// renamed to "ofday" as "day" is a reserved word in H2 2.1.x
	@Temporal(TemporalType.DATE)
	@Column(nullable = false, name = "ofday")
	private Calendar day;

	@ManyToOne(optional = false)
	private Dish dish;

	@ManyToOne(optional = false)
	private Menu menu;

	protected Meal() {
		// for ORM
	}

	/**
	 *
	 * @param mealType
	 * @param ofDay
	 * @param dish
	 * @param theMenu
	 */
	public Meal(final MealType mealType, final Calendar ofDay, final Dish dish, final Menu theMenu) {
		Preconditions.noneNull(mealType, ofDay, dish, theMenu);

		this.mealType = mealType;
		day = ofDay;
		this.dish = dish;
		menu = theMenu;
	}

	@Override
	public boolean equals(final Object o) {
		return DomainEntities.areEqual(this, o);
	}

	@Override
	public int hashCode() {
		return DomainEntities.hashCode(this);
	}

	public MealType mealType() {
		return mealType;
	}

	public Dish dish() {
		return dish;
	}

	public Calendar day() {
		return day;
	}

	@Override
	public Long identity() {
		return id;
	}

	@Override
	public boolean sameAs(final Object other) {
		if (!(other instanceof Meal)) {
			return false;
		}

		final var that = (Meal) other;
		if (this == that) {
			return true;
		}

		return identity().equals(that.identity()) && day.equals(that.day) && dish.sameAs(that.dish)
				&& mealType.equals(that.mealType) && menu.sameAs(that.menu);
	}

	public Menu menu() {
		return menu;
	}

	@Override
	public String toString() {
		return dish + " @ " + Calendars.format(day) + " / " + mealType;
	}
}
