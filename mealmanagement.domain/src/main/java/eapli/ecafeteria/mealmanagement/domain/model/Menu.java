/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealmanagement.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Version;

import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.time.domain.model.DateInterval;
import eapli.framework.validations.Preconditions;

/**
 *
 * @author losa
 * @author Fernando
 */
@Entity
public class Menu implements AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Long version;

    // TODO use a value object class instead of String?
    private String description;

    @ManyToOne(optional = false)
    private Cafeteria cafeteria;

    @Embedded
    private DateInterval timePeriod;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private MenuStatus status;

    protected Menu() {
        // for ORM
    }

    public Menu(final Cafeteria cafeteria, final String description,
            final DateInterval timePeriod) {
        Preconditions.noneNull(cafeteria, description, timePeriod);

        this.cafeteria = cafeteria;
        this.description = description;
        this.timePeriod = timePeriod;
        status = MenuStatus.WIP;
    }

    public Cafeteria cafeteria() {
        return cafeteria;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public Long identity() {
        return id;
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof Menu)) {
            return false;
        }

        final Menu that = (Menu) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && cafeteria.equals(that.cafeteria)
                && timePeriod.equals(that.timePeriod) && status.equals(that.status);
    }

    public String description() {
        return description;
    }

    public DateInterval timePeriod() {
        return timePeriod;
    }

    public MenuStatus status() {
        return status;
    }

    @Override
    public String toString() {
        return String.format("Menu[ %d ]", id);
    }
}
