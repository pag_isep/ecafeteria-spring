/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.components.meals.console.presentation;

import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.framework.time.util.Calendars;
import eapli.framework.visitor.Visitor;

/**
 * @author ajs@isep.ipp.pt 02/05/2016
 */
@SuppressWarnings({ "squid:S106" })
public class MenuPrinter implements Visitor<Menu> {

    @Override
    public void visit(final Menu visitee) {
        String start = Calendars.format(visitee.timePeriod().start());
        start += Calendars.weekdayName(visitee.timePeriod().start());
        final String end = Calendars.format(visitee.timePeriod().end());
        System.out.printf("%-8s%-30s%-20s%-20s", visitee.identity(), visitee.description(), start,
                end);
    }
}
