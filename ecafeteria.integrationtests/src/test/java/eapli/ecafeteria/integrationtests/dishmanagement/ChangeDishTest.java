/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.application.ChangeDishController;
import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.model.NutricionalInfo;
import eapli.ecafeteria.integrationtests.ECafeteriaIntegrationTests;
import eapli.framework.money.domain.model.Money;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = ECafeteriaIntegrationTests.class)
public class ChangeDishTest extends IntegrationTestBase {
    private static Logger LOGGER = LogManager.getLogger(ChangeDishTest.class);

    @Autowired
    private ChangeDishController changeDishController;

    private Dish subject;

    @Before
    public void setUp() throws Exception {
        registerAndAuthenticateMenuManager();

        subject = fetchOrCreateDish();
    }

    @Test
    @Transactional
    public void testUpdateDish() {
        // the UI will present the dish and allow the user to enter the changed data
        // (e.g., in a
        // data grid)
        final var newInfo = new NutricionalInfo(20, 5);
        final var newPrice = Money.euros(25);
        subject.update(subject.dishType(), false, newPrice, newInfo);

        // the UI calls the controller
        final var updated = changeDishController.updateDish(subject);

        assertEquals(newInfo, updated.nutricionalInfo().orElseThrow(IllegalStateException::new));
        assertEquals(newPrice, updated.currentPrice());
        assertFalse(updated.isActive());

        LOGGER.info("»»» Updated dish {}", updated);
    }

    @Test
    @Transactional
    public void testChangeDishPrice() {
        // change price
        final Money oldPrice = subject.currentPrice();
        final Money newPrice = oldPrice.add(Money.euros(1));

        final Dish changedDish = changeDishController.changeDishPrice(subject, newPrice);

        assertEquals(newPrice, changedDish.currentPrice());

        LOGGER.info("Changed price of {} to {}", subject, changedDish.currentPrice());
    }

    @Test
    @Transactional
    public void testChangeDishNutricionalInfo() {
        // change nutricional info
        final var oldInfo = subject.nutricionalInfo().orElseThrow(IllegalStateException::new);
        final var newInfo = new NutricionalInfo(10, 2);

        final var changedDish = changeDishController.changeDishNutricionalInfo(subject, newInfo);

        assertEquals(newInfo, changedDish.nutricionalInfo().get());

        LOGGER.info("Changed nutricional info of {} from to {}", subject, oldInfo, changedDish.nutricionalInfo());
    }
}