/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishTypeRepository;
import eapli.ecafeteria.integrationtests.ECafeteriaIntegrationTests;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = ECafeteriaIntegrationTests.class)
public class DishTypeCrudTest {
    private static final Logger LOGGER = LogManager.getLogger(DishTypeCrudTest.class);

    @Autowired
    private DishTypeRepository repo;

    @Test
    @Transactional
    public void testCRUD() {
        // save
        if (!repo.ofIdentity("one").isPresent()) {
            final DishType d = repo.save(new DishType("one", "one desc"));
            assertNotNull(d);
        }
        if (!repo.ofIdentity("two").isPresent()) {
            final DishType d = repo.save(new DishType("two", "two desc"));
            assertNotNull(d);
        }
        LOGGER.info("created dish types");

        // findAll
        final Iterable<DishType> l = repo.findAll();
        assertNotNull(l);
        assertNotNull(l.iterator());
        assertTrue(l.iterator().hasNext());
        LOGGER.info("found all dish types");

        // count
        final long n = repo.count();
        LOGGER.info("# dish types = {}", n);
        assertTrue(n >= 0);

        // ofIdentity
        final DishType dt1 = repo.ofIdentity("one").orElseThrow(IllegalStateException::new);
        assertNotNull(dt1);
        final DishType dt2 = repo.ofIdentity("two").orElseThrow(IllegalStateException::new);
        assertNotNull(dt2);
        LOGGER.info("found dish types of identity");

        // containsOfIdentity
        final boolean hasId = repo.containsOfIdentity(dt1.identity());
        assertTrue(hasId);
        LOGGER.info("contains dish types of identity");

        // contains
        final boolean has = repo.contains(dt1);
        assertTrue(has);
        LOGGER.info("contains dish type");

        // delete
        repo.delete(dt1);
        LOGGER.info("deleted dish type");

        // deleteOfIdentity
        repo.deleteOfIdentity(dt2.identity());
        LOGGER.info("deleted dish type of identity");

        // size
        final long n2 = repo.size();
        assertTrue(n2 == n - 2);
        LOGGER.info("# dish types = {}", n2);
    }
}
