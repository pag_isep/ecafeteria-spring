/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.application.viadto.ChangeDishViaDTOController;
import eapli.ecafeteria.dishmanagement.dto.ChangeDishNutricionalInfoDTO;
import eapli.ecafeteria.dishmanagement.dto.ChangeDishPriceDTO;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.integrationtests.ECafeteriaIntegrationTests;

/**
 *
 * @author Paulo Gandra de Sousa 2021.05.20
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = ECafeteriaIntegrationTests.class)
public class ChangeDishViaDTOTest extends IntegrationTestBase {
	private static Logger LOGGER = LogManager.getLogger(ChangeDishViaDTOTest.class);

	@Autowired
	private ChangeDishViaDTOController changeDishController;

	private DishDTO subject;

	@Before
	public void setUp() throws Exception {
		registerAndAuthenticateMenuManager();

		subject = fetchOrCreateDish().toDTO();
	}

	@Test
	@Transactional
	public void testChangeDishPrice() {
		final var newPrice = new ChangeDishPriceDTO(subject.getName(), 31, subject.getCurrency());

		final var changed = changeDishController.changeDishPrice(newPrice);

		assertEquals(31, changed.getPrice(), 0.001);

		LOGGER.info("Changed price of {} to {} via DTO", subject, changed.getPrice());
	}

	@Test
	@Transactional
	public void testChangeDishNutricionalInfo() {
		final var newInfo = new ChangeDishNutricionalInfoDTO(subject.getName(), 10, 2);
		final var changed = changeDishController.changeDishNutricionalInfo(newInfo);

		assertEquals(10, changed.getCalories());
		assertEquals(2, changed.getSalt());

		LOGGER.info("Changed nutricional info of {} via DTO", subject);
	}

	@Test
	@Transactional
	public void testUpdateDish() {
		// the UI will present the dish and allow the user to enter the changed data
		// (e.g., in a data grid)
		subject.setPrice(25);
		subject.setSalt(5);
		subject.setCalories(20);
		subject.setActive(false);

		// the UI calls the controller
		final var updated = changeDishController.updateDish(subject);

		assertEquals(20, updated.getCalories());
		assertEquals(5, updated.getSalt());
		assertEquals(25, updated.getPrice(), 0.001);
		assertFalse(updated.isActive());

		LOGGER.info("»»» Updated dish via DTO {}", updated.getName());
	}
}