/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishController;
import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.integrationtests.ECafeteriaIntegrationTests;
import eapli.ecafeteria.reporting.dishes.application.DishReportingController;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = ECafeteriaIntegrationTests.class)
public class DishManagementTest extends IntegrationTestBase {
    private static Logger LOGGER = LogManager.getLogger(DishManagementTest.class);

    @Autowired
    private ActivateDeactivateDishController activateDeactivateDishController;

    @Autowired
    private DishReportingController dishReportingController;

    private Dish subject;

    @Before
    @Transactional
    public void setUp() throws Exception {
        registerAndAuthenticateMenuManager();

        subject = fetchOrCreateDish();
    }

    @Test
    @Transactional
    public void testAllDishes() {
        final var l = activateDeactivateDishController.allDishes();

        assertNotNull(l);
    }

    @Test
    @Transactional
    public void testActivateDeactivateDish() {
        final var oldStatus = subject.isActive();

        final var changedDish = activateDeactivateDishController.changeDishState(subject);

        assertEquals(!oldStatus, changedDish.isActive());

        LOGGER.info("Changed status of {} to {}", subject, subject.isActive());
    }

    // TODO improve these tests
    @Test
    @Transactional
    public void testReportingDish() {
        final var l1 = dishReportingController.reportDishesPerCaloricCategory();
        assertNotNull(l1);

        final var l2 = dishReportingController.reportDishesPerCaloricCategoryAsTuples();
        assertNotNull(l2);

        final var l3 = dishReportingController.reportDishesPerDishType();
        assertNotNull(l3);

        final var l4 = dishReportingController.reportHighCaloriesDishes();
        assertNotNull(l4);

        LOGGER.info("Executed Reporting of dishes");
    }
}