/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.model.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.dishmanagement.domain.model.NutricionalInfo;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishRepository;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishTypeRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.UserManagementService;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.money.domain.model.Money;

/**
 *
 * @author Paulo Gandra de Sousa 29/05/2019
 *
 */
public class IntegrationTestBase {
    private static Logger LOGGER = LogManager.getLogger(IntegrationTestBase.class);

    @Autowired
    private UserManagementService userSvc;

    @Autowired
    private AuthenticationService authz;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private DishTypeRepository dishTypeRepository;

    /**
     *
     * @param username
     * @param password
     * @param role
     * @return
     */
    @Transactional
    protected SystemUser registerUser(final String username, final String password, final Role role) {
        return userSvc.userOfIdentity(Username.valueOf(username)).orElseGet(() -> {
            final Set<Role> roles = new HashSet<>();
            roles.add(role);
            return userSvc.registerNewUser(username, password, "joe", "power", "joe@email.org", roles);
        });
    }

    /**
     *
     */
    @Transactional
    protected Dish fetchOrCreateDish() {
        // fetch or create "fish" dish type
        final var dtFish = dishTypeRepository.ofIdentity("peixe")
                .orElseGet(() -> dishTypeRepository.save(new DishType("peixe", "peixe")));

        // fetch or create dish for test
        final var subject = dishRepository.ofIdentity(Designation.valueOf("robalo"))
                .orElseGet(
                        () -> {
                            final Dish robalo = new DishBuilder().ofType(dtFish).named(Designation.valueOf("robalo"))
                                    .costing(Money.euros(15.0)).withNutricionalInfo(new NutricionalInfo(1, 1)).build();
                            return dishRepository.save(robalo);
                        });
        LOGGER.debug("Created dish");

        return subject;
    }

    /**
     *
     */
    protected void registerAndAuthenticateMenuManager() {
        registerUser("chef", "Password1", CafeteriaRoles.MENU_MANAGER);

        final var us = authz.authenticate("chef", "Password1");
        LOGGER.info("Logged in as {}", us.orElseThrow(IllegalStateException::new).authenticatedUser());
    }
}