/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.integrationtests.dishmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishTypeController;
import eapli.ecafeteria.dishmanagement.application.ChangeDishTypeController;
import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishTypeRepository;
import eapli.ecafeteria.integrationtests.ECafeteriaIntegrationTests;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = ECafeteriaIntegrationTests.class)
public class DishTypeManagementTest extends IntegrationTestBase {
	private static Logger LOGGER = LogManager.getLogger(DishTypeManagementTest.class);

	@Autowired
	private ActivateDeactivateDishTypeController controller;

	@Autowired
	private ChangeDishTypeController changeDishTypeController;

	@Autowired
	private DishTypeRepository dishTypeRepository;

	private DishType subject;

	@Before
	@Transactional
	public void setUp() throws Exception {
		registerAndAuthenticateMenuManager();

		// create a couple of dish types for the test
		subject = dishTypeRepository.ofIdentity("one")
				.orElseGet(() -> dishTypeRepository.save(new DishType("one", "one desc")));

		LOGGER.debug("created dish types");
	}

	@Test
	@Transactional
	public void testAllDishTypes() {
		final var l = controller.allDishTypes();

		assertNotNull(l);
	}

	@Test
	@Transactional
	public void testChangeDishType() {
		final var newDescription = subject.description() + " (edited)";
		subject = changeDishTypeController.changeDishType(subject, newDescription);

		assertEquals(newDescription, subject.description());

		LOGGER.info("Changed description of {} to {}", subject, subject.description());
	}

	@Test
	@Transactional
	public void testActivateDeactivateDishType() {
		final var oldStatus = subject.isActive();

		subject = controller.changeDishTypeState(subject);

		assertEquals(!oldStatus, subject.isActive());

		LOGGER.debug("Changed status of {} to {}", subject, subject.isActive());
	}
}
