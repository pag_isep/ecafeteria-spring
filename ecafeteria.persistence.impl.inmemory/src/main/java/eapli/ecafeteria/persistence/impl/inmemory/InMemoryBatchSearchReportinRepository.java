/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.inmemory;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.repositories.BatchSearchReportingRepository;

/**
 * @author Paulo Gandra de Sousa
 *
 */
public class InMemoryBatchSearchReportinRepository implements BatchSearchReportingRepository {

    private static final String NOT_SUPPORTED_YET = "Not supported yet.";

    static {
        InMemoryInitializer.init();
    }

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.persistence.BatchSearchReportingRepository#
     * bookingsOfMealsWhichUsedBatch(eapli.ecafeteria.domain.traceability.Material,
     * eapli.ecafeteria.domain.traceability.BatchNumber)
     */
    @Override
    public Iterable<Booking> bookingsOfMealsWhichUsedBatch(final Material material, final BatchNumber batchNumber) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.persistence.BatchSearchReportingRepository#
     * mealsWhichUsedBatch(eapli.ecafeteria.domain.traceability.Material,
     * eapli.ecafeteria.domain.traceability.BatchNumber)
     */
    @Override
    public Iterable<Meal> mealsWhichUsedBatch(final Material material, final BatchNumber batchNumber) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.persistence.BatchSearchReportingRepository#
     * whoConsumedMealsWhichUsedBatch(eapli.ecafeteria.domain.traceability.Material,
     * eapli.ecafeteria.domain.traceability.BatchNumber)
     */
    @Override
    public Iterable<CafeteriaUser> whoConsumedMealsWhichUsedBatch(final Material material,
            final BatchNumber batchNumber) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET);
    }
}
