/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.inmemory;

import java.util.Calendar;
import java.util.Optional;

import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.delivery.domain.repositories.DeliveryStationWorkSessionRepository;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author Paulo Gandra de Sousa
 */
public class InMemoryDeliveryStationWorkSessionRepository extends
        InMemoryDomainRepository<DeliveryStationWorkSession, Long>
        implements DeliveryStationWorkSessionRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<DeliveryStationWorkSession> findOpenWorkSessions(final Calendar day,
            final MealType mealType) {
        return match(e -> e.mealType().equals(mealType) && e.day().equals(day) && e.isOpen());
    }

    @Override
    public Optional<DeliveryStationWorkSession> findOpenByDeliveryStation(
            final DeliveryStation station) {
        return matchOne(e -> e.station().equals(station) && e.isOpen());
    }

    @Override
    public Optional<DeliveryStationWorkSession> findBySession(final DeliveryStation station,
            final Calendar day,
            final MealType mealType) {
        return matchOne(e -> e.mealType().equals(mealType) && e.day().equals(day)
                && e.station().equals(station));
    }

    @Override
    public Optional<DeliveryStationWorkSession> findBySession(final CafeteriaName cafe,
            final Designation station,
            final Calendar day, final MealType mealType) {
        return matchOne(e -> e.mealType().equals(mealType) && e.day().equals(day)
                && e.station().acronym().equals(station)
                && e.station().cafeteria().hasIdentity(cafe));
    }
}
