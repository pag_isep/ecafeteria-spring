/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.inmemory;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUserBuilder;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CardMovement;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MovementType;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;
import eapli.framework.money.domain.model.Money;

/**
 * @author Paulo Gandra de Sousa
 *
 */
public class CardMovementHelperTest {

    private SystemUser dummyUser(final Role... roles) {
        final var userBuilder = new SystemUserBuilder(new NilPasswordPolicy(), new PlainTextEncoder());
        return userBuilder.with("dummy", "duMMy1", "dummy", "dummy", "a@b.ro").withRoles(roles).build();
    }

    private OrganicUnit dummyOU() {
        return new OrganicUnit(Designation.valueOf("dummy"), "dummy", "dummy");
    }

    /*
     * Test method for {@link CardMovementHelper#balanceOf( MecanographicNumber,
     * java.util.stream.Stream)}.
     */
    @Test
    public void testBalanceOf() {
        final var user1 = new CafeteriaUserBuilder().withOrganicUnit(dummyOU())
                .withMecanographicNumber("900330").withSystemUser(dummyUser(CafeteriaRoles.CAFETERIA_USER)).build();
        final var user2 = new CafeteriaUserBuilder().withOrganicUnit(dummyOU())
                .withMecanographicNumber("11203040").withSystemUser(dummyUser(CafeteriaRoles.CAFETERIA_USER)).build();

        final List<CardMovement> instance = new ArrayList<>();
        instance.add(new CardMovement(MovementType.RECHARGE, Money.euros(10), user1));
        instance.add(new CardMovement(MovementType.RECHARGE, Money.euros(10), user2));
        instance.add(new CardMovement(MovementType.RECHARGE, Money.euros(10), user1));
        instance.add(new CardMovement(MovementType.RECHARGE, Money.euros(10), user1));

        final Money expected = Money.euros(30);
        final Money result = CardMovementHelper.balanceOf(MecanographicNumber.valueOf("900330"), instance.stream());
        assertEquals(expected, result);
    }
}
