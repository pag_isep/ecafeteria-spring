REM assumes the build was executed with maven
SET ECAFETERIA_CP=;ecafeteria.app.bootstrap.console\target\dependency\*;

REM call the java VM, e.g, 
java -jar ecafeteria.app.bootstrap.console\target\app.bootstrap.console-7.0.0.jar eapli.ecafeteria.app.bootstrap.ECafeteriaBootstrap -smoke:e2e -smoke:basic -wait
