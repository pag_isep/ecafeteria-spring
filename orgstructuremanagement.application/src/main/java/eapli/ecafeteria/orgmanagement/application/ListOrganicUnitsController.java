/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.orgmanagement.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.ecafeteria.orgmanagement.domain.repositories.OrganicUnitRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 *
 * @author losa
 */
@Component
@UseCaseController
public class ListOrganicUnitsController {

    @Autowired
    private OrganicUnitRepository repo;
    @Autowired
    private AuthorizationService authorizationService;

    /**
     * Returns all organic units.
     *
     * @return all organic units
     */
    public Iterable<OrganicUnit> organicUnits() {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        return repo.findAll();
    }

    /**
     * Returns the details of the specified organic unit.
     *
     * @param id
     * @return the details of the specified organic unit.
     */
    public Optional<OrganicUnit> organicUnit(final Designation id) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        return repo.ofIdentity(id);
    }
}