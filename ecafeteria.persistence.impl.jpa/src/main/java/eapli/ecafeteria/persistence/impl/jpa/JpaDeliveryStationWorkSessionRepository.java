/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.delivery.domain.repositories.DeliveryStationWorkSessionRepository;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStationState;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.general.domain.model.Designation;

/**
 *
 * @author Paulo Gandra de Sousa
 */

@Component
public class JpaDeliveryStationWorkSessionRepository
        extends CafeteriaJpaRepositoryBase<DeliveryStationWorkSession, Long, Long>
        implements DeliveryStationWorkSessionRepository {

    public JpaDeliveryStationWorkSessionRepository() {
        super("id");
    }

    private static final String STATION = "station";
    private static final String MEAL_TYPE = "mealType";

    @Override
    public Iterable<DeliveryStationWorkSession> findOpenWorkSessions(final Calendar day,
            final MealType mealType) {
        return match("e.state = :state" + " AND e.day = :day" + " AND e.mealType = :mealType",
                "state",
                DeliveryStationState.OPEN, "day", day, MEAL_TYPE, mealType);
    }

    @Override
    public Optional<DeliveryStationWorkSession> findOpenByDeliveryStation(
            final DeliveryStation station) {
        return matchOne("e.state = :state" + " AND e.station = :station",
                "state",
                DeliveryStationState.OPEN, STATION,
                station);
    }

    @Override
    public Optional<DeliveryStationWorkSession> findBySession(final DeliveryStation station,
            final Calendar day,
            final MealType mealType) {
        return matchOne("e.station = :station" + " AND e.day = :day" + " AND e.mealType = :mealType",
                STATION, station,
                "day", day, MEAL_TYPE, mealType);
    }

    @Override
    public Optional<DeliveryStationWorkSession> findBySession(final CafeteriaName cafe,
            final Designation station,
            final Calendar day, final MealType mealType) {
        return matchOne(
                "e.station.cafeteria.name = :cafe AND e.station.acronym = :station AND e.day = :day AND e.mealType = :mealType",
                "cafe", cafe, STATION, station, "day", day, MEAL_TYPE, mealType);
    }
}
