/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;

import jakarta.persistence.TemporalType;
import jakarta.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealplanning.domain.model.MealPlanItem;
import eapli.ecafeteria.mealplanning.domain.repositories.MealPlanItemRepository;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt 02/05/2016
 */
@Component
class JpaMealPlanItemRepository extends CafeteriaJpaRepositoryBase<MealPlanItem, Long, Long>
        implements MealPlanItemRepository {

    public JpaMealPlanItemRepository() {
        super("id");
    }

    @Override
    public Iterable<MealPlanItem> findItemsOnPeriod(final Calendar beginDate, final Calendar endDate) {
        final TypedQuery<MealPlanItem> q = createQuery(
                "SELECT e FROM MealPlanItem e WHERE e.meal.day BETWEEN :beginDate AND :endDate", MealPlanItem.class);
        q.setParameter("beginDate", beginDate, TemporalType.DATE);
        q.setParameter("endDate", endDate, TemporalType.DATE);
        return q.getResultList();
    }
}
