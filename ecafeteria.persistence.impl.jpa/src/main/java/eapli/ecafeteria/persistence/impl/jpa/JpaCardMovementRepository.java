/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CardMovement;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CardMovementRepository;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.money.domain.model.Money;

/**
 *
 * @author mcn
 */
@Component
public class JpaCardMovementRepository extends CafeteriaJpaRepositoryBase<CardMovement, String, String>
        implements CardMovementRepository {

    public JpaCardMovementRepository() {
        super("id");
    }

    @Override
    public Iterable<CardMovement> findByUsername(final Username user) {
        return match("e.user=(SELECT c FROM CafeteriaUser c WHERE c.systemUser.username=:name)", "name", user);
    }

    @Override
    public Iterable<CardMovement> findByMecanographicNumber(final MecanographicNumber number) {
        return match("e.user.mecanographicNumber=:number", "number", number);
    }

    @Override
    public Money balanceOf(final MecanographicNumber number) {
        final var query = createQuery(
                "SELECT new eapli.framework.money.domain.model.Money(SUM(e.amount.amount), 'EUR') FROM CardMovement e WHERE e.user.mecanographicNumber=:number",
                Money.class).setParameter("number", number);
        return query.getSingleResult();
    }

    @Override
    public Money balanceOf(final Username card) {
        final var query = createQuery(
                "SELECT new eapli.framework.money.domain.model.Money(SUM(e.amount.amount), 'EUR') FROM CardMovement e WHERE e.user.systemUser.username=:name",
                Money.class).setParameter("name", card);
        return query.getSingleResult();
    }
}
