/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealbooking.domain.repositories.BookingRepository;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.infrastructure.authz.domain.model.Username;

/**
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class JpaBookingRepository extends CafeteriaJpaRepositoryBase<Booking, Long, BookingToken>
		implements BookingRepository {

	public JpaBookingRepository() {
		super("token");
	}

	@Override
	public Iterable<Booking> findByUser(final CafeteriaUser who) {
		final Map<String, Object> params = new HashMap<>();
		params.put("user", who);
		return match("e.user = :user", params);
	}

	@Override
	public Iterable<Booking> findByMeal(final Meal what) {
		final Map<String, Object> params = new HashMap<>();
		params.put("what", what);
		return match("e.meal = :what", params);
	}

	@Override
	public Iterable<Booking> findByCafe(final CafeteriaName cafe, final Calendar day, final MealType type) {
		final Map<String, Object> params = new HashMap<>();
		params.put("cafe", cafe);
		params.put("day", day);
		params.put("type", type);

		return match("e.meal.menu.cafeteria.name = :cafe AND e.meal.day = :day AND e.meal.mealType = :type", params);
	}

	@Override
	public Optional<Booking> findByUserForADayAndMealType(final CafeteriaUser user, final Calendar day,
			final MealType mealType) {
		final Map<String, Object> params = new HashMap<>();
		params.put("user", user);
		params.put("day", day);
		params.put("type", mealType);
		return matchOne("e.user = :user AND e.meal.day = :day AND e.meal.mealType = :type", params);
	}

	@Override
	public Iterable<Booking> findByUsername(Username who) {
		final Map<String, Object> params = new HashMap<>();
		params.put("who", who);
		return match("e.user.systemUser.username = :who", params);
	}

	@Override
	public Iterable<Booking> findByMecanographicNumber(MecanographicNumber who) {
		final Map<String, Object> params = new HashMap<>();
		params.put("who", who);
		return match(" e.user.mecanographicNumber = :who", params);
	}
}
