/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.model.UsedMaterialBatch;
import eapli.ecafeteria.traceability.domain.repositories.UsedMaterialBatchRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class JpaUsedMaterialRepository extends CafeteriaJpaRepositoryBase<UsedMaterialBatch, Long, Long>
        implements UsedMaterialBatchRepository {

    public JpaUsedMaterialRepository() {
        super("id");
    }

    @Override
    public Iterable<UsedMaterialBatch> findByMaterialAndBatchNumber(final Material material,
            final BatchNumber batchNumber) {
        final Map<String, Object> parms = new HashMap<>();
        parms.put("m", material);
        parms.put("b", batchNumber);
        return match("e.material  :m AND e.batchNumber = :b", parms);
    }
}
