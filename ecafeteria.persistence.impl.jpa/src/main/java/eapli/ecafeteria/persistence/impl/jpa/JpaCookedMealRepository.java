/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealexecution.domain.model.CookedMealQty;
import eapli.ecafeteria.mealexecution.domain.repositories.CookedMealRepository;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;

/**
 *
 * @author Fernando
 */
@Component
public class JpaCookedMealRepository extends CafeteriaJpaRepositoryBase<CookedMealQty, Long, Long>
		implements CookedMealRepository {

	public JpaCookedMealRepository() {
		super("id");
	}

	@Override
	public List<CookedMealQty> findByMeal(final Meal meal) {
		final Map<String, Object> params = new HashMap<>();
		params.put("meal", meal);
		return match("e.meal = :meal", params);
	}

	@Override
	public List<CookedMealQty> findByDay(final Calendar ofDay) {
		final Map<String, Object> params = new HashMap<>();
		params.put("ofDay", ofDay);
		return match("e.meal.day = :ofDay", params);
	}

	@Override
	public List<CookedMealQty> findByDay(final Calendar ofDay, final MealType mealType) {
		final Map<String, Object> params = new HashMap<>();
		params.put("ofDay", ofDay);
		params.put("mealType", mealType);
		return match("e.meal.day = :ofDay AND e.meal.mealType = :mealType", params);
	}
}
