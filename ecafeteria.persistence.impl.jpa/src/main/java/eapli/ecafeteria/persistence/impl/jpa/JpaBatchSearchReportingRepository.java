/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import jakarta.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.repositories.BatchSearchReportingRepository;

/**
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class JpaBatchSearchReportingRepository extends CafeteriaJpaReportingRepositoryBase
		implements BatchSearchReportingRepository {

	/*
	 * (non-Javadoc)
	 *
	 * @see eapli.ecafeteria.persistence.BatchSearchReportingRepository#
	 * bookingsOfMealsWhichUsedBatch(eapli.ecafeteria.domain.traceability. Material,
	 * eapli.ecafeteria.domain.traceability.BatchNumber)
	 */
	/**
	 * BATCH SEARCH
	 */
	@Override
	public Iterable<Booking> bookingsOfMealsWhichUsedBatch(final Material material, final BatchNumber batchNumber) {
		final TypedQuery<Booking> query = createQuery(
				"SELECT b FROM Booking b, UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn AND e.meal = b.meal",
				Booking.class);
		query.setParameter("m", material);
		query.setParameter("bn", batchNumber);
		return query.getResultList();
	}

	/**
	 *
	 *
	 * @param material
	 * @param batchNumber
	 * @return
	 */
	@Override
	public Iterable<Meal> mealsWhichUsedBatch(final Material material, final BatchNumber batchNumber) {
		final TypedQuery<Meal> query = createQuery(
				"SELECT e.meal FROM UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn", Meal.class);
		query.setParameter("m", material);
		query.setParameter("bn", batchNumber);
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eapli.ecafeteria.persistence.BatchSearchReportingRepository#
	 * whoConsumedMealsWhichUsedBatch(eapli.ecafeteria.domain.traceability.
	 * Material, eapli.ecafeteria.domain.traceability.BatchNumber)
	 */
	@Override
	public Iterable<CafeteriaUser> whoConsumedMealsWhichUsedBatch(final Material material,
			final BatchNumber batchNumber) {
		final TypedQuery<CafeteriaUser> query = createQuery(
				"SELECT b.user FROM Booking b, UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn AND e.meal = b.meal",
				CafeteriaUser.class);
		query.setParameter("m", material);
		query.setParameter("bn", batchNumber);
		return query.getResultList();
	}
}
