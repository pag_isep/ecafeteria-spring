/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.jpa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import jakarta.persistence.TemporalType;
import jakarta.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;

/**
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class JpaMealRepository extends CafeteriaJpaRepositoryBase<Meal, Long, Long> implements MealRepository {

    public JpaMealRepository() {
        super("id");
    }

    private static final String CAFETERIA_PARAM = "cafeteria";

    @Override
    public Iterable<Meal> findByDay(final Cafeteria where, final Calendar forDay, final MealType forMeal) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterable<Meal> findByDay(final Cafeteria where, final Calendar forDay) {
        final TypedQuery<Meal> q = createQuery(
                "SELECT e FROM Meal e WHERE e.menu.cafeteria.name = :cafeteria AND e.day = :forDay", Meal.class);
        q.setParameter(CAFETERIA_PARAM, where);
        q.setParameter("forDay", forDay, TemporalType.DATE);
        return q.getResultList();
    }

    @Override
    public Iterable<Meal> findByMenu(final Menu menu) {
        return match("e.menu=:menu", "menu", menu);
    }

    @Override
    public Iterable<Meal> findByPeriod(final Cafeteria where, final Calendar beginDate, final Calendar endDate) {
        final TypedQuery<Meal> q = createQuery(
                "SELECT e FROM Meal e WHERE e.menu.cafeteria = :cafeteria AND e.day BETWEEN :beginDate AND :endDate",
                Meal.class);
        q.setParameter("beginDate", beginDate, TemporalType.DATE);
        q.setParameter("endDate", endDate, TemporalType.DATE);
        q.setParameter(CAFETERIA_PARAM, where);
        return q.getResultList();
    }

    @Override
    public Iterable<Meal> findNotYetCookedByDay(final Cafeteria where, final Calendar ofDay, final MealType mealType) {
        final Map<String, Object> params = new HashMap<>();
        params.put("ofDay", ofDay);
        params.put("mealType", mealType);
        params.put(CAFETERIA_PARAM, where);
        return match(
                "e.menu.cafeteria = :cafeteria AND e.day = :ofDay AND e.mealType = :mealType AND e NOT IN (SELECT c.meal FROM CookedMealQty c)",
                params);
    }

    @Override
    public Iterable<Meal> findByCafeteria(final Cafeteria cafeteria) {
        final Map<String, Object> params = new HashMap<>();
        params.put(CAFETERIA_PARAM, cafeteria);
        return match("SELECT e FROM Meal e WHERE e.menu.cafeteria = :cafeteria", params);
    }

    @Override
    public Iterable<Meal> findByDayAndType(final CafeteriaName where, final Calendar forDay, final MealType forMeal) {
        final TypedQuery<Meal> q = createQuery(
                "SELECT e FROM Meal e WHERE e.menu.cafeteria.name = :cafeteria AND e.day = :forDay AND e.mealType = :type",
                Meal.class);
        q.setParameter(CAFETERIA_PARAM, where);
        q.setParameter("forDay", forDay, TemporalType.DATE);
        q.setParameter("type", forMeal);
        return q.getResultList();
    }
}
