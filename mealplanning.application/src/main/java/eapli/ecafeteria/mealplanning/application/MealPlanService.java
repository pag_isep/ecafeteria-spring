/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealplanning.application;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealmanagement.application.ListMealService;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealplanning.domain.model.MealPlan;
import eapli.ecafeteria.mealplanning.domain.model.MealPlanItem;
import eapli.ecafeteria.mealplanning.domain.repositories.MealPlanItemRepository;
import eapli.ecafeteria.mealplanning.domain.repositories.MealPlanRepository;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.application.ApplicationService;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 * an application service to avoid overloading CreateMealPlanController with responsibilities /
 * business logic
 *
 * @author Jorge Santos ajs@isep.ipp.pt 09/05/2016
 *
 */
@Component
@ApplicationService
class MealPlanService {
    private static final Logger LOGGER = LogManager.getLogger(MealPlanService.class);

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private ListMealService listMealSvc;
    @Autowired
    private MealPlanItemRepository mealPlanItemRepository;
    @Autowired
    private MealPlanRepository mealPlanRepository;

    @Transactional
    public void importMeals(final Cafeteria cafeteria, final Calendar beginDate,
            final Calendar endDate) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.MENU_MANAGER);

        final Iterable<Meal> meals = listMealSvc.mealsOnPeriod(cafeteria, beginDate, endDate);

        for (final Meal meal : meals) {
            final MealPlanItem item = new MealPlanItem(meal, 0);

            try {
                // TODO item should be returned from this method
                mealPlanItemRepository.save(item);
            } catch (final ConcurrencyException | IntegrityViolationException ex) {
                LOGGER.debug("%s already was already imported before", meal, ex);
            }
        }
    }

    public Iterable<MealPlan> allMealPlans() {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.MENU_MANAGER);

        return mealPlanRepository.findAll();
    }
}
