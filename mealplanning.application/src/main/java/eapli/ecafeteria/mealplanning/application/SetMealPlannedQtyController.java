/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealplanning.application;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealplanning.domain.model.MealPlan;
import eapli.ecafeteria.mealplanning.domain.model.MealPlanItem;
import eapli.ecafeteria.mealplanning.domain.repositories.MealPlanItemRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.validations.Preconditions;

/**
 * @author ajs@isep.ipp.pt 21/05/2016
 */
@Component
@UseCaseController
public class SetMealPlannedQtyController {

    @Autowired
    private ListMealPlanItemService itemSvc;
    @Autowired
    private MealPlanItemRepository repo;
    @Autowired
    private MealPlanService planSvc;
    @Autowired
    private AuthorizationService authorizationService;

    @Transactional
    public MealPlanItem setPlannedQty(final MealPlanItem theMealPlanItem, final int newQty) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.MENU_MANAGER);

        Preconditions.nonNull(theMealPlanItem);

        theMealPlanItem.setPlannedMeals(newQty);

        return repo.save(theMealPlanItem);
    }

    public Iterable<MealPlan> listMealPlans() {
        return planSvc.allMealPlans();
    }

    public Iterable<MealPlanItem> listMealPlanItems(final Calendar beginDate,
            final Calendar endDate) {
        return itemSvc.mealPlanItemsOnPeriod(beginDate, endDate);
    }
}
