/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.domain.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 * @author Paulo Gandra de Sousa 13/05/2022
 *
 */
class SignupRequestTest {

    private SignupRequest subject;

    @BeforeEach
    void setUp() {
        final OrganicUnit ou = new OrganicUnit(Designation.valueOf("OU1"), "OU one", "OU one desc");
        subject = new SignupRequestBuilder(new NilPasswordPolicy(), new PlainTextEncoder())
                .withData("user", "pass", "a@b.com", ou, "1234567").withName("Mary", "Smith")
                .createdOn(CurrentTimeCalendars.now()).build();
    }

    @Test
    void ensureNewlyCreatedSignupRequestIsPending() {
        assertTrue(subject.isPending());
    }

    @Test
    void ensureAceptedSignupRequestIsInProperState() {
        subject.accept();

        assertTrue(subject.isAccepted());
        assertFalse(subject.isPending());
        assertFalse(subject.isRefused());
    }

    @Test
    void ensureREfusedSignupRequestIsInProperState() {
        subject.refuse();

        assertTrue(subject.isRefused());
        assertFalse(subject.isAccepted());
        assertFalse(subject.isPending());
    }
}
