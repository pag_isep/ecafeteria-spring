/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.domain.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import eapli.ecafeteria.cafeteriausermanagement.testutil.CafeteriaUsersTestUtil;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.Role;

/**
 * @author Nuno Bettencourt [NMB] on 03/04/16.
 */
public class CafeteriaUserTest {

    private static final String aMecanographicNumber = "abc";
    private static final String anotherMecanographicNumber = "xyz";

    @Test
    public void ensureCafeteriaUserEqualsPassesForTheSameMecanographicNumber() {
        final Set<Role> roles = new HashSet<>();
        roles.add(CafeteriaRoles.ADMIN);

        final CafeteriaUser aCafeteriaUser = CafeteriaUsersTestUtil.dummyCafeteriaUser(aMecanographicNumber,
                "OU");
        final CafeteriaUser anotherCafeteriaUser = CafeteriaUsersTestUtil
                .dummyCafeteriaUser(aMecanographicNumber, "OU");

        assertEquals(aCafeteriaUser, anotherCafeteriaUser);
    }

    @Test
    public void ensureCafeteriaUserEqualsFailsForDifferenteMecanographicNumber() {
        final Set<Role> roles = new HashSet<>();
        roles.add(CafeteriaRoles.ADMIN);

        final CafeteriaUser aCafeteriaUser = CafeteriaUsersTestUtil.dummyCafeteriaUser(aMecanographicNumber,
                "OU");
        final CafeteriaUser anotherCafeteriaUser = CafeteriaUsersTestUtil
                .dummyCafeteriaUser(anotherMecanographicNumber, "OU");

        final boolean expected = aCafeteriaUser.equals(anotherCafeteriaUser);

        assertFalse(expected);
    }

    @Test
    public void ensureCafeteriaUserEqualsAreTheSameForTheSameInstance() {
        final CafeteriaUser aCafeteriaUser = new CafeteriaUser();

        final boolean expected = aCafeteriaUser.equals(aCafeteriaUser);

        assertTrue(expected);
    }

    @Test
    public void ensureCafeteriaUserIsTheSameAsItsInstance() {
        final CafeteriaUser aCafeteriaUser = CafeteriaUsersTestUtil.dummyCafeteriaUser(aMecanographicNumber,
                "OU");

        final boolean expected = aCafeteriaUser.sameAs(aCafeteriaUser);

        assertTrue(expected);
    }

    @Test
    public void ensureTwoCafeteriaUserWithDifferentMecanographicNumbersAreNotTheSame() {
        final Set<Role> roles = new HashSet<>();
        roles.add(CafeteriaRoles.ADMIN);
        final CafeteriaUser aCafeteriaUser = CafeteriaUsersTestUtil.dummyCafeteriaUser(aMecanographicNumber,
                "OU");

        final CafeteriaUser anotherCafeteriaUser = CafeteriaUsersTestUtil
                .dummyCafeteriaUser(anotherMecanographicNumber, "OU");

        final boolean expected = aCafeteriaUser.sameAs(anotherCafeteriaUser);

        assertFalse(expected);
    }

    @Test
    public void ensureTwoCafeteriaUsersWithDifferentSystemUsersAreNotTheSame() {
        final CafeteriaUser aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(CafeteriaUsersTestUtil.dummyOU("OU"))
                .withMecanographicNumber("DUMMY")
                .withSystemUser(CafeteriaUsersTestUtil.dummyUser("one-dummy", CafeteriaRoles.ADMIN))
                .build();

        final CafeteriaUser anotherCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(CafeteriaUsersTestUtil.dummyOU("OU")).withMecanographicNumber("DUMMY")
                .withSystemUser(CafeteriaUsersTestUtil.dummyUser("two-dummy", CafeteriaRoles.ADMIN))
                .build();

        final boolean expected = aCafeteriaUser.sameAs(anotherCafeteriaUser);

        assertFalse(expected);
    }

    @Test
    public void ensureTwoCafeteriaUsersWithDifferentOrganicUnitsAreNotTheSame() {
        boolean expected = false;

        final CafeteriaUser aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(
                        new OrganicUnit(Designation.valueOf("one-dummy"), "dummy", "dummy"))
                .withMecanographicNumber("DUMMY")
                .withSystemUser(CafeteriaUsersTestUtil.dummyUser("admin1", CafeteriaRoles.ADMIN)).build();

        final CafeteriaUser anotherCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(
                        new OrganicUnit(Designation.valueOf("two-dummy"), "dummy", "dummy"))
                .withMecanographicNumber("DUMMY")
                .withSystemUser(CafeteriaUsersTestUtil.dummyUser("admin2", CafeteriaRoles.ADMIN)).build();

        expected = aCafeteriaUser.sameAs(anotherCafeteriaUser);

        assertFalse(expected);
    }

    @Test
    public void ensureCafeteriaUserHasAccountCard() {
        final CafeteriaUser aCafeteriaUser = new CafeteriaUserBuilder()
                .withOrganicUnit(new OrganicUnit(Designation.valueOf("dummy"), "dummy", "dummy"))
                .withMecanographicNumber("DUMMY")
                .withSystemUser(CafeteriaUsersTestUtil.dummyUser("admin1", CafeteriaRoles.ADMIN)).build();

        final boolean expected = aCafeteriaUser.card() != null;

        assertTrue(expected);
    }
}
