# Cafeteria Management

           _____       __     _            _
          / ____|     / _|   | |          (_)
      ___| |     __ _| |_ ___| |_ ___ _ __ _  __ _
     / _ \ |    / _` |  _/ _ \ __/ _ \ '__| |/ _` |
    |  __/ |___| (_| | ||  __/ ||  __/ |  | | (_| |
     \___|\_____\__,_|_| \___|\__\___|_|  |_|\__,_|


Engenharia de Aplicações (EAPLI)

Polythecnic of Porto, School of Engineering

---------------------------------------------

This application is part of the lab project for the course unit EAPLI. Parts of 
the application were developed to show specific approaches or techniques; as such, 
the overall application is not consistent in terms of design. for production ready 
code this would obvisously be a problem as we should strive for consistency. In 
this case, it is acceptable as the inconsistencies are meant to provide samples 
of different valid approaches.

_eCafeteria logo created with [kammerl ascii signature](https://www.kammerl.de/ascii/AsciiSignature.php) using font "big"_

## Who do I talk to?

Paulo Gandra de Sousa [pag@isep.ipp.pt](emailto:pag@isep.ipp.pt) / [pagsousa@gmail.com](emailto:pagsousa@gmail.com)

## License and copyright

Copyright (c) 2013-2024 the original author or authors.

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Build

Make sure Maven is installed and on the PATH.

The java source is Java 11 so any JDK 11 or later will work. However, some dependencies impose JDK17. in order to generate the javadoc and UML diagrams the JDK version must be *strictly 1.8*.

If using an Oracle database, you will need to change your maven settings for 
downloading the Oracle drivers. see <https://blogs.oracle.com/dev2dev/entry/how_to_get_oracle_jdbc#settings> for more information.

run script 

    rebuild-all.bat

## Running

make sure a JRE is installed and on the PATH

run script 

    run-backoffice 

or 

    run-user.bat

## Project Structure

_see the documentation folder_

apps

- presentation using console
- Main class
- application properties in resource folder

bootstrap app

- simple app to bootstrap the database and execute a basic end to end "smoke" test.

integrationtests

- simple spring boot junit for runing integration tests

daemon

- server application using sockets
- the client application is the `kiosk` app

core

- use case controllers, model, and repository interfaces

infrastructure

- general infrastructure issues, e.g., roles

persistence

- repository implementation (JPA, in memory, spring data)

## Use cases

_see the documentation folder_

Backoffice > Kitchen 
- Add Material 
- List Material 

Backoffice > Chef 
- Add dish type 
- Edit dish type 
- Deactivate dish type 
- List dish types 
- Add dish 
- List dish 
- Add dish (DTO) 
- List dish (DTO) 
- Edit dish > nutricional info 
- Edit dish > price 
- Deactivate dish 
- Register meal 
- List meal

Backofffice > Reporting 
- Dishes per type 
- High calories dishes 
- Dishes per caloric category 

Backoffice > Admin 
- Add user 
- List users 
- Deactivate user 
- Approve new user 

User 
- Signup 
- List movements 
- Book a meal 
- List my bookings 

POS 
- Recharge user account

Domain objects or DTOs
- Add dish vs. Add dish (DTO)
- List dish vs List dish (DTO)

Changing an attribute (value object) of an entity
- Edit dish > nutricional info
- Edit dish > price

Reporting
- Dishes per type
- High calories dishes
- Dishes per caloric category

Transactional control
- Approve new user

Pub/Sub
- Approve new user

## Architecture

The main architectural style is Hexagonal/Onion.

### Domain objects with persistence knowledge or not

Two different approaches are possible:

- pure domain objects without any knowledge of the persistence
- domain objects that can save and load thenselves from persistence (thus, an Active Record)

In the first case, the controller is responsible for obtaining the domain objects from the 
repository, asking the domain objects to perform the business logic and then pass them 
back to the repository. in this case, the domain objects can "easily" be tested as they 
do not depend on any other package this gets trickier when we need/want to have lazy 
load of collections...

In the second case, the controller asks the domain object class to load a certain 
instance, asks that object to perform the business operation and then asks the 
object to save itself back to the database

### Passing domain objects to the UI or not

The decision is to use domain objects outside of the controllers boundary. One could argue 
that domain objects should be known only "inside" the application boundary and as such other
 data structures should be returned to outside layers, i.e., DTO (Data Transfer Objects).

### Performing calculations in memory or directly at the persistence layer

Both approaches have advantages and disavantages:

- in memory

  - advantages

    - allows the use of business logic in code
    - disavantages performance may be poor

- at persistence layer

  - advantages

    - use of aggregated SQL functions is straigth forward
    - performance

  - disavantages

    - complicated business logic is hard to implement

//TODO provide one example of each approach.

See also <http://www.martinfowler.com/articles/dblogic.html>

### Factoring out common behaviour

use services at the application or domain layer

### Can controllers call other controllers?

it is best if they call application services

### Should the UI/controller create domain objects directly

Should the rules for the Creator pattern be fully enforced, e.g., the responsibility 
to create a Payment should be of Expense, or can the controller/UI create a Payment 
and pass it to the Expense?

### How to reuse behavior betwen controllers

Factor out common behaviour in an application service.

### When showing movements grouped by type, who performs the sum operation? UI, Controller or Domain object?

the UI might not be smart enough to compute the total sum with enough precision, 
and would carry a burden for the computer running the interface

the Controller might indeed perform such calculation as it has all the data is needs 
for a short period of time, but it is not the controller function to perform mathematical 
operations.

the domain object might indeed be the very best resource to calculate the sum for each 
expense type, but it would not make sense to delegate the domain object to the interface.

### When showing movements grouped by type, should types with no values also appear?

this can be another strategy

## References and bibliography

Start by reading the essential material listed in [EAPLI framework](https://bitbucket.org/pag_isep/eapli.framework/src/master/README.md)

### JPA

- [Entities or DTOs in JPA Queries](https://thoughts-on-java.org/entities-dtos-use-projection/)
- [Primary key mapping](https://thoughts-on-java.org/primary-key-mappings-jpa-hibernate/)
- [equals() and hashCode()](https://thoughts-on-java.org/ultimate-guide-to-implementing-equals-and-hashcode-with-hibernate/)

### Spring boot readings

Starting points on Spring:

- [Baeldung's springboot tutorial](https://www.baeldung.com/spring-boot-start)
- The complete [Baeldung's Learn Spring Boot](https://www.baeldung.com/spring-boot)
- [mkyong's tutorials](https://www.mkyong.com/tutorials/spring-boot-tutorials/)
- The [Spring.io guides](https://spring.io/guides)
- And the [Spring boot reference](https://docs.spring.io/spring-boot/docs/current/reference/html/)

Fundamental reading used in the scope of this project:

- [Spring boot multi-module projects](https://www.baeldung.com/spring-boot-multiple-modules)
- [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
- [Spring boot console application](https://www.baeldung.com/spring-boot-console-app)
- [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
- [Testing in spring Boot](https://www.baeldung.com/spring-boot-testing)

The following guides illustrate how to use some features concretely:

- [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
- [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
- [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
- [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
- [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)
- [custom banner](https://www.baeldung.com/spring-boot-custom-banners)

### Other useful readings

T.B.D.
