/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.user.console.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.components.authz.console.presentation.MyUserMenuBuilder;
import eapli.ecafeteria.app.components.meals.console.presentation.ListMenuUI;
import eapli.ecafeteria.app.user.console.presentation.mybookings.BookingPrinter;
import eapli.ecafeteria.app.user.console.presentation.mybookings.MovementPrinter;
import eapli.ecafeteria.app.user.console.presentation.mybookings.RegisterBookingUI;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealbooking.application.MyBookingsController;
import eapli.ecafeteria.sales.application.ListMovementsController;
import eapli.framework.actions.Action;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ListUI;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * @author Paulo Gandra Sousa
 */
@Component
class MainMenu extends CafeteriaUserBaseUI {

    private static final String SEPARATOR_LABEL = "--------------";

    private static final String RETURN = "Return ";

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int BOOKINGS_OPTION = 2;
    private static final int ACCOUNT_OPTION = 3;
    private static final int SETTINGS_OPTION = 4;

    // BOOKINGS MENU
    private static final int LIST_MENUS_OPTION = 1;
    private static final int BOOK_A_MEAL_OPTION = 2;
    private static final int LIST_MY_BOOKINGS_OPTION = 3;

    // ACCOUNT MENU
    private static final int LIST_MOVEMENTS_OPTION = 1;

    // SETTINGS
    private static final int SET_USER_ALERT_LIMIT_OPTION = 1;

    @Autowired
    private AuthorizationService authz;

    @Autowired
    private MyUserMenuBuilder myUserMenuBuilder;

    @Autowired
    private ListMenuUI listMenuUI;

    @Autowired
    private MyBookingsController myBookingsController;

    @Autowired
    private ListMovementsController listMovementsController;

    @Autowired
    private RegisterBookingUI registerBookingUI;

    private MenuRenderer renderer;

    @Override
    public boolean show() {
        renderer = buildRenderer(buildMainMenu());
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        return renderer.render();
    }

    private MenuRenderer buildRenderer(final Menu menu) {
        return new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = myUserMenuBuilder.withRole(CafeteriaRoles.CAFETERIA_USER);
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);
        mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        mainMenu.addSubMenu(BOOKINGS_OPTION, buildBookingsMenu());
        mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        mainMenu.addSubMenu(ACCOUNT_OPTION, buildAccountMenu());
        mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        mainMenu.addSubMenu(SETTINGS_OPTION, buildAdminSettingsMenu());
        mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildAccountMenu() {
        final Action listMovementsAction = () -> {
            final Username user = authz.session().orElseThrow(IllegalStateException::new).authenticatedUser()
                    .identity();
            final AbstractUI listUI = new ListUI<>(
                    () -> listMovementsController.allMovementsByUsername(user),
                    new MovementPrinter(), "Movement",
                    String.format("#  %-15s%-15s%-10s", "ON", "TYPE", "AMOUNT"), "List Movements",
                    "No data.");
            listUI.show();
            return true;
        };

        final Menu accountMenu = new Menu("Account");
        accountMenu.addItem(LIST_MOVEMENTS_OPTION, "List movements", listMovementsAction);
        accountMenu.addItem(EXIT_OPTION, RETURN, Actions.SUCCESS);
        return accountMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu adminSettingsMenu = new Menu("Settings >");

        adminSettingsMenu.addItem(SET_USER_ALERT_LIMIT_OPTION, "Set users' alert limit",
                new ShowMessageAction("Not implemented yet"));
        adminSettingsMenu.addItem(EXIT_OPTION, RETURN, Actions.SUCCESS);

        return adminSettingsMenu;
    }

    private Menu buildBookingsMenu() {
        final Menu bookingsMenu = new Menu("Bookings");
        bookingsMenu.addItem(LIST_MENUS_OPTION, "List menus", listMenuUI::show);
        bookingsMenu.addItem(BOOK_A_MEAL_OPTION, "Book a meal", () -> registerBookingUI.show());
        bookingsMenu.addItem(LIST_MY_BOOKINGS_OPTION, "My Bookings", () -> {
            new ListUI<>(() -> myBookingsController.myBookings(), new BookingPrinter(), "Booking",
                    String.format("   %-40s %-15s %s", "TOKEN", "DAY", "STATUS"), "List My Bookings",
                    "No data.").show();
            return true;
        });
        bookingsMenu.addItem(EXIT_OPTION, RETURN, Actions.SUCCESS);
        return bookingsMenu;
    }
}
