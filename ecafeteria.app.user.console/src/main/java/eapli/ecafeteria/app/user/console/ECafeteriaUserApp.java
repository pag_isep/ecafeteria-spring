/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.app.user.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import eapli.ecafeteria.app.components.base.console.presentation.ECafeteriaBaseApplication;
import eapli.ecafeteria.app.user.console.presentation.FrontMenu;
import eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers.MealBookedWatchDog;
import eapli.ecafeteria.mealbooking.domain.model.BookedEvent;
import eapli.framework.infrastructure.pubsub.EventDispatcher;

/**
 * eCafeteria User Application.
 *
 * @author Paulo Gandra de Sousa
 */
@SpringBootApplication
@ComponentScan(basePackages = { "eapli.ecafeteria.spring" })
@SuppressWarnings("squid:S106")
public class ECafeteriaUserApp extends ECafeteriaBaseApplication {

	@Autowired
	private MealBookedWatchDog mealBookedWatchDog;

	@Autowired
	private FrontMenu frontMenu;

	public static void main(final String[] args) {
		SpringApplication.run(ECafeteriaUserApp.class, args);
	}

	@Override
	protected void doMain(final String[] args) {
		boolean wantsToExit;
		do {
			wantsToExit = frontMenu.show();
		} while (!wantsToExit);
	}

	@Override
	protected String appTitle() {
		return "eCafeteria User App";
	}

	@Override
	protected String appGoodbye() {
		return "eCafeteria User App";
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
		// typically there would be this watch dog here as this would be handled by a
		// backend server, but since we are for now dealing only with console
		// application clients we will do it here
		dispatcher.subscribe(mealBookedWatchDog, BookedEvent.class);
	}
}
