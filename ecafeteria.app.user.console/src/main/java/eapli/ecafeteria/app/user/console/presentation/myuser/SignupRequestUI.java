/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.user.console.presentation.myuser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.mycafeteriauser.application.SignupController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class SignupRequestUI extends AbstractUI {
	private static final Logger LOGGER = LogManager.getLogger(SignupRequestUI.class);

	@Autowired
	private SignupController theController;

	@Override
	protected boolean doShow() {
		final var userData = new UserDataWidget();

		userData.show();

		final var selector = new SelectWidget<>("Select a signup request", theController.organicUnits(),
				new OrganicUnitPrinter());
		selector.show();

		final var organicUnit = selector.selectedElement();

		final var mecanographicNumber = Console.readLine("Mecanographic Number");

		try {
			theController.signup(userData.username(), userData.password(), userData.firstName(), userData.lastName(),
					userData.email(), organicUnit, mecanographicNumber);

			System.out.println("You request was recorded. Please wait for a confirmation email");
		} catch (final IllegalArgumentException e) {
			System.out.println("There is a problem with your input. Please retry");
		} catch (final IntegrityViolationException e) {
			System.out.println("There is already a signup request for this user");
		} catch (final ConcurrencyException e) {
			LOGGER.error("This should never happen", e);
			System.out.println(
					"Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
		}

		return false;
	}

	@Override
	public String headline() {
		return "Sign Up";
	}
}
