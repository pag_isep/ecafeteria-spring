/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealplanning.domain.model;

import java.util.Calendar;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Version;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.time.domain.model.DateInterval;
import eapli.framework.validations.Preconditions;

/**
 * A Meal Plan is a view over a set of Meal Plan Items between two dates and it
 * is defined by two dates and a name.
 * <p>
 * Each Item is a meal proposed by a specific menu for a certain period of time
 * e.g., a week or a day, added with a set of quantities like: planned meals;
 * cooked meals;
 * <p>
 * There are no explicit references between MealPlan and MealPlanItem
 * <p>
 * Different plans can overlap their time periods meaning that a mealPlanItem
 * can appear in multiple plans For instance one working the in kitchen can use
 * a two day plan and other person working in provisioning use a two week plan
 *
 * @author ajs
 */
@Entity
public class MealPlan implements AggregateRoot<Designation> {

	private static final String NOT_IMPLEMENTED_YET = "Not implemented yet!";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)
	private Long pk;

	@Version
	private Long version;

	// The name of the plan can be proposed "automagically"
	// for instance Plan of Week XX
	@Embedded
	@Column(unique = true, nullable = false)
	private Designation name;

	@Embedded
	private DateInterval timePeriod;

	// FIXME
	// @ManyToOne(optional = false)
	private transient Cafeteria cafeteria;

	protected MealPlan() {
		// for ORM
	}

	public MealPlan(final Cafeteria cafeteria, final Designation name, final Calendar beginDate,
			final Calendar endDate) {
		Preconditions.noneNull(cafeteria, beginDate, endDate, name);

		this.cafeteria = cafeteria;
		this.name = name;
		timePeriod = new DateInterval(beginDate, endDate);
	}

	@Override
	public boolean equals(final Object o) {
		return DomainEntities.areEqual(this, o);
	}

	@Override
	public int hashCode() {
		return DomainEntities.hashCode(this);
	}

	@Override
	public Designation identity() {
		return name;
	}

	@Override
	public boolean sameAs(final Object other) {
		if (!(other instanceof MealPlan)) {
			return false;
		}

		final var that = (MealPlan) other;
		if (this == that) {
			return true;
		}

		return identity().equals(that.identity());
		// TODO check other fields
	}

	public Designation name() {
		return name;
	}

	public Calendar beginDate() {
		return timePeriod.start();
	}

	public Calendar endDate() {
		return timePeriod.end();
	}

	public void cancelBooking(final Meal meal) {
		throw new UnsupportedOperationException(NOT_IMPLEMENTED_YET);
	}

	public void registerBooking(final Meal meal) {
		throw new UnsupportedOperationException(NOT_IMPLEMENTED_YET);
	}

	public void deliverMeal(final Meal meal) {
		throw new UnsupportedOperationException(NOT_IMPLEMENTED_YET);
	}

	public void registerNotDeliveredMeals(final Meal meal) {
		throw new UnsupportedOperationException(NOT_IMPLEMENTED_YET);
	}
}
