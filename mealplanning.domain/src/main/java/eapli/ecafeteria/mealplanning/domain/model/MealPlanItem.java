/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealplanning.domain.model;

import java.io.Serializable;
import java.util.Calendar;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Version;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;

/**
 * Each MealPlanItem is a meal proposed by a specific menu for a certain period
 * of time e.g., a week or a day, added with a set of quantities like: planned
 * meals; cooked meals;
 * <p>
 * There are no explicit references between MealPlan and MealPlanItem
 *
 * @FIXME this class overlaps with CookedMealQty
 * @author ajs
 */
@Entity
public class MealPlanItem implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;

    @Version
    private Long version;

    // TODO unique constraint as this is the business identity
    private Long identity;

    @OneToOne(optional = false)
    private Meal meal;

    // TODO use a value object class instead of int?
    private int plannedMeals;
    private int cookedMeals;
    private int bookedMeals;
    private int deliveredMeals;
    private int notDeliveredMeals;

    protected MealPlanItem() {
        // for ORM
    }

    public MealPlanItem(final Meal meal, final int plannedMeals) {
        Preconditions.nonNull(meal);
        Preconditions.isPositive(plannedMeals);

        identity = meal.identity();
        this.meal = meal;
        this.plannedMeals = plannedMeals;
        cookedMeals = 0;
        bookedMeals = 0;
        deliveredMeals = 0;
        notDeliveredMeals = 0;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    public MealType mealType() {
        return mealType();
    }

    @Override
    public Long identity() {
        return identity;
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof MealPlanItem)) {
            return false;
        }

        final MealPlanItem that = (MealPlanItem) other;
        if (this == that) {
            return true;
        }

        return pk.equals(that.pk);
        // TODO check other fields
    }

    public Calendar ofDay() {
        return meal.day();
    }

    public int plannedMeals() {
        return plannedMeals;
    }

    public void setPlannedMeals(final int plannedMeals) {
        Preconditions.isPositive(plannedMeals);

        this.plannedMeals = plannedMeals;
    }

    public void cancelBooking() {
        // FIXME validate invariants, e.g., bookedMeals > 0
        if (plannedMeals <= 0) {
            throw new IllegalStateException();
        }

        bookedMeals--;
    }

    public void registerBooking() {
        // FIXME validate invariants
        bookedMeals++;
    }

    public void deliverMeal() {
        // FIXME validate invariants, e.g., deliveredMeals < cookedMeals
        deliveredMeals++;
    }

    public void setNotDeliveredMeals() {
        if (bookedMeals < deliveredMeals) {
            throw new IllegalStateException();
        }

        notDeliveredMeals = bookedMeals - deliveredMeals;
    }

    public Dish dish() {
        return meal.dish();
    }
}
