/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.orgmanagement.domain.model;

import java.io.Serializable;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Version;

import com.fasterxml.jackson.annotation.JsonGetter;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.validations.Preconditions;

/**
 * An organic unit, i.e., a cafeteria at a specific location in the
 * organization.
 *
 * @author arocha
 */
@Entity
public class OrganicUnit implements AggregateRoot<Designation>, Serializable {

    private static final long serialVersionUID = 1L;

    // database primary key
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;

    @Version
    private Long version;

    // business identity
    @Column(unique = true, nullable = false)
    @AttributeOverride(name = "name", column = @Column(name = "acronym"))
    private Designation acronym;

    @Column(nullable = false)
    private String name;

    // TODO make this attribute optional
    @Column(nullable = false)
    private String description;

    private boolean active;

    protected OrganicUnit() {
        // for ORM
    }

    public OrganicUnit(final Designation acronym, final String name, final String description) {
        Preconditions.nonNull(acronym);
        Preconditions.nonEmpty(name);
        Preconditions.nonEmpty(description);

        this.acronym = acronym;
        this.name = name;
        this.description = description;
        active = true;
    }

    @Override
    public Designation identity() {
        return acronym;
    }

    public boolean isActive() {
        return active;
    }

    @JsonGetter
    public String name() {
        return name;
    }

    @JsonGetter
    public String description() {
        return description;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof OrganicUnit)) {
            return false;
        }

        final OrganicUnit that = (OrganicUnit) other;
        if (this == that) {
            return true;
        }

        return acronym.equals(that.acronym) && name.equals(that.name)
                && description.equals(that.description)
                && active == that.active;
    }

    @Override
    public String toString() {
        return acronym.toString();
    }
}
