/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.orgmanagement.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Version;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.validations.Preconditions;

/**
 * A Cafeteria.
 *
 * @author Paulo Gandra Sousa
 *
 */
@Entity
@SuppressWarnings("squid:S2160")
public class Cafeteria implements AggregateRoot<CafeteriaName> {

    private static final long serialVersionUID = -6763256902584926321L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;
    @Version
    private Long version;

    // @EmbeddedId
    @Column(nullable = false)
    private CafeteriaName name;

    private String description;

    @ManyToOne(optional = false)
    private OrganicUnit organicUnit;

    protected Cafeteria() {
        // for ORM
    }

    public Cafeteria(final OrganicUnit unit, final Designation name, final String description) {
        Preconditions.noneNull(name, unit);

        this.name = new CafeteriaName(unit.identity(), name);
        this.description = description;
        organicUnit = unit;
    }

    public Cafeteria(final OrganicUnit unit, final Designation name) {
        this(unit, name, null);
    }

    @Override
    public boolean sameAs(final Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public CafeteriaName identity() {
        return name;
    }

    public String description() {
        return description;
    }

    public OrganicUnit unit() {
        return organicUnit;
    }

    public CafeteriaName name() {
        return name;
    }
}
