/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.orgmanagement.domain.model;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.validations.Preconditions;
import lombok.Value;
import lombok.experimental.Accessors;

/**
 * A value object to capture the concept of a cafeteria's name; a compound of
 * organic unit + name.
 *
 * @author Paulo Gandra Sousa
 *
 */
@Embeddable
@Value
@Accessors(fluent = true)
public class CafeteriaName implements ValueObject, Comparable<CafeteriaName> {

    private static final long serialVersionUID = 1L;

    @Embedded
    @AttributeOverride(name = "name", column = @Column(name = "unit_name"))
    private final Designation unit;
    @Embedded
    @AttributeOverride(name = "name", column = @Column(name = "cafe_name"))
    private final Designation cafe;

    protected CafeteriaName() {
        // for ORM
        unit = null;
        cafe = null;
    }

    public CafeteriaName(final OrganicUnit ou, final Designation name) {
        Preconditions.nonNull(ou);
        Preconditions.nonNull(name, "Cafeterias' name cannot be empty neither null");

        unit = ou.identity();
        cafe = name;
    }

    public CafeteriaName(final Designation ou, final Designation name) {
        Preconditions.nonNull(ou);
        Preconditions.nonNull(name, "Cafeterias' name cannot be empty neither null");

        unit = ou;
        cafe = name;
    }

    /**
     * Factory method. Works the opposite way of {@link #toString()}.
     *
     * @param name
     * @return a cafeteria name object
     */
    public static CafeteriaName valueOf(final String name) {
        final String[] tokens = name.split("/");
        Preconditions.ensure(tokens.length == 2, "Invalid format");
        return new CafeteriaName(Designation.valueOf(tokens[0]), Designation.valueOf(tokens[1]));
    }

    @Override
    public String toString() {
        return unit() + "/" + cafe();
    }

    @Override
    public int compareTo(final CafeteriaName o) {
        return toString().compareTo(o.toString());
    }
}
