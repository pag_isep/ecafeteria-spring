/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.orgmanagement.domain.model;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import eapli.framework.general.domain.model.Designation;

/**
 *
 * @author arocha
 */
class OrganicUnitTest {

	@Test
	void ensureAcronymMustNotBeNull() {
		assertThrows(IllegalArgumentException.class, () -> new OrganicUnit(null, "Instituto", "Porto"));
	}

	@Test
	void ensureNameMustNotBeNull() {
		assertThrows(IllegalArgumentException.class, () -> new OrganicUnit(Designation.valueOf("ISEP"), null, "Porto"));
	}

	@Test
	void ensureNameMustNotBeBlank() {
		assertThrows(IllegalArgumentException.class, () -> new OrganicUnit(Designation.valueOf("ISEP"), "", "Porto"));
	}

	@Test
	void ensureDescriptionMustNotBeNull() {
		assertThrows(IllegalArgumentException.class,
				() -> new OrganicUnit(Designation.valueOf("ISEP"), "Instituto", null));
	}

	@Test
	void ensureDescriptionMustNotBeBlank() {
		assertThrows(IllegalArgumentException.class,
				() -> new OrganicUnit(Designation.valueOf("ISEP"), "Instituto", ""));
	}

	/**
	 * Test of is method, of class OrganicUnit.
	 */
	@Test
	void testIs() {
		System.out.println("Attest 'is' method - Normal Behaviour");
		final var id = Designation.valueOf("ISEP");
		final var instance = new OrganicUnit(id, "Instituto", "Porto");
		assertTrue(instance.hasIdentity(id));
	}
}
