/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.infrastructure.repositories.impl.springdata.SpringDataBaseRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface SpringDataMealRepository extends MealRepository, SpringDataBaseRepository<Meal, Long> {

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria = :cafeteria AND e.day = :ofDay AND e.mealType = :forMeal")
    Iterable<Meal> findByDay(@Param("cafeteria") Cafeteria cafeteria, @Param("ofDay") Calendar forDay,
            @Param("forMeal") MealType forMeal);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria.name = :cafeteria AND e.day = :ofDay AND e.mealType=:forMeal")
    Iterable<Meal> findByDayAndType(@Param("cafeteria") CafeteriaName where, @Param("ofDay") Calendar forDay,
            @Param("forMeal") MealType forMeal);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria = :cafeteria AND e.day = :ofDay")
    Iterable<Meal> findByDay(@Param("cafeteria") Cafeteria cafeteria, @Param("ofDay") Calendar ofDay);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria = :cafeteria AND e.day BETWEEN :beginDate AND :endDate")
    Iterable<Meal> findByPeriod(@Param("cafeteria") Cafeteria cafeteria, @Param("beginDate") Calendar beginDate,
            @Param("endDate") Calendar endDate);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria = :cafeteria AND e.day = :ofDay AND e.mealType = :mealType AND e NOT IN (SELECT c.meal FROM CookedMealQty c)")
    Iterable<Meal> findNotYetCookedByDay(@Param("cafeteria") Cafeteria cafeteria, @Param("ofDay") Calendar ofDay,
            @Param("mealType") MealType mealType);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.menu.cafeteria = :cafeteria")
    Iterable<Meal> findByCafeteria(@Param("cafeteria") final Cafeteria cafeteria);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.id = :id")
    Optional<Meal> ofIdentity(@Param("id") Long entityId);

    @Override
    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.id = :id")
    void deleteOfIdentity(@Param("id") Long entityId);
}
