/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerDishType;
import eapli.ecafeteria.reporting.dishes.repositories.DishReportingRepository;
import eapli.framework.general.domain.model.Designation;

/**
 *
 * @author Paulo Gandra Sousa
 */
@RepositoryDefinition(domainClass = Dish.class, idClass = Designation.class)
public interface SpringDataDishReportingRepository
        extends DishReportingRepository, SpringDataDishReportingRepositoryCustom {

    @Override
    @Query("SELECT NEW eapli.ecafeteria.reporting.dishes.dto.DishesPerDishType(t.acronym, COUNT(d)) FROM Dish d, DishType t WHERE d.dishType = t GROUP BY t.acronym")
    Iterable<DishesPerDishType> reportDishesPerDishType();

    @Override
    @Query("SELECT d FROM Dish d WHERE d.nutricionalInfo.calories > 350")
    Iterable<Dish> reportHighCaloriesDishes();

    @Override
    @Query(value = Queries.DISHES_PER_CALORIC_CATEGORY, nativeQuery = true, countQuery = Queries.COUNT_DISHES_PER_CALORIC_CATEGORY)
    Iterable<Object[]> reportDishesPerCaloricCategoryAsTuples();
}
