/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.model.UsedMaterialBatch;
import eapli.ecafeteria.traceability.domain.repositories.BatchSearchReportingRepository;
import eapli.framework.domain.repositories.ReportingRepository;

/**
 * @author Paulo Gandra de Sousa
 *
 */
@RepositoryDefinition(domainClass = UsedMaterialBatch.class, idClass = Long.class)
@ReportingRepository
public interface SpringDataBatchSearchReportingRepository extends BatchSearchReportingRepository {

    /**
     */
    @Override
    @Query("SELECT b FROM Booking b, UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn AND e.meal = b.meal")
    Iterable<Booking> bookingsOfMealsWhichUsedBatch(@Param("m") Material material,
            @Param("bn") BatchNumber batchNumber);

    /**
     * BATCH SEARCH
     *
     */
    @Override
    @Query("SELECT b.user FROM Booking b, UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn AND e.meal = b.meal")
    Iterable<CafeteriaUser> whoConsumedMealsWhichUsedBatch(@Param("m") Material material,
            @Param("bn") BatchNumber batchNumber);

    /**
     *
     * @param material
     * @param batchNumber
     * @return
     */
    @Override
    @Query("SELECT e.meal FROM UsedMaterialBatch e WHERE e.material = :m AND e.batchNumber = :bn")
    Iterable<Meal> mealsWhichUsedBatch(@Param("m") Material material,
            @Param("bn") BatchNumber batchNumber);
}
