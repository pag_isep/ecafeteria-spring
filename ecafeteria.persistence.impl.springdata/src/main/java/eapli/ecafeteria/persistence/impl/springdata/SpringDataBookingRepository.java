/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealbooking.domain.repositories.BookingRepository;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.infrastructure.repositories.impl.springdata.SpringDataBaseRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface SpringDataBookingRepository extends BookingRepository, SpringDataBaseRepository<Booking, Long> {

	@Override
	@Query("SELECT e FROM #{#entityName} e WHERE e.user.systemUser.username = ?1")
	Iterable<Booking> findByUsername(Username who);

	@Override
	@Query("SELECT e FROM #{#entityName} e WHERE e.user.mecanographicNumber = ?1")
	Iterable<Booking> findByMecanographicNumber(MecanographicNumber who);

	@Override
	@Query("SELECT e FROM #{#entityName} e WHERE e.meal.menu.cafeteria.name = :cafe AND e.meal.day = :day AND e.meal.mealType = :type")
	Iterable<Booking> findByCafe(@Param("cafe") CafeteriaName cafe, @Param("day") Calendar day,
			@Param("type") MealType type);

	@Override
	@Query("SELECT e FROM #{#entityName} e WHERE e.token = :id")
	Optional<Booking> ofIdentity(@Param("id") BookingToken entityId);

	@Override
	@Modifying
	@Query("DELETE FROM #{#entityName} e WHERE e.token = :id")
	void deleteOfIdentity(@Param("id") BookingToken entityId);

	@Override
	@Query("SELECT e FROM #{#entityName} e WHERE e.user = :user AND e.meal.day = :day AND e.meal.mealType = :type")
	Optional<Booking> findByUserForADayAndMealType(@Param("user") CafeteriaUser user, @Param("day") Calendar day,
			@Param("type") MealType mealType);
}
