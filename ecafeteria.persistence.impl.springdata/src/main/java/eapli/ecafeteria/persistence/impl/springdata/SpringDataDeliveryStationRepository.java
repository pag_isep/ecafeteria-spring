/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.deliverystationmanagement.domain.repositories.DeliveryStationRepository;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.repositories.impl.springdata.SpringDataBaseRepository;

/**
 *
 * @author Paulo Gandra de Sousa
 */
public interface SpringDataDeliveryStationRepository
        extends SpringDataBaseRepository<DeliveryStation, Long>, DeliveryStationRepository {

    @Override
    @Query("SELECT e.station FROM DeliveryStationWorkSession e WHERE e.state = eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStationState.OPEN AND e.day = :day AND e.mealType = :mealType")
    Iterable<DeliveryStation> findOpenWorkStations(@Param("day") Calendar day,
            @Param("mealType") MealType mealType);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.cafeteria.name = :cafe AND e.acronym = :acronym")
    Optional<DeliveryStation> findByCafeteriaAndAcronym(@Param("cafe") CafeteriaName cafe,
            @Param("acronym") Designation acronym);

    @Override
    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.id = :id")
    void deleteOfIdentity(@Param("id") Long entityId);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.id = :id")
    Optional<DeliveryStation> ofIdentity(@Param("id") Long entityId);
}
