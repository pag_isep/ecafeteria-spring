/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import eapli.framework.util.Utility;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Utility
class Queries {

    /**
     * We are using a native query here as JPQL does not support SELECT FROM
     * sub-query
     */
    public static final String DISHES_PER_CALORIC_CATEGORY = "SELECT caloricCategory, COUNT(*) as n "
            + "FROM (SELECT *, CASE WHEN calories <= 150 THEN 'low' WHEN calories > 150 AND calories < 350 THEN 'medium' ELSE 'high' END AS caloricCategory FROM DISH) x "
            + "GROUP BY caloricCategory";
    // workaround for NPE in SpringBoot 2.7.9 with native queries
    public static final String COUNT_DISHES_PER_CALORIC_CATEGORY = "select 3 FROM (select count(*) from T_SYSTEMUSER)";

    /**
     * ensure utility
     */
    private Queries() {

    }
}
