/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.math.BigInteger;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import eapli.ecafeteria.reporting.dishes.dto.DishesPerCaloricCategory;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Converter(autoApply = true)
public class DishPerCaloricCategoryConverter implements AttributeConverter<DishesPerCaloricCategory, Object[]> {

    @Override
    public Object[] convertToDatabaseColumn(final DishesPerCaloricCategory arg0) {
        final Object[] col = new Object[2];
        col[0] = arg0.caloricCategory;
        col[1] = arg0.quantityOfDishes.longValue();
        return col;
    }

    @Override
    public DishesPerCaloricCategory convertToEntityAttribute(final Object[] arg0) {
        assert (arg0.length == 2);
        return new DishesPerCaloricCategory((String) arg0[0], BigInteger.valueOf((long) arg0[1]));
    }
}
