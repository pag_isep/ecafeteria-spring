/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.util.Optional;

import jakarta.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishTypeRepository;
import eapli.framework.infrastructure.repositories.impl.springdata.SpringDataBaseRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface SpringDataDishTypeRepository extends DishTypeRepository, SpringDataBaseRepository<DishType, Long> {

    /**
     * Find all dishes according to the desired status.
     * <p>
     * This method takes advantage of Spring Data convention that automatically
     * generates the query based on the method name.
     *
     * @param status
     * @return
     */
    Optional<DishType> findByActive(boolean status);

    /**
     * Find all active dishes.
     * <p>
     * For this method, since it doesn't follow spring Data convention, we need
     * to specify the exact JPQL we want to execute
     */
    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.active=true ORDER BY e.acronym ASC")
    Iterable<DishType> activeDishTypes();

    /**
     * Find all active dishes.
     *
     * <p>
     * This method takes advantage of Spring Data convention that automatically
     * generates the query based on the method name.
     *
     * @return
     */
    Iterable<DishType> findByActiveTrueOrderByAcronymAsc();

    /**
     * This method takes advantage of Spring Data convention that automatically
     * generates the query based on the method name.
     *
     * @param description
     * @return
     */
    Optional<DishType> findByDescription(String description);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.acronym = :id")
    Optional<DishType> ofIdentity(@Param("id") String entityId);

    @Override
    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.acronym = :id")
    void deleteOfIdentity(@Param("id") String entityId);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e = :entity")
    @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
    DishType lockItUp(@Param("entity") DishType entity);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.acronym = :id")
    @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
    Optional<DishType> lockOfIdentity(@Param("id") String id);
}
