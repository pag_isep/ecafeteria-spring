/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.persistence.impl.springdata;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealplanning.domain.model.MealPlanItem;
import eapli.ecafeteria.mealplanning.domain.repositories.MealPlanItemRepository;
import eapli.framework.infrastructure.repositories.impl.springdata.SpringDataBaseRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface SpringDataMealPlanItemRepository
        extends MealPlanItemRepository, SpringDataBaseRepository<MealPlanItem, Meal> {

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.meal.day BETWEEN :beginDate AND :endDate")
    Iterable<MealPlanItem> findItemsOnPeriod(@Param("beginDate") Calendar beginDate,
            @Param("endDate") Calendar endDate);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.identity = :id")
    Optional<MealPlanItem> ofIdentity(@Param("id") Long entityId);

    @Override
    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.identity = :id")
    void deleteOfIdentity(@Param("id") Long entityId);
}
