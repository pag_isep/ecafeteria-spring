/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.meals;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealmanagement.application.RegisterMenuController;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.time.util.Calendars;
import eapli.framework.io.util.Console;

/**
 *
 * @author losa
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class RegisterMenuOfMealsUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(RegisterMenuOfMealsUI.class);

    @Autowired
    private RegisterMenuController theController;

    @Override
    protected boolean doShow() {

        // FIXME select a cafeteria + OU
        final Cafeteria cafeteria = null;

        final String description = Console.readLine("Menu Description");
        final Calendar beginDay = Calendars
                .fromDate(Console.readDate("Data Início do Menu"));
        final Calendar endDay = Calendars.fromDate(Console.readDate("Data Fim do Menu"));
        try {
            theController.registerMenu(cafeteria, description, beginDay, endDay);
        } catch (final IntegrityViolationException ex) {
            System.out.println("A menu like that already exists.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Create Menu of Meals";
    }
}
