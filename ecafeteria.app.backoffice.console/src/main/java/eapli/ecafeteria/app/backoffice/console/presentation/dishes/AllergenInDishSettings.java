/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.dishes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eapli.ecafeteria.dishmanagement.domain.model.Allergen;
import eapli.framework.io.util.Console;

/**
 *
 * @author mcn
 */
@SuppressWarnings({ "squid:S106" })
public class AllergenInDishSettings {
    private final Set<Allergen> allergeansSet;
    private final List<Allergen> list;
    private int option = -1;

    public AllergenInDishSettings(final Iterable<Allergen> all) {
        allergeansSet = new HashSet<>();
        list = makeList(all);

    }

    private List<Allergen> makeList(final Iterable<Allergen> allergean) {
        final List<Allergen> result = new ArrayList<>();
        for (final Allergen item : allergean) {
            result.add(item);
        }
        return result;
    }

    private void showAllergeans() {
        int cont = 0;
        System.out.println(" CHOOSE THE ALLERGEANS IN THIS DIDH");
        System.out.println("0.  NO   MORE  ALLERGEANS");
        for (final Allergen allergean : list) {
            cont++;
            System.out.println(cont + ".  " + allergean.identity() + " " + allergean.fullName());
        }
    }

    public Set<Allergen> setAllergeansInThisDish() {
        showAllergeans();
        while (option != 0) {
            option = Console.readOption(1, list.size() - 1, 0);
            if (option != 0) {
                final Allergen elem = list.get(option - 1);
                allergeansSet.add(elem);
            }
        }
        return allergeansSet;
    }
}
