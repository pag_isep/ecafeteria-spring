/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.cafeteria;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.orgmanagement.application.AddCafeteriaController;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;

/**
 *
 * @author Paulo Gandra de Sousa
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class AddCafeteriaUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(AddCafeteriaUI.class);

    @Autowired
    private AddCafeteriaController theController;

    @Override
    protected boolean doShow() {
        final SelectWidget<OrganicUnit> selectorOU = new SelectWidget<>("Select an Organic Unit",
                theController.organicUnits(), new OrganicUnitPrinter());
        selectorOU.show();
        final OrganicUnit ou = selectorOU.selectedElement();

        final String acronym = Console.readLine("Acronym");
        final String description = Console.readLine("Description");

        try {
            theController.addCafeteria(ou, acronym, description);
        } catch (final IntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Organic Unit";
    }

}
