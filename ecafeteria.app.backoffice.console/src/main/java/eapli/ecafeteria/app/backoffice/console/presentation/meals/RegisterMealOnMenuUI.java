/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.meals;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.dishes.DishPrinter;
import eapli.ecafeteria.app.components.meals.console.presentation.MenuPrinter;
import eapli.ecafeteria.dishmanagement.application.ListDishService;
import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.mealmanagement.application.RegisterMealController;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.time.util.Calendars;

/**
 *
 * @author losa
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class RegisterMealOnMenuUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(RegisterMealOnMenuUI.class);

    @Autowired
    private RegisterMealController controller;
    @Autowired
    private ListDishService listDishService;

    @Override
    protected boolean doShow() {
        try {
            final Menu theMenu = listMenus();
            final Calendar currentDay = theMenu.timePeriod().start();
            final Dish theDish = selectDishForMealODay(theMenu);

            controller.registerMeal(MealType.LUNCH, currentDay, theDish, theMenu);
        } catch (final IntegrityViolationException ex) {
            System.out.println("There is already a meal for that day/type in that menu");
        } catch (final ConcurrencyException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }
        return false;
    }

    private Menu listMenus() {
        System.out.println("List of Menus");
        final Iterable<Menu> listMenus = controller.wipMenus();
        final SelectWidget<Menu> selectorMenu = new SelectWidget<>("Select a menu", listMenus,
                new MenuPrinter());
        selectorMenu.show();
        return selectorMenu.selectedElement();
    }

    private Dish selectDishForMealODay(final Menu theMenu) {
        final Calendar currentDay = theMenu.timePeriod().start();
        final String stringDay = Calendars.format(currentDay, "YYYY/MM/DD");
        System.out.printf("Dia da Semana %-30s%n", stringDay);
        System.out.println("List of Dishes - Select a Dish");
        final Iterable<Dish> listDish = listDishService.allDishes();
        final SelectWidget<Dish> selectorDish = new SelectWidget<>("Select a dish", listDish,
                new DishPrinter());
        selectorDish.show();
        return selectorDish.selectedElement();
    }

    @Override
    public String headline() {
        return "Register Meal On a Menu";
    }
}
