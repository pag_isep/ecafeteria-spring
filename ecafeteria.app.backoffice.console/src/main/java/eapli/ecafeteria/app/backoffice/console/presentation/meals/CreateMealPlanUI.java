/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.meals;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.CafeteriaPrinter;
import eapli.ecafeteria.mealplanning.application.CreateMealPlanController;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 * @author ajs@isep.ipp.pt 02/05/2016
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class CreateMealPlanUI extends AbstractUI {

    @Autowired
    private CreateMealPlanController theController;

    @Override
    protected boolean doShow() {
        final Iterable<Cafeteria> allCafeterias = theController.cafeterias();
        final SelectWidget<Cafeteria> selector = new SelectWidget<>("Select a cafeteria", allCafeterias,
                new CafeteriaPrinter());
        selector.show();
        final Cafeteria cafeteria = selector.selectedElement();
        final String name = Console.readLine("Name of Plan");
        final Calendar beginDate = Console.readCalendar("Begin date");
        final Calendar endDate = Console.readCalendar("End date");

        try {
            theController.createMealPlan(cafeteria, name, beginDate, endDate);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            System.out.println("That plan already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Import Menu to New Plan of Meals";
    }
}
