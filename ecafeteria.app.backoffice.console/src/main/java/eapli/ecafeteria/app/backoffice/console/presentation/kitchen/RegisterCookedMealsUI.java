/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.kitchen;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.meals.MealPrinter;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.mealexecution.application.RegisterCookedMealController;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Fernando
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class RegisterCookedMealsUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(RegisterCookedMealsUI.class);

    @Autowired
    private RegisterCookedMealController theController;

    @Override
    protected boolean doShow() {
        final Calendar theday = Console.readCalendar("Date?");

        final Iterable<MealType> mealTypes = theController.listMealTypes();
        final SelectWidget<MealType> selector = new SelectWidget<>("Meal types:", mealTypes, new MealTypePrinter());
        selector.show();
        final MealType theMealType = selector.selectedElement();

        // FIXME select a cafeteria + OU
        final Cafeteria cafeteria = null;

        if (theMealType != null) {
            Iterable<Meal> meals = null;
            do {
                meals = theController.listMealsNotYetCooked(cafeteria, theday, theMealType);
                if (meals.iterator().hasNext()) {
                    final SelectWidget<Meal> selectorMeal = new SelectWidget<>("Meals:", meals, new MealPrinter());

                    selectorMeal.show();
                    final Meal theMeal = selectorMeal.selectedElement();
                    if (theMeal != null) {
                        final int quantity = Console.readInteger("Quantity cooked?");
                        try {
                            theController.registerCookedMeal(theMeal, quantity);
                        } catch (final IntegrityViolationException e) {
                            System.out.println("Problema no registo de refeicoes preparadas.");
                        } catch (final ConcurrencyException e) {
                            LOGGER.error("This should never happen", e);
                            System.out.println(
                                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
                        }
                    }
                } else {
                    System.out.println("There is no more Meals for registering Cooked Meals");
                }
            } while (meals.iterator().hasNext());
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Cooked Meals";
    }
}
