/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.dishesviadto;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.dishes.NutricionalInfoDataWidget;
import eapli.ecafeteria.dishmanagement.application.viadto.RegisterDishViaDTOController;
import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.dishmanagement.dto.DishTypeDTO;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class RegisterDishViaDTOUI extends AbstractUI {
	private static final Logger LOGGER = LogManager.getLogger(RegisterDishViaDTOUI.class);

	@Autowired
	private RegisterDishViaDTOController theController;

	@Override
	protected boolean doShow() {
		final var dishTypes = theController.dishTypes();
		final var selector = new SelectWidget<DishTypeDTO>("Dish types:", dishTypes,
				new DishTypeDTOPrinter());
		selector.show();
		final var theDishType = selector.selectedElement();

		final var name = Console.readLine("Name");

		final var nutricionalData = new NutricionalInfoDataWidget();
		nutricionalData.show();

		final var price = Console.readDouble("Price");

		try {
			final var dish = new DishDTO(theDishType.getAcronym(), theDishType.getAcronym(), name,
					nutricionalData.calories(), nutricionalData.salt(), price, "EUR", true);
			theController.registerDish(dish);
		} catch (final IntegrityViolationException e) {
			System.out.println("You tried to enter a dish which already exists in the database.");
		} catch (final ConcurrencyException e) {
			LOGGER.error("This should never happen", e);
			System.out.println(
					"Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
		}

		return false;
	}

	@Override
	public String headline() {
		return "Register Dish";
	}
}
