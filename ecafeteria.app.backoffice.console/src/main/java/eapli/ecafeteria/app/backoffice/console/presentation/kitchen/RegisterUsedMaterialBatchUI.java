/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.kitchen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.meals.MealPrinter;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.traceability.application.RegisterUsedMaterialController;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;

/**
 *
 * @author pag
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class RegisterUsedMaterialBatchUI extends AbstractUI {
    private static final Logger LOGGER = LogManager.getLogger(RegisterUsedMaterialBatchUI.class);

    @Autowired
    private RegisterUsedMaterialController theController;

    @Override
    protected boolean doShow() {
        final Cafeteria cafeteria = null;
        final Meal meal = selectMeal(cafeteria);
        final Material material = selectMaterial();

        final String lotNumber = Console.readLine("Lot Number:");

        try {
            theController.registerUsedMaterial(meal, material, lotNumber);
        } catch (final IntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        } catch (final ConcurrencyException e) {
            LOGGER.error("This should never happen", e);
            System.out.println(
                    "Unfortunatelly there was an unexpected error in the application. Please try again and if the problem persists, contact your system admnistrator.");
        }
        return false;
    }

    // TODO extract to generic utility method
    private Meal selectMeal(final Cafeteria cafeteria) {
        final Iterable<Meal> list = theController.meals(cafeteria);
        final SelectWidget<Meal> selector = new SelectWidget<>("Select a meal", list, new MealPrinter());
        selector.show();
        return selector.selectedElement();
    }

    // TODO extract to generic utility method
    private Material selectMaterial() {
        final Iterable<Material> list = theController.materials();
        final SelectWidget<Material> selector = new SelectWidget<>("Select a material", list, new MaterialPrinter());
        selector.show();
        return selector.selectedElement();
    }

    @Override
    public String headline() {
        return "Register Material";
    }
}
