/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.ecafeteria.app.backoffice.console.presentation.authz.DeactivateUserUI;
import eapli.ecafeteria.app.backoffice.console.presentation.authz.ListUsersUI;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.AddCafeteriaUI;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.AddDeliveryStationUI;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.AddOrganicUnitUI;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.CafeteriaPrinter;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.CafeteriaUserPrinter;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.DeliveryStationPrinter;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteria.OrganicUnitPrinter;
import eapli.ecafeteria.app.backoffice.console.presentation.cafeteriausers.AcceptRefuseSignupRequestUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ActivateDeactivateDishTypeUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ActivateDeactivateDishUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ChangeDishNutricionalInfoUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ChangeDishPriceUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ChangeDishTypePessimisticLockUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ChangeDishTypeUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ListDishTypeUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.ListDishUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.RegisterDishTypeUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.RegisterDishUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.reporting.ReportDishesPerCaloricCategoryAsTuplesUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.reporting.ReportDishesPerCaloricCategoryUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.reporting.ReportDishesPerDishTypeUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishes.reporting.ReportHighCaloriesDishesUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishesviadto.ListDishViaDTOUI;
import eapli.ecafeteria.app.backoffice.console.presentation.dishesviadto.RegisterDishViaDTOUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.BatchSearchUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.ListAllCookedMealsUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.ListCookedMealsByDayUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.ListMaterialUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.RegisterCookedMealsUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.RegisterMaterialUI;
import eapli.ecafeteria.app.backoffice.console.presentation.kitchen.RegisterUsedMaterialBatchUI;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.CreateMealPlanUI;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.ListMealUI;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.RegisterMealOnMenuUI;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.RegisterMenuOfMealsUI;
import eapli.ecafeteria.app.backoffice.console.presentation.meals.SetMealPlannedQtyUI;
import eapli.ecafeteria.app.components.authz.console.presentation.MyUserMenuBuilder;
import eapli.ecafeteria.app.components.meals.console.presentation.ListMenuUI;
import eapli.ecafeteria.cafeteriausermanagement.application.ListCafeteriaUsersController;
import eapli.ecafeteria.deliverystationmanagement.application.ListDeliveryStationsController;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.orgmanagement.application.ListCafeteriaController;
import eapli.ecafeteria.orgmanagement.application.ListOrganicUnitsController;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ListUI;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * @TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
@Component
public class MainMenu extends AbstractUI {

    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String ACRONYM = "ACRONYM";
    private static final String NOT_IMPLEMENTED_YET_LABEL = "Not implemented yet";
    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // ORGANIC UNITS
    private static final int ADD_ORGANIC_UNIT_OPTION = 1;
    private static final int LIST_ORGANIC_UNIT_OPTION = 2;
    private static final int ADD_CAFETERIA_OPTION = 3;
    private static final int LIST_CAFETERIA_OPTION = 4;
    private static final int LIST_CAFETERIA_USERS_OPTION = 5;

    // POS
    private static final int ADD_POS_OPTION = 1;
    private static final int LIST_POS_OPTION = 2;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;

    // DISHES - DISH TYPE
    private static final int DISH_TYPE_REGISTER_OPTION = 1;
    private static final int DISH_TYPE_LIST_OPTION = 2;
    private static final int DISH_TYPE_CHANGE_OPTION = 3;
    private static final int DISH_TYPE_CHANGE_PESIMISTIC_LOCK_OPTION = 4;
    private static final int DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION = 5;
    // DISHES - DISH
    private static final int DISH_REGISTER_OPTION = 10;
    private static final int DISH_LIST_OPTION = 11;
    private static final int DISH_REGISTER_DTO_OPTION = 12;
    private static final int DISH_LIST_DTO_OPTION = 13;
    private static final int DISH_ACTIVATE_DEACTIVATE_OPTION = 14;
    private static final int DISH_CHANGE_OPTION = 15;
    // DISHES - DISH - NUTRICIONAL INFO
    private static final int CHANGE_DISH_NUTRICIONAL_INFO_OPTION = 1;
    private static final int CHANGE_DISH_PRICE_OPTION = 2;

    // MEALS
    private static final int REGISTER_COOKED_MEALS_OPTION = 1;
    private static final int LIST_ALL_COOKED_MEALS_OPTION = 2;
    private static final int LIST_COOKED_MEALS_BY_DAY_OPTION = 3;

    // MENUS AND PLAN

    // Menus
    private static final int LIST_MEALS_OPTION = 1;
    private static final int CREATE_MENU_OF_MEALS_OPTION = 2;
    private static final int LIST_MENU_OF_MEALS_OPTION = 3;
    private static final int MEAL_REGISTER_OPTION = 4;

    // Plan of Meals
    private static final int CREATE_PLAN_OF_MEALS_OPTION = 5;
    private static final int REGISTER_MEAL_QTY_PLAN_OPTION = 6;

    // TRACEABILITY
    private static final int MATERIAL_REGISTER_OPTION = 1;
    private static final int MATERIAL_LIST_OPTION = 2;
    private static final int USED_MATERIAL_BATCH_REGISTER_OPTION = 3;
    private static final int BATCH_SEARCH_OPTION = 4;

    // REPORTING
    private static final int REPORTING_DISHES_PER_DISHTYPE_OPTION = 1;
    private static final int REPORTING_HIGH_CALORIES_DISHES_OPTION = 2;
    private static final int REPORTING_DISHES_PER_CALORIC_CATEGORY_OPTION = 3;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int ORGANIC_UNITS_OPTION = 3;
    private static final int POS_OPTION = 4;
    private static final int DISHES_OPTION = 5;
    private static final int REPORTING_DISHES_OPTION = 6;
    private static final int MENUS_OPTION = 7;
    private static final int MEALS_OPTION = 8;
    private static final int TRACEABILITY_OPTION = 9;
    private static final int SETTINGS_OPTION = 88;

    @Autowired
    private AuthorizationService authz;

    @Autowired
    private MyUserMenuBuilder myUserMenuBuilder;

    @Autowired
    private RegisterCookedMealsUI registerCookedMealsUI;

    @Autowired
    private ListAllCookedMealsUI listAllCookedMealsUI;

    @Autowired
    private ListCookedMealsByDayUI listCookedMealsByDayUI;

    @Autowired
    private RegisterMenuOfMealsUI registerMenuOfMealsUI;

    @Autowired
    private ListMealUI listMealUI;

    @Autowired
    private ListMenuUI listMenuUI;

    @Autowired
    private RegisterMealOnMenuUI registerMealOnMenuUI;

    @Autowired
    private CreateMealPlanUI createMealPlanUI;

    @Autowired
    private SetMealPlannedQtyUI setMealPlannedQtyUI;

    @Autowired
    private AddOrganicUnitUI addOrganicUnitUI;

    @Autowired
    private ListOrganicUnitsController listOrganicUnitsController;

    @Autowired
    private AddCafeteriaUI addCafeteriaUI;

    @Autowired
    private ListCafeteriaController listCafeteriaController;

    @Autowired
    private ListCafeteriaUsersController listCafeteriaUsersController;

    @Autowired
    private AddDeliveryStationUI addDeliveryStationUI;

    @Autowired
    private ListDeliveryStationsController listDeliveryStationsController;

    @Autowired
    private AddUserUI addUserUI;

    @Autowired
    private ListUsersUI listUsersUI;

    @Autowired
    private DeactivateUserUI deactivateUserUI;

    @Autowired
    private AcceptRefuseSignupRequestUI acceptRefuseSignupRequestUI;

    @Autowired
    private RegisterDishTypeUI registerDishTypeUI;

    @Autowired
    private ListDishTypeUI listDishTypeUI;

    @Autowired
    private ChangeDishTypeUI changeDishTypeUI;

    @Autowired
    private ChangeDishTypePessimisticLockUI changeDishTypePessimisticLockUI;

    @Autowired
    private ActivateDeactivateDishTypeUI activateDeactivateDishTypeUI;

    @Autowired
    private RegisterDishUI registerDishUI;

    @Autowired
    private ListDishUI listDishUI;

    @Autowired
    private RegisterDishViaDTOUI registerDishViaDTOUI;

    @Autowired
    private ListDishViaDTOUI listDishViaDTOUI;

    @Autowired
    private ActivateDeactivateDishUI activateDeactivateDishUI;

    @Autowired
    private ChangeDishNutricionalInfoUI changeDishNutricionalInfoUI;

    @Autowired
    private ChangeDishPriceUI changeDishPriceUI;

    @Autowired
    private ReportDishesPerDishTypeUI reportDishesPerDishTypeUI;

    @Autowired
    private ReportHighCaloriesDishesUI reportHighCaloriesDishesUI;

    @Autowired
    private ReportDishesPerCaloricCategoryUI reportDishesPerCaloricCategoryUI;

    @Autowired
    private ReportDishesPerCaloricCategoryAsTuplesUI reportDishesPerCaloricCategoryAsTuplesUI;

    @Autowired
    private RegisterMaterialUI registerMaterialUI;

    @Autowired
    private ListMaterialUI listMaterialUI;

    @Autowired
    private RegisterUsedMaterialBatchUI registerUsedMaterialBatchUI;

    @Autowired
    private BatchSearchUI batchSearchUI;

    @Value("${eapli.ecafeteria.ui.menu.layout:horizontal}")
    private String menuLayout;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        // Consider caching the main menu. keep in mind that it needs to be rebuilt if
        // the logged in user changes as it might have other roles than the
        // current one
        return buildRenderer(buildMainMenu()).render();
    }

    private Boolean isMenuLayoutHorizontal() {
        return "horizontal".equalsIgnoreCase(menuLayout);
    }

    private MenuRenderer buildRenderer(final Menu menu) {
        if (isMenuLayoutHorizontal()) {
            return new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            return new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "eCAFETERIA [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("eCAFETERIA [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = myUserMenuBuilder.withRole(null);
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator("--------------"));
        }

        if (authz.hasSession()) {
            if (authz.isAuthenticatedUserAuthorizedTo(CafeteriaRoles.ADMIN)) {
                mainMenu.addSubMenu(USERS_OPTION, buildUsersMenu());
                mainMenu.addSubMenu(ORGANIC_UNITS_OPTION, buildOrganicUnitsMenu());
                mainMenu.addSubMenu(POS_OPTION, buildPOSMenu());
                mainMenu.addSubMenu(SETTINGS_OPTION, buildAdminSettingsMenu());
            }
            if (authz.isAuthenticatedUserAuthorizedTo(CafeteriaRoles.KITCHEN_MANAGER)) {
                mainMenu.addSubMenu(MEALS_OPTION, buildKitchenMealsMenu());
                mainMenu.addSubMenu(TRACEABILITY_OPTION, buildKitchenMenu());
            }
            if (authz.isAuthenticatedUserAuthorizedTo(CafeteriaRoles.MENU_MANAGER)) {
                mainMenu.addSubMenu(DISHES_OPTION, buildDishesMenu());
                mainMenu.addSubMenu(MENUS_OPTION, buildMenusMenu());
                // reporting
                mainMenu.addSubMenu(REPORTING_DISHES_OPTION, buildReportingDishesMenu());
            }
        }
        if (!isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator("--------------"));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildKitchenMealsMenu() {
        final Menu kitchenMealsMenu = new Menu("Meals >");

        kitchenMealsMenu.addItem(REGISTER_COOKED_MEALS_OPTION, "Register cooked meals",
                registerCookedMealsUI::show);
        kitchenMealsMenu.addItem(LIST_ALL_COOKED_MEALS_OPTION, "List all cooked meals",
                listAllCookedMealsUI::show);
        kitchenMealsMenu.addItem(LIST_COOKED_MEALS_BY_DAY_OPTION, "List cooked meals by day",
                listCookedMealsByDayUI::show);

        kitchenMealsMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return kitchenMealsMenu;
    }

    private Menu buildMenusMenu() {
        final Menu menusMenu = new Menu("Menus >");

        menusMenu.addItem(LIST_MEALS_OPTION, "List Meals (** DEBUG **)", listMealUI::show);

        menusMenu.addItem(CREATE_MENU_OF_MEALS_OPTION, "Create Menu of Meals ",
                registerMenuOfMealsUI::show);
        menusMenu.addItem(LIST_MENU_OF_MEALS_OPTION, "List Menu (** DEBUG **)", listMenuUI::show);
        menusMenu.addItem(MEAL_REGISTER_OPTION, "Register meal on Menu",
                registerMealOnMenuUI::show);

        menusMenu.addItem(CREATE_PLAN_OF_MEALS_OPTION, "Create Plan of Meals from Menu",
                createMealPlanUI::show);
        menusMenu.addItem(REGISTER_MEAL_QTY_PLAN_OPTION, "Register Meal Quantity on Plan",
                setMealPlannedQtyUI::show);
        menusMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);
        return menusMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu adminSettingsMenu = new Menu("Settings >");

        adminSettingsMenu.addItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction(NOT_IMPLEMENTED_YET_LABEL));
        adminSettingsMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return adminSettingsMenu;
    }

    private Menu buildOrganicUnitsMenu() {
        final Menu organicUnitsMenu = new Menu("Organic units >");

        organicUnitsMenu.addItem(ADD_ORGANIC_UNIT_OPTION, "Add Organic Unit",
                addOrganicUnitUI::show);
        organicUnitsMenu.addItem(LIST_ORGANIC_UNIT_OPTION, "List Organic Unit", () -> {
            // example of using the generic list ui from the framework
            new ListUI<>(() -> listOrganicUnitsController.organicUnits(), new OrganicUnitPrinter(),
                    "Organic Unit",
                    String.format("#  %-10s%-30s%-4s", ACRONYM, DESCRIPTION, "ACTIVE"),
                    "List Organic Units", "No data.").show();
            return true;
        });
        organicUnitsMenu.addItem(ADD_CAFETERIA_OPTION, "Add Cafeteria", addCafeteriaUI::show);
        organicUnitsMenu.addItem(LIST_CAFETERIA_OPTION, "List Cafeterias", () -> {
            // example of using the generic list ui from the framework
            new ListUI<>(() -> listCafeteriaController.listCafeterias(), new CafeteriaPrinter(),
                    "Cafeteria",
                    String.format("#  %-10s%-30s%-4s", ACRONYM, DESCRIPTION, "OU"),
                    "List Cafeterias", "No data.").show();
            return true;
        });
        organicUnitsMenu.addItem(LIST_CAFETERIA_USERS_OPTION, "List Cafeteria Users", () -> {
            // example of using the generic list ui from the framework
            new ListUI<>(() -> listCafeteriaUsersController.activeCafeteriaUsers(),
                    new CafeteriaUserPrinter(),
                    "Cafeteria User",
                    String.format("#  %10s %20s %20s %10s", "USERNAME", "F. NAME", "L. NAME",
                            "MEC. NUMBER"),
                    "List Organic Cafeteria Users", "No data.").show();
            return true;
        });

        organicUnitsMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return organicUnitsMenu;
    }

    private Menu buildPOSMenu() {
        final Menu posMenu = new Menu("POS >");

        posMenu.addItem(ADD_POS_OPTION, "Add POS", addDeliveryStationUI::show);
        posMenu.addItem(LIST_POS_OPTION, "List POS", () -> {
            // example of using the generic list ui from the framework
            new ListUI<>(() -> listDeliveryStationsController.listStations(),
                    new DeliveryStationPrinter(), "POS",
                    String.format("#  %-10s%-10s%-30s", "UNIT", ACRONYM, DESCRIPTION), "List POSs",
                    "No data.").show();
            return true;
        });
        posMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return posMenu;
    }

    private Menu buildUsersMenu() {
        final Menu usersMenu = new Menu("Users >");

        usersMenu.addItem(ADD_USER_OPTION, "Add User", addUserUI::show);
        usersMenu.addItem(LIST_USERS_OPTION, "List all Users", listUsersUI::show);
        usersMenu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", deactivateUserUI::show);
        usersMenu.addItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                acceptRefuseSignupRequestUI::show);
        usersMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return usersMenu;
    }

    private Menu buildDishesMenu() {
        final Menu dishesmenu = new Menu("Dishes >");

        // dish types
        dishesmenu.addItem(DISH_TYPE_REGISTER_OPTION, "Register new Dish Type",
                registerDishTypeUI::show);
        dishesmenu.addItem(DISH_TYPE_LIST_OPTION, "List all Dish Type", listDishTypeUI::show);
        dishesmenu.addItem(DISH_TYPE_CHANGE_OPTION, "Change Dish Type description",
                changeDishTypeUI::show);
        dishesmenu.addItem(DISH_TYPE_CHANGE_PESIMISTIC_LOCK_OPTION, "Change Dish Type description (Pessimistic Lock)",
                changeDishTypePessimisticLockUI::show);
        dishesmenu.addItem(DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish Type",
                activateDeactivateDishTypeUI::show);

        // dishes
        dishesmenu.addItem(DISH_REGISTER_OPTION, "Register new Dish", registerDishUI::show);
        dishesmenu.addItem(DISH_LIST_OPTION, "List all Dish", listDishUI::show);
        dishesmenu.addItem(DISH_REGISTER_DTO_OPTION, "Register new Dish (via DTO)",
                registerDishViaDTOUI::show);
        dishesmenu.addItem(DISH_LIST_DTO_OPTION, "List all Dish (via DTO)", listDishViaDTOUI::show);
        dishesmenu.addItem(DISH_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish",
                activateDeactivateDishUI::show);

        dishesmenu.addSubMenu(DISH_CHANGE_OPTION, buildChangeDishMenu());

        dishesmenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return dishesmenu;
    }

    private Menu buildChangeDishMenu() {
        final Menu changeDishesMenu = new Menu("Change Dish >");

        changeDishesMenu.addItem(CHANGE_DISH_NUTRICIONAL_INFO_OPTION, "Change Nutricional Info",
                changeDishNutricionalInfoUI::show);
        changeDishesMenu.addItem(CHANGE_DISH_PRICE_OPTION, "Change Price", changeDishPriceUI::show);
        changeDishesMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return changeDishesMenu;
    }

    private Menu buildReportingDishesMenu() {
        final Menu reportingDishesMenu = new Menu("Reporting Dishes >");

        reportingDishesMenu.addItem(REPORTING_DISHES_PER_DISHTYPE_OPTION, "Dishes per Dish Type",
                reportDishesPerDishTypeUI::show);
        reportingDishesMenu.addItem(REPORTING_HIGH_CALORIES_DISHES_OPTION, "High Calories Dishes",
                reportHighCaloriesDishesUI::show);
        reportingDishesMenu.addItem(REPORTING_DISHES_PER_CALORIC_CATEGORY_OPTION,
                "Dishes per Caloric Category",
                reportDishesPerCaloricCategoryUI::show);
        reportingDishesMenu.addItem(REPORTING_DISHES_PER_CALORIC_CATEGORY_OPTION + 1,
                "Dishes per Caloric Category (as tuples)",
                reportDishesPerCaloricCategoryAsTuplesUI::show);
        reportingDishesMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return reportingDishesMenu;
    }

    private Menu buildKitchenMenu() {
        final Menu kitchenMenu = new Menu("Traceability >");

        kitchenMenu.addItem(MATERIAL_REGISTER_OPTION, "Register new material",
                registerMaterialUI::show);
        kitchenMenu.addItem(MATERIAL_LIST_OPTION, "List all materials", listMaterialUI::show);
        kitchenMenu.addItem(USED_MATERIAL_BATCH_REGISTER_OPTION, "Register used material & lot",
                registerUsedMaterialBatchUI::show);
        kitchenMenu.addItem(BATCH_SEARCH_OPTION, "Search used batch", batchSearchUI::show);

        kitchenMenu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return kitchenMenu;
    }
}
