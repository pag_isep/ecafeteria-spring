/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.kitchen;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealexecution.application.ListCookedMealsController;
import eapli.ecafeteria.mealexecution.domain.model.CookedMealQty;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Fernando
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class ListCookedMealsByDayUI extends AbstractUI {

    @Autowired
    private ListCookedMealsController theController;

    private Visitor<CookedMealQty> elementPrinter() {
        return new CookedMealQtyPrinter();
    }

    /**
     * listing of elements.
     *
     */
    @Override
    protected boolean doShow() {
        final Calendar theDay = Console.readCalendar("Date?");

        final Iterable<CookedMealQty> cookedMeals = theController.listCookedMeals(theDay);
        if (!cookedMeals.iterator().hasNext()) {
            System.out.println("There is no registered cooked meals");
        } else {
            new ListWidget<>("Listing Cooked Meals ", cookedMeals, elementPrinter()).show();
        }
        return true;
    }

    @Override
    public String headline() {
        return "list of Register Cooked Meals";
    }
}
