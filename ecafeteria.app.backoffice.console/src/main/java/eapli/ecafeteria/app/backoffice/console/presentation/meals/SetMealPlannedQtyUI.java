/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.meals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealplanning.application.SetMealPlannedQtyController;
import eapli.ecafeteria.mealplanning.domain.model.MealPlan;
import eapli.ecafeteria.mealplanning.domain.model.MealPlanItem;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.io.util.Console;

/**
 * @author ajs@isep.ipp.pt 02/05/2016
 */
@Component
@SuppressWarnings({ "squid:S106" })
public class SetMealPlannedQtyUI extends AbstractUI {

    @Autowired
    private SetMealPlannedQtyController theController;

    @Override
    protected boolean doShow() {

        // List and select a Meal Plan
        final Iterable<MealPlan> mealPlans = theController.listMealPlans();
        final SelectWidget<MealPlan> selector = new SelectWidget<>("Select a meal plan", mealPlans,
                new MealPlanPrinter());
        selector.show();
        final MealPlan theMealPlan = selector.selectedElement();

        if (theMealPlan != null) {

            // List and select a Meal Plan Item
            final Iterable<MealPlanItem> mealPlanItems = theController.listMealPlanItems(theMealPlan.beginDate(),
                    theMealPlan.endDate());

            final SelectWidget<MealPlanItem> selectorItem = new SelectWidget<>("Select a meal plan item", mealPlanItems,
                    new MealPlanItemPrinter());
            selectorItem.show();
            final MealPlanItem theMealPlanItem = selectorItem.selectedElement();

            if (theMealPlanItem != null) {

                final int newPlannedQty = Console.readInteger("Enter new number of planned meals");

                try {
                    theController.setPlannedQty(theMealPlanItem, newPlannedQty);
                } catch (final ConcurrencyException | IntegrityViolationException exMerge) {
                    System.out.println("Data has changed or been deleted since it was last read. Please try again.");
                }
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Change Dish Type description";
    }
}
