/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.dishes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import eapli.ecafeteria.dishmanagement.domain.model.Allergen;
import eapli.ecafeteria.dishmanagement.domain.model.AllergenInDish;

/**
 *
 * @author mcn
 */
@SuppressWarnings({ "squid:S106" })
public class AllergenInDishWidget {
    private final List<Allergen> list;

    public AllergenInDishWidget(final Set<AllergenInDish> all) {
        if (all.isEmpty()) {
            list = new ArrayList<>();
        } else {
            list = makeList(all);
        }
    }

    private List<Allergen> makeList(final Set<AllergenInDish> allergenSet) {
        final List<Allergen> list = new ArrayList<>();
        if (!allergenSet.isEmpty()) {
            for (final AllergenInDish item : allergenSet) {
                list.add(item.allergen());
            }
        }
        return list;
    }

    public void showAllergeans() {
        if (list.isEmpty()) {
            System.out.println("NO  ALLERGENS IN THIS DISH");
        } else {
            System.out.println("ALLERGENS IN THIS DISH:");
            for (final Allergen allergean : list) {
                new AllergenPrinter().visit(allergean);
            }
        }
    }
}
