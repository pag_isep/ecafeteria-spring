/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import eapli.ecafeteria.app.backoffice.console.presentation.MainMenu;
import eapli.ecafeteria.app.components.authz.console.presentation.LoginUI;
import eapli.ecafeteria.app.components.base.console.presentation.ECafeteriaBaseApplication;
import eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers.MealBookedWatchDog;
import eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers.NewUserRegisteredFromSignupWatchDog;
import eapli.ecafeteria.cafeteriausermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import eapli.ecafeteria.cafeteriausermanagement.domain.events.SignupAcceptedEvent;
import eapli.ecafeteria.mealbooking.domain.model.BookedEvent;
import eapli.ecafeteria.usermanagement.application.eventhandlers.SignupAcceptedWatchDog;
import eapli.framework.infrastructure.pubsub.EventDispatcher;

/**
 *
 * @author Paulo Gandra Sousa
 */
@SpringBootApplication
@ComponentScan(basePackages = { "eapli.ecafeteria.spring" })
@SuppressWarnings("squid:S106")
public class ECafeteriaBackoffice extends ECafeteriaBaseApplication {

    @Autowired
    private SignupAcceptedWatchDog signupAcceptedWatchDog;

    @Autowired
    private NewUserRegisteredFromSignupWatchDog newUserRegisteredFromSignupWatchDog;

    @Autowired
    private MealBookedWatchDog mealBookedWatchDog;

    @Autowired
    private LoginUI loginUI;

    @Autowired
    private MainMenu mainMenu;

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(ECafeteriaBackoffice.class, args);
    }

    @Override
    protected void doMain(final String[] args) {
        // login and go to main menu
        if (loginUI.show()) {
            // go to main menu
            mainMenu.mainLoop();
        }
    }

    @Override
    protected String appTitle() {
        return "eCafeteria Back Office";
    }

    @Override
    protected String appGoodbye() {
        return "eCafeteria Back Office";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
        dispatcher.subscribe(newUserRegisteredFromSignupWatchDog, NewUserRegisteredFromSignupEvent.class);
        dispatcher.subscribe(signupAcceptedWatchDog, SignupAcceptedEvent.class);
        dispatcher.subscribe(mealBookedWatchDog, BookedEvent.class);
    }
}
