/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.kitchen;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.app.backoffice.console.presentation.meals.MealPrinter;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.application.BatchSearchController;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.model.UsedMaterialBatch;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;

/**
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class BatchSearchUI extends AbstractUI {
    @Autowired
    private BatchSearchController theController;

    private Iterable<Meal> mealsFromUsedBatches(final Material material, final String lotNumber) {
        final Iterable<UsedMaterialBatch> used = theController.usedMaterialAndBatch(material,
                lotNumber);
        final List<Meal> usedMeals = new ArrayList<>();
        used.forEach(e -> usedMeals.add(e.meal()));
        return usedMeals;
    }

    private Iterable<Meal> mealsWhichUsedBatches(final Material material, final String lotNumber) {
        return theController.mealsWhichUsedBatch(material, lotNumber);
    }

    private Iterable<Meal> affectedMeals(final Material material, final String lotNumber) {
        // just for fun we are using two different methods to do the same and
        // letting "chance" decide which one
        if (eapli.framework.math.util.Math.heads()) {
            return mealsWhichUsedBatches(material, lotNumber);
        } else {
            return mealsFromUsedBatches(material, lotNumber);
        }
    }

    @Override
    protected boolean doShow() {
        // gather input
        final Material material = selectMaterial();
        final String lotNumber = Console.readLine("Lot Number:");

        // show affected meals
        final Iterable<Meal> usedMeals = affectedMeals(material, lotNumber);

        final ListWidget<Meal> usedW = new ListWidget<>("Meals which used material batch",
                usedMeals,
                new MealPrinter());
        usedW.show();

        // warn users who consumed those meals
        final boolean warnUsers = Console.readBoolean("Send email to users?");
        if (warnUsers) {
            theController.warnAffectedUsers(material, lotNumber);
        }
        return true;
    }

    @Override
    public String headline() {
        return "BATCH SEARCH";
    }

    private Material selectMaterial() {
        final Iterable<Material> list = theController.materials();
        final SelectWidget<Material> selector = new SelectWidget<>("Select a material", list,
                new MaterialPrinter());
        selector.show();
        return selector.selectedElement();
    }
}
