/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealexecution.domain.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.Version;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;

/**
 * Stores the actual cooked number of meals
 *
 * @FIXME the business id of this aggregate should be the Meal/MealID
 * @FIXME this class overlaps with the MealPlanItem in terms of responsibility
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "meal_id" }) })
public class CookedMealQty implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(optional = false)
    private Meal meal;

    // TODO use a value object class instead of int?
    @Column(nullable = false)
    private int quantity = 0;

    protected CookedMealQty() {
        // for ORM only
    }

    public CookedMealQty(final Meal meal, final int cooked) {
        Preconditions.nonNull(meal);
        Preconditions.isPositive(cooked);

        this.meal = meal;
        quantity = cooked;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    public Meal meal() {
        return meal;
    }

    public int quantity() {
        return quantity;
    }

    @Override
    public Long identity() {
        return id;
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof CookedMealQty)) {
            return false;
        }

        final CookedMealQty that = (CookedMealQty) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && meal.equals(that.meal)
                && quantity == that.quantity;
    }
}
