/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.mealbooking.csvprotocol.server.CsvBookingProtocolMessageParser;
import eapli.framework.csv.util.CsvLineMarshaler;

/**
 *
 * @author Paulo Gandra Sousa 08/06/2020
 *
 */
public abstract class BaseServerErrorOnThrowsBookingRequestTest extends BaseBookingRequestTest {

    protected abstract CsvBookingProtocolMessageParser parser();

    private static final Logger LOGGER = LogManager.getLogger(BaseServerErrorOnThrowsBookingRequestTest.class);

    protected abstract String getSampleOkRequest();

    @Test
    public void ensureServerErrorOnThrows() throws ParseException {

        final var req = parser().parse(getSampleOkRequest());

        final var result = req.execute();
        LOGGER.info("{} -> {}", getSampleOkRequest(), result);

        final var lines = result.split("\n");
        assertEquals(1, lines.length);

        final var tokens = CsvLineMarshaler.tokenize(lines[0]).toArray(new String[0]);
        assertEquals(3, tokens.length);
        assertEquals("SERVER_ERROR", tokens[0]);
        assertEquals("\"" + MockControllers.NOT_IMPLEMENTED_YET + "\"", tokens[2]);
        assertEquals('\n', result.charAt(result.length() - 1));
    }
}