/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.deamon.booking.protocol.requests;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.dishmanagement.domain.model.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayController;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthenticatedException;
import eapli.framework.money.domain.model.Money;
import eapli.framework.time.domain.model.DateInterval;
import eapli.framework.time.util.Calendars;
import eapli.framework.time.util.CurrentTimeCalendars;
import eapli.framework.util.Utility;

/**
 * @author Paulo Gandra de Sousa 09/06/2020
 *
 */
@Utility
public final class MockControllers {

    private MockControllers() {
        // ensure utility
    }

    public static BookAMealForADayController getMockControllerNoData() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when, final MealType forMeal) {
                return new ArrayList<>();
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                return BookingToken.newToken();
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                return BookingToken.newToken();
            }

            @Override
            public Iterable<Cafeteria> getCafeterias() {
                return new ArrayList<>();
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when,
                    final MealType forMeal) {
                return new ArrayList<>();
            }
        };
    }

    public static BookAMealForADayController getMockControllerUnknowns() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when, final MealType forMeal) {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public Iterable<Cafeteria> getCafeterias() {
                throw new IllegalArgumentException("Unknown");
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when,
                    final MealType forMeal) {
                throw new IllegalArgumentException("Unknown");
            }
        };
    }

    public static BookAMealForADayController getMockController1RowOfData() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when, final MealType forMeal) {
                final var dt = new DishType("fish", "fish");
                final var d1 = new DishBuilder().ofType(dt).named(Designation.valueOf("bacalhau"))
                        .costing(Money.euros(7.95)).build();
                final List<Meal> results = new ArrayList<>();

                final var cafe = buildCafe();

                final var menu = new Menu(cafe, "test",
                        new DateInterval(Calendars.of(2020, 6, 8), Calendars.of(2020, 6, 8)));
                results.add(new Meal(MealType.LUNCH, CurrentTimeCalendars.now(), d1, menu));
                return results;
            }

            private Cafeteria buildCafe() {
                final var ou = new OrganicUnit(Designation.valueOf("ou1"), "O.U. #1", "O.U. #1");
                final var cafe = new Cafeteria(ou, Designation.valueOf("cafe1"));
                return cafe;
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                return BookingToken.newToken();
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                return BookingToken.newToken();
            }

            @Override
            public Iterable<Cafeteria> getCafeterias() {
                final List<Cafeteria> c = new ArrayList<>();
                c.add(buildCafe());
                return c;
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when,
                    final MealType forMeal) {
                return getMealsOfADay(buildCafe(), when, forMeal);
            }
        };
    }

    public static final String NOT_IMPLEMENTED_YET = "not implemented yet";

    public static BookAMealForADayController getMockControllerThrow() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when, final MealType forMeal) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public Iterable<Cafeteria> getCafeterias() {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when,
                    final MealType forMeal) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }
        };
    }

    public static BookAMealForADayController getMockControllerNoAuth() {
        // TODO use mockito?
        // MOCK controller
        return new BookAMealForADayController() {
            @Override
            public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when, final MealType forMeal) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
                throw new UnauthenticatedException();
            }

            @Override
            public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
                throw new UnauthenticatedException();
            }

            @Override
            public Iterable<Cafeteria> getCafeterias() {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }

            @Override
            public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when,
                    final MealType forMeal) {
                throw new IllegalStateException(NOT_IMPLEMENTED_YET);
            }
        };
    }

}