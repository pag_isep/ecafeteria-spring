/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import eapli.ecafeteria.app.components.base.console.presentation.ECafeteriaBaseApplication;
import eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers.MealBookedWatchDog;
import eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers.NewUserRegisteredFromSignupWatchDog;
import eapli.ecafeteria.cafeteriausermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import eapli.ecafeteria.cafeteriausermanagement.domain.events.SignupAcceptedEvent;
import eapli.ecafeteria.infrastructure.bootstrappers.ECafeteriaBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.ECafeteriaDemoBootstrapper;
import eapli.ecafeteria.infrastructure.smoketests.ECafeteriaDemoSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.ECafeteriaSampleEndToEndSmokeTester;
import eapli.ecafeteria.mealbooking.domain.model.BookedEvent;
import eapli.ecafeteria.usermanagement.application.eventhandlers.SignupAcceptedWatchDog;
import eapli.framework.collections.util.ArrayPredicates;
import eapli.framework.infrastructure.pubsub.EventDispatcher;
import eapli.framework.io.util.Console;

/**
 * eCafeteria Bootstrapping data application.
 *
 * <p>
 * Create some sample data for faster testing of the application to make sure
 * whenever we start the application we already have a couple of entities in the
 * database.
 *
 * @author Paulo Gandra de Sousa
 */
@SpringBootApplication
@ComponentScan(basePackages = { "eapli.ecafeteria.spring" })
@SuppressWarnings("squid:S106")
public class ECafeteriaBootstrap extends ECafeteriaBaseApplication {

    @Autowired
    private ECafeteriaBootstrapper masterBootstrapper;

    @Autowired
    private ECafeteriaDemoBootstrapper demoBootstrapper;

    @Autowired
    private ECafeteriaSampleEndToEndSmokeTester endToEndSmokeTester;

    @Autowired
    private ECafeteriaDemoSmokeTester demoSmokeTester;

    @Autowired
    private SignupAcceptedWatchDog signupAcceptedWatchDog;

    @Autowired
    private NewUserRegisteredFromSignupWatchDog newUserRegisteredFromSignupWatchDog;

    @Autowired
    private MealBookedWatchDog mealBookedWatchDog;

    private boolean isToBootstrapDemoData;
    private boolean isToRunSampleE2E;
    private boolean isToRunSmokeTests;
    private boolean isToWaitInTheEnd;

    public static void main(final String[] args) {
        SpringApplication.run(ECafeteriaBootstrap.class, args);
    }

    @Override
    protected void doMain(final String[] args) {
        handleArgs(args);

        System.out.println("\n\n------- MASTER DATA -------");
        masterBootstrapper.execute();

        if (isToBootstrapDemoData) {
            System.out.println("\n\n------- DEMO DATA -------");
            demoBootstrapper.execute();
        }
        if (isToRunSampleE2E) {
            System.out.println("\n\n------- BASIC SCENARIO -------");
            endToEndSmokeTester.execute();
        }
        if (isToRunSmokeTests) {
            System.out.println("\n\n------- SPECIFIC FEATURES -------");
            demoSmokeTester.execute();
        }

        if (isToWaitInTheEnd) {
            Console.readLine("\n\n>>>>>> Enter to finish the program.");
        }
    }

    private void handleArgs(final String[] args) {
        isToRunSmokeTests = ArrayPredicates.contains(args, "-smoke:basic");
        isToRunSampleE2E = ArrayPredicates.contains(args, "-smoke:e2e");
        if (isToRunSampleE2E || isToRunSmokeTests) {
            isToBootstrapDemoData = true;
        } else {
            isToBootstrapDemoData = ArrayPredicates.contains(args, "-bootstrap:demo");
        }

        isToWaitInTheEnd = ArrayPredicates.contains(args, "-wait");
    }

    @Override
    protected String appTitle() {
        return "Bootstrapping eCafeteria data";
    }

    @Override
    protected String appGoodbye() {
        return "Bootstrap data done.";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
        dispatcher.subscribe(newUserRegisteredFromSignupWatchDog, NewUserRegisteredFromSignupEvent.class);
        dispatcher.subscribe(signupAcceptedWatchDog, SignupAcceptedEvent.class);
        dispatcher.subscribe(mealBookedWatchDog, BookedEvent.class);
    }
}
