# TO DO

## Todo items

- [DTOs in API](https://www.baeldung.com/entity-to-and-from-dto-for-a-java-spring-application)

- [Exception handling in API](https://www.baeldung.com/exception-handling-for-rest-with-spring)

- [Spring exceptions](https://www.baeldung.com/spring-exceptions)

## Ideas about improvements

- [HATEOS](https://spring.io/guides/gs/rest-hateoas/)

- API Doc

  - [restdoc](https://spring.io/guides/gs/testing-restdocs/)
  - [Swagger](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)

- [Spring validation](https://www.mkyong.com/spring-boot/spring-rest-validation-example/)

- [bulk delete/update operation](https://thoughts-on-java.org/hibernate-tip-fastest-option-delete-100-records/)

- [custom id generator](https://thoughts-on-java.org/custom-sequence-based-idgenerator/)

- Java 8 time

  - [toughts on java](https://thoughts-on-java.org/persist-localdate-localdatetime-jpa/)
  - [baeldung](https://www.baeldung.com/spring-data-java-8)

- [Properties with Spring](https://www.baeldung.com/properties-with-spring)

- <https://www.baeldung.com/persistence-with-spring-series>
