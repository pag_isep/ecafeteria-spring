/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealexecution.application;

import java.util.Arrays;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealexecution.domain.model.CookedMealQty;
import eapli.ecafeteria.mealexecution.domain.repositories.CookedMealRepository;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 *
 * @author Fernando
 */
@Component
@UseCaseController
public class RegisterCookedMealController {

	@Autowired
	private MealRepository mealRepository;
	@Autowired
	private CookedMealRepository cookedMealRepository;
	@Autowired
	private AuthorizationService authorizationService;

	public Iterable<MealType> listMealTypes() {
		return Arrays.asList(MealType.values());
	}

	public Iterable<Meal> listMeals(final Cafeteria cafeteria, final Calendar ofDay, final MealType mealType) {
		return mealRepository.findByDay(cafeteria, ofDay, mealType);
	}

	public Iterable<Meal> listMealsNotYetCooked(final Cafeteria cafeteria, final Calendar ofDay,
			final MealType mealType) {
		return mealRepository.findNotYetCookedByDay(cafeteria, ofDay, mealType);
	}

	@Transactional
	public CookedMealQty registerCookedMeal(final Meal meal, final int quantity) {

		authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.KITCHEN_MANAGER);
		final var newCookeMealdQty = new CookedMealQty(meal, quantity);
		return cookedMealRepository.save(newCookeMealdQty);
	}
}
