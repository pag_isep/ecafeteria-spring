/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.domain.model;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.Version;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Entity
@Table(name="T_USED_MATERIAL_BATCH", uniqueConstraints = { @UniqueConstraint(columnNames = { "meal_id", "material_pk", "batch_number" }) })
public class UsedMaterialBatch implements AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * primary id (both ORM & business)
     */
    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Long version;

    @ManyToOne(optional = false)
    private Meal meal;

    @ManyToOne(optional = false)
    private Material material;

    @Embedded
    private BatchNumber batchNumber;

    /**
     * Constructs a new Used Material Batch.
     *
     * @param meal
     * @param material
     * @param batch
     */
    public UsedMaterialBatch(final Meal meal, final Material material, final BatchNumber batch) {
        Preconditions.noneNull(meal, material, batch);

        this.material = material;
        this.meal = meal;
        batchNumber = batch;
    }

    protected UsedMaterialBatch() {
        // ORM
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof UsedMaterialBatch)) {
            return false;
        }
        final UsedMaterialBatch that = (UsedMaterialBatch) other;

        return hasIdentity(that.id) && meal.sameAs(that.meal) && material.sameAs(that.material)
                && batchNumber.equals(that.batchNumber);
    }

    @Override
    public Long identity() {
        return id;
    }

    public Meal meal() {
        return meal;
    }
}
