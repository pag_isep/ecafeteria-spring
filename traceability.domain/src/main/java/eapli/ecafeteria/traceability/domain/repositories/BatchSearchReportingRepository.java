/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.domain.repositories;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.framework.domain.repositories.ReportingRepository;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@ReportingRepository
public interface BatchSearchReportingRepository {

    /**
     * Reports the bookings of meals that used a certain batch of material.
     *
     * @param material
     * @param batchNumber
     * @return the bookings of meals that used a certain batch of material
     */
    Iterable<Booking> bookingsOfMealsWhichUsedBatch(Material material, BatchNumber batchNumber);

    /**
     * Reports the meals that used a certain material batch.
     *
     *
     * @param material
     * @param batchNumber
     * @return the meals that used a certain material batch
     */
    Iterable<Meal> mealsWhichUsedBatch(Material material, BatchNumber batchNumber);

    /**
     * Reports the users who have consumed a meal that used a certain material batch.
     *
     * @param material
     * @param batchNumber
     * @return the user who have consumed a meal that used a certain material batch
     */
    Iterable<CafeteriaUser> whoConsumedMealsWhichUsedBatch(Material material,
            BatchNumber batchNumber);
}
