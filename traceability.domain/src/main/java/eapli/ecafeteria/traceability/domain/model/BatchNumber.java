/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.domain.model;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;

/**
 * @FIXME javadoc
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Embeddable
@EqualsAndHashCode
public class BatchNumber implements ValueObject {
    private static final long serialVersionUID = 8979764564824385044L;

    @Column(name = "batch_number", length = 25)
    private final String number;

    protected BatchNumber(final String batchNumber) {
        Preconditions.nonEmpty(batchNumber);

        number = batchNumber;
    }

    protected BatchNumber() {
        // ORM
        number = null;
    }

    /**
     * Factory method to create a batch number.
     *
     * @param batchNumber
     * @return a new batch number.
     */
    public static BatchNumber valueOf(final String batchNumber) {
        return new BatchNumber(batchNumber);
    }

    @Override
    public String toString() {
        return number;
    }
}
