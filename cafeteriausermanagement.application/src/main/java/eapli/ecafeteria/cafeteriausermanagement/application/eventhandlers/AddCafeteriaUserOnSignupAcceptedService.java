/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUserBuilder;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.framework.application.ApplicationService;
import eapli.framework.functional.Functions;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@ApplicationService
@Component
/* package */ class AddCafeteriaUserOnSignupAcceptedService {

	private final UserRepository repo;
	private final CafeteriaUserRepository cafeteriaUserRepository;

	@Autowired
	public AddCafeteriaUserOnSignupAcceptedService(UserRepository repo,
			CafeteriaUserRepository cafeteriaUserRepository) {
		this.cafeteriaUserRepository = cafeteriaUserRepository;
		this.repo = repo;
	}

	@Transactional
	public CafeteriaUser addCafeteriaUser(final NewUserRegisteredFromSignupEvent event) {
		return findUser(event).map(u -> {
			final var cafeteriaUser = new CafeteriaUserBuilder().withMecanographicNumber(event.mecanographicNumber())
					.withOrganicUnit(event.organicUnit()).withSystemUser(u).build();

			return cafeteriaUserRepository.save(cafeteriaUser);
		}).orElseThrow(IllegalStateException::new);
	}

	private Optional<SystemUser> findUser(final NewUserRegisteredFromSignupEvent event) {
		// since we are using events, the actual user may not yet be
		// created, so lets give it a time and wait
		return Functions.retry(() -> repo.ofIdentity(event.username()), 500, 30);
	}
}
