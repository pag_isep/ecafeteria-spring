/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.events.SignupAcceptedEvent;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.SignupRequest;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.SignupRequestRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.pubsub.EventPublisher;
import eapli.framework.validations.Preconditions;
import lombok.RequiredArgsConstructor;

/**
 * The eventful controller for the use case "Accept or refuse signup request".
 *
 * <p>
 * This implementation makes use of domain events to (1) follow the rule that
 * one controller should only modify one aggregate, and (2) notify other parts
 * of the system to react accordingly. For an alternative transactional approach
 * see {@link AcceptRefuseSignupRequestControllerTransactionalImpl}.
 *
 *
 * <p>
 * In order to follow an eventful approach, first it is necessary to register
 * the event handlers ("watch dogs")
 * </p>
 * <p>
 * <img src="seq-init.svg"/>
 * </p>
 *
 * <p>
 * Afterwards, during the use case execution, the actual three steps are divided
 * and executed in separate transactions (and eventually threads) according to
 * the publishing of events.
 * </p>
 * <p>
 * <img src="seq-use-case.svg"/>
 * </p>
 *
 * <!--
 *
 * @startuml seq-init.svg
 *
 *           title Register event handlers
 *
 *           participant App
 *           participant dispatcher
 *           participant SignupAcceptedWatchDog
 *
 *           create SignupAcceptedWatchDog
 *           App -> SignupAcceptedWatchDog:new
 *           App -> dispatcher: register(signupWatchDog)
 *
 *           create NewUserRegisteredFromSignupWatchDog
 *           App -> NewUserRegisteredFromSignupWatchDog:new
 *           App -> dispatcher: register(newUserWatchDog)
 *
 * @enduml
 *
 *         -->
 *
 *         <!--
 *
 * @startuml seq-use-case.svg
 *
 *           title Use case execution
 *
 *           participant dispatcher
 *
 *           group Accept the signup
 *           UI -> AcceptRefuseSignupController:acceptSignupRequest
 *           activate AcceptRefuseSignupController
 *           AcceptRefuseSignupController -> SignupRepository:save
 *           create SignupAcceptedEvent
 *           AcceptRefuseSignupController -> SignupAcceptedEvent: new
 *           AcceptRefuseSignupController -> dispatcher:publish
 *           deactivate AcceptRefuseSignupController
 *           end
 *
 *           group Register system user
 *           dispatcher -> SignupAcceptedWatchDog: onEvent
 *           activate SignupAcceptedWatchDog
 *           SignupAcceptedWatchDog -> AddUserOnSignupAcceptedController:addUser
 *           activate AddUserOnSignupAcceptedController
 *           AddUserOnSignupAcceptedController -> UserRepository:save
 *           create NewUserRegisteredFromSignupEvent
 *           AddUserOnSignupAcceptedController -> NewUserRegisteredFromSignupEvent:new
 *           AddUserOnSignupAcceptedController -> dispatcher:publish
 *           deactivate AddUserOnSignupAcceptedController
 *           deactivate SignupAcceptedWatchDog
 *           end
 *
 *           group Create cafeteria user
 *           dispatcher -> NewUserRegisteredFromSignupWatchDog:onEvent
 *           activate NewUserRegisteredFromSignupWatchDog
 *           NewUserRegisteredFromSignupWatchDog ->
 *           AddCafeteriaUserOnSignupAcceptedController:addCafeteriaUser
 *           activate AddCafeteriaUserOnSignupAcceptedController
 *           AddCafeteriaUserOnSignupAcceptedController -> CafeteriaUserRepository:save
 *           deactivate AddCafeteriaUserOnSignupAcceptedController
 *           deactivate NewUserRegisteredFromSignupWatchDog
 *           end
 * @enduml
 *
 *         -->
 *
 * @see AcceptRefuseSignupRequestController
 * @see AcceptRefuseSignupRequestControllerTransactionalImpl
 *
 * @author Paulo Gandra de Sousa
 */
@UseCaseController
@Component
@Profile("UseCaseDemos.Eventful")
public class AcceptRefuseSignupRequestControllerEventfulImpl implements AcceptRefuseSignupRequestController {

    private final SignupRequestRepository signupRequestsRepository;
    private final AuthorizationService authorizationService;
    private final EventPublisher dispatcher;

    /**
     * Constructor.
     * <p>
     * We are using constructor dependency injection to facilitate testing of this controller.
     * <p>
     * This boilerplate code could be avoided by leveraging Lombok's {@link RequiredArgsConstructor}
     *
     * @param signupRequestsRepository
     * @param authorizationService
     * @param dispatcher
     */
    @Autowired
    public AcceptRefuseSignupRequestControllerEventfulImpl(final SignupRequestRepository signupRequestsRepository,
            final AuthorizationService authorizationService, final EventPublisher dispatcher) {
        this.signupRequestsRepository = signupRequestsRepository;
        this.authorizationService = authorizationService;
        this.dispatcher = dispatcher;
    }

    @Override
    @Transactional
    @SuppressWarnings("squid:S1226")
    public SignupRequest acceptSignupRequest(SignupRequest theSignupRequest) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        Preconditions.nonNull(theSignupRequest);

        theSignupRequest = markSignupRequestAsAccepted(theSignupRequest);
        return theSignupRequest;
    }

    /**
     * Modify Signup Request to accepted
     *
     * @param theSignupRequest
     * @return
     * @throws ConcurrencyException
     * @throws IntegrityViolationException
     */
    @SuppressWarnings("squid:S1226")
    private SignupRequest markSignupRequestAsAccepted(SignupRequest theSignupRequest) {
        // do just what is needed in the scope of this use case
        theSignupRequest.accept();
        theSignupRequest = signupRequestsRepository.save(theSignupRequest);

        // notify interested parties (if any)
        final DomainEvent event = new SignupAcceptedEvent(theSignupRequest);
        dispatcher.publish(event);

        return theSignupRequest;
    }

    @Override
    @Transactional
    public SignupRequest refuseSignupRequest(final SignupRequest theSignupRequest) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        Preconditions.nonNull(theSignupRequest);

        theSignupRequest.refuse();
        return signupRequestsRepository.save(theSignupRequest);
    }

    @Override
    public Iterable<SignupRequest> pendingSignupRequests() {
        return signupRequestsRepository.pendingSignupRequests();
    }
}
