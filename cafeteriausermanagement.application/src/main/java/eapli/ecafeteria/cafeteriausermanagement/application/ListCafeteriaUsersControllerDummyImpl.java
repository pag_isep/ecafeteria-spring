/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUserBuilder;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.PasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

/**
 * just an example of a mock controller. the spring application context
 * configures which actual implementation bean to use.
 *
 * @author PAG
 */
@Component
public class ListCafeteriaUsersControllerDummyImpl implements ListCafeteriaUsersController {
	@Autowired
	private PasswordPolicy enforcer;
	@Autowired
	private PasswordEncoder encoder;

	private static final String DUMMY = "dummy";

	@Override
	public Iterable<CafeteriaUser> activeCafeteriaUsers() {
		final List<CafeteriaUser> dummies = new ArrayList<>();
		dummies.add(buildOneDummyUser());
		return dummies;
	}

	private CafeteriaUser buildOneDummyUser() {
		final var userBuilder = new SystemUserBuilder(enforcer, encoder);
		final var sysUser = userBuilder.with(DUMMY, "duMMy1", DUMMY, DUMMY, "a@b.ro").withRoles(CafeteriaRoles.CASHIER)
				.build();
		return new CafeteriaUserBuilder().withOrganicUnit(new OrganicUnit(Designation.valueOf(DUMMY), DUMMY, DUMMY))
				.withMecanographicNumber("DUMMY").withSystemUser(sysUser).build();
	}
}
