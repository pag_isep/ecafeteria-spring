/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUserBuilder;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.SignupRequest;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.SignupRequestRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.UserManagementService;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.validations.Preconditions;

/**
 * The transactional controller for the use case "accept/refuse a signup request".
 * <p>
 * following the guideline that a controller should only change one Aggregate, we shouldn't be
 * changing all these entities here, but should instead use asynchronous events. However in this
 * case we will take advantage of TransactionalContext.
 *
 * @see AcceptRefuseSignupRequestController
 * @see AcceptRefuseSignupRequestControllerEventfulImpl
 *
 * @author AJS on 08/04/2016.
 */
@UseCaseController
@Component
@Profile("UseCaseDemos.Transactional")
public class AcceptRefuseSignupRequestControllerTransactionalImpl implements AcceptRefuseSignupRequestController {

    @Autowired
    private SignupRequestRepository signupRequestsRepository;

    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private UserManagementService userService;

    @Autowired
    private CafeteriaUserRepository cafeteriaUserRepository;

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.application.cafeteriausermanagement.
     * AcceptRefuseSignupRequestController#acceptSignupRequest(eapli.ecafeteria.
     * domain.users.cafeteriausers.SignupRequest)
     */
    @Override
    @Transactional
    public SignupRequest acceptSignupRequest(SignupRequest theSignupRequest) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        Preconditions.nonNull(theSignupRequest);

        final SystemUser newUser = createUnderlyingSystemUser(theSignupRequest);
        createCafeteriaUser(theSignupRequest, newUser);
        theSignupRequest = markSignupRequestAsAccepted(theSignupRequest);

        return theSignupRequest;
    }

    /**
     * Modify Signup Request to accepted.
     *
     * @param theSignupRequest
     * @return
     * @throws ConcurrencyException
     * @throws IntegrityViolationException
     */
    @SuppressWarnings("squid:S1226")
    private SignupRequest markSignupRequestAsAccepted(SignupRequest theSignupRequest) {
        theSignupRequest.accept();
        theSignupRequest = signupRequestsRepository.save(theSignupRequest);
        return theSignupRequest;
    }

    private void createCafeteriaUser(final SignupRequest theSignupRequest,
            final SystemUser newUser) {
        final var cafeteriaUserBuilder = new CafeteriaUserBuilder();
        cafeteriaUserBuilder.withMecanographicNumber(theSignupRequest.mecanographicNumber())
                .withOrganicUnit(theSignupRequest.organicUnit()).withSystemUser(newUser);
        cafeteriaUserRepository.save(cafeteriaUserBuilder.build());
    }

    private SystemUser createUnderlyingSystemUser(final SignupRequest theSignupRequest) {
        final Set<Role> roles = new HashSet<>();
        roles.add(CafeteriaRoles.CAFETERIA_USER);
        return userService.registerUser(theSignupRequest.username(), theSignupRequest.password(),
                theSignupRequest.name(), theSignupRequest.email(), roles);
    }

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.application.cafeteriausermanagement.
     * AcceptRefuseSignupRequestController#refuseSignupRequest(eapli.ecafeteria.
     * domain.users.cafeteriausers.SignupRequest)
     */
    @Override
    @Transactional
    public SignupRequest refuseSignupRequest(final SignupRequest theSignupRequest) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.ADMIN);

        Preconditions.nonNull(theSignupRequest);

        theSignupRequest.refuse();
        return signupRequestsRepository.save(theSignupRequest);
    }

    /*
     * (non-Javadoc)
     *
     * @see eapli.ecafeteria.application.cafeteriausermanagement.
     * AcceptRefuseSignupRequestController#listPendingSignupRequests()
     */
    @Override
    public Iterable<SignupRequest> pendingSignupRequests() {
        return signupRequestsRepository.pendingSignupRequests();
    }
}
