/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CardMovementRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.application.ApplicationService;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.money.domain.model.Money;

/**
 *
 * @author mcn
 */
@Component
@ApplicationService
public class CafeteriaUserService {

    private final AuthorizationService authz;

    private final CafeteriaUserRepository repo;

    private final CardMovementRepository repoMovements;

    @Autowired
    public CafeteriaUserService(final AuthorizationService authz, final CafeteriaUserRepository repo,
            final CardMovementRepository repoMovements) {
        this.authz = authz;
        this.repo = repo;
        this.repoMovements = repoMovements;
    }

    /**
     *
     * @param mecNumber
     * @return
     */
    public Optional<CafeteriaUser> findCafeteriaUserByMecNumber(final String mecNumber) {
        authz.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.ADMIN, CafeteriaRoles.CASHIER);
        return repo.ofIdentity(MecanographicNumber.valueOf(mecNumber));
    }

    public Optional<CafeteriaUser> findCafeteriaUserByUsername(final Username user) {
        authz.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.ADMIN);
        return repo.findByUsername(user);
    }

    public Money balanceOf(final MecanographicNumber number) {
        return repoMovements.balanceOf(number);
    }
}
