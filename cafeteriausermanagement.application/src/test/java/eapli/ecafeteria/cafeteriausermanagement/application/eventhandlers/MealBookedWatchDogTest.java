/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.application.eventhandlers;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eapli.ecafeteria.cafeteriausermanagement.testutil.DummyCafeteriaUsers;
import eapli.ecafeteria.mealbooking.domain.model.BookedEvent;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.money.domain.model.Money;

/**
 * @author Paulo Gandra de Sousa 19/05/2022
 *
 */
class MealBookedWatchDogTest {

    private DebitUserCardService controller;
    private MealBookedWatchDog subject;

    @BeforeEach
    void setUp() {
        controller = mock(DebitUserCardService.class);

        subject = new MealBookedWatchDog(controller);
    }

    @Test
    void ensureWorksOn() {

        final var booking = mock(Booking.class);
        final var event = new BookedEvent(booking);

        final var utente = DummyCafeteriaUsers.dummyCafeteriaUser("abc123", "ou");
        when(booking.user()).thenReturn(utente);

        final Money cost = Money.euros(10);
        when(booking.cost()).thenReturn(cost);

        subject.onEvent(event);

        verify(controller).debitUserCard(eq(utente), eq(cost));
    }

    @Test
    void ensureDoesntWorksOnOtherEvents() {

        final DomainEvent event = new DomainEvent() {

            private static final long serialVersionUID = 1L;

            @Override
            public Calendar occurredAt() {
                return null;
            }

            @Override
            public Calendar registeredAt() {
                return null;
            }

        };

        assertThrows(IllegalArgumentException.class, () -> subject.onEvent(event));
    }
}
