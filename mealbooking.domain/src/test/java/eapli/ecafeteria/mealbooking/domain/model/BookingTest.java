/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.domain.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.model.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;
import eapli.framework.money.domain.model.Money;
import eapli.framework.time.domain.model.DateInterval;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 *
 * @author Paulo Gandra de Sousa 2021.03.23
 *
 */
class BookingTest {
    private final DishType aDishType = new DishType("fish", "fish");
    private final Money aPrice = Money.euros(123);
    private final Dish aDish = new DishBuilder().ofType(aDishType).named(Designation.valueOf("Braised Cod"))
            .costing(aPrice).build();
    private final OrganicUnit aOU = new OrganicUnit(Designation.valueOf("UNIT"), "unit name", "unit desc");
    private final Cafeteria aCafeteria = new Cafeteria(aOU, Designation.valueOf("cafe name"), "cafe desc");
    private final Menu aMenu = new Menu(aCafeteria, "menu desc",
            new DateInterval(CurrentTimeCalendars.now(), CurrentTimeCalendars.now()));
    private final Meal aMeal = new Meal(MealType.LUNCH, CurrentTimeCalendars.now(), aDish, aMenu);
    private final SystemUser aSysUser = new SystemUserBuilder(new NilPasswordPolicy(), new PlainTextEncoder())
            .with("userName", "password", "firstName", "lastName", "email@acme.com").build();
    private final CafeteriaUser aUser = new CafeteriaUser(aSysUser, aOU, MecanographicNumber.valueOf("102030"));

    @Test
    void ensureBookingHasMeal() {
        assertThrows(IllegalArgumentException.class, () -> new Booking(aUser, null));
    }

    @Test
    void ensureBookingHasUser() {
        assertThrows(IllegalArgumentException.class, () -> new Booking(null, aMeal));
    }

    @Test
    void ensureBookingHasToken() {
        final var b = new Booking(aUser, aMeal);
        assertTrue(b.identity() != null);
    }

    @Test
    void ensureCostIsCurrentDishPrice() {
        final var subject = new Booking(aUser, aMeal);

        assertEquals(subject.cost(), aDish.currentPrice());
    }
}
