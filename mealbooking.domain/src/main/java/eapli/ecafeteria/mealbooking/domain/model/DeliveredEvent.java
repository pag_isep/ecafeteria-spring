/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import eapli.framework.infrastructure.authz.domain.model.SystemUser;

/**
 * The "delivered" event in the lifecycle of a booking.
 */
@Entity
public class DeliveredEvent extends AbstractBookingEvent {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    private SystemUser theCashier;

    protected DeliveredEvent() {
        super();
        // for ORM
    }

    /**
     * Constructor.
     *
     * @param booking
     *            the booking that was delivered
     * @param theCashier
     *            The SystemUser that represents the cashier.
     */
    public DeliveredEvent(final Booking booking, final SystemUser theCashier) {
        super(booking);
        this.theCashier = theCashier;
    }

    /**
     *
     * @return
     */
    public SystemUser who() {
        return theCashier;
    }
}
