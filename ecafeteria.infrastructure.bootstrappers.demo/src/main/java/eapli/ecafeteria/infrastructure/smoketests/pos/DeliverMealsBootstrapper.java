/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.pos;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.delivery.application.DeliverAMealController;
import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.delivery.domain.repositories.DeliveryStationWorkSessionRepository;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Designation;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class DeliverMealsBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DeliverMealsBootstrapper.class);

    @Autowired
    private DeliverAMealController controller;
    @Autowired
    private DeliveryStationWorkSessionRepository posRepo;

    private List<BookingToken> bookings;

    public DeliverMealsBootstrapper withBookings(final List<BookingToken> bookings) {
        this.bookings = bookings;
        return this;
    }

    @Override
    public boolean execute() {
        if (bookings == null || bookings.isEmpty()) {
            return true;
        }
        final DeliveryStationWorkSession session = posRepo
                .findBySession(DemoDataConstants.ISEP_CAFE,
                        Designation.valueOf(DemoDataConstants.POS001),
                        DemoDataConstants.DATE_TO_BOOK, DemoDataConstants.MEAL_TYPE_TO_BOOK)
                .orElseThrow(IllegalStateException::new);

        for (final BookingToken token : bookings) {
            try {
                controller.deliver(session, token);
                LOGGER.debug("Delivered booking {}", token);
            } catch (ConcurrencyException | IntegrityViolationException e) {
                LOGGER.error("This should not happen", e);
                return false;
            }
        }
        return true;
    }
}
