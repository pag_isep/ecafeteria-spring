/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.BootstrapperBase;
import eapli.ecafeteria.infrastructure.smoketests.cafeteriauser.BookingBootstrapper;
import eapli.ecafeteria.infrastructure.smoketests.cafeteriauser.CafeteriaUserBootstrapper;
import eapli.ecafeteria.infrastructure.smoketests.pos.DeliverMealsBootstrapper;
import eapli.ecafeteria.infrastructure.smoketests.pos.OpenDeliveryStationWorkSessionBootStrapper;
import eapli.ecafeteria.infrastructure.smoketests.pos.RechargeUserCardBootstrapper;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.framework.actions.Action;

/**
 * Execute a sample end-to-end scenario for faster testing of the application to make sure whenever
 * we start the application we already have a couple of entities in the database.
 *
 * @author Paulo Gandra de Sousa
 */
@Component
@SuppressWarnings("squid:S1126")
public class ECafeteriaSampleEndToEndSmokeTester extends BootstrapperBase {

    @Autowired
    private CafeteriaUserBootstrapper cafeteriaUserBootstrapper;

    @Autowired
    private BookingBootstrapper bookingBootstrapper;

    @Autowired
    private OpenDeliveryStationWorkSessionBootStrapper openDeliveryStationWorkSessionBootStrapper;

    @Autowired
    private RechargeUserCardBootstrapper rechargeUserCardBootstrapper;

    @Autowired
    private DeliverMealsBootstrapper deliverMealsBootstrapper;

    @Override
    public boolean execute() {
        authenticateForBootstrapping();

        if (!sampleEndToEndScenario()) {
            return false;
        }

        // nothing more to do; everything went well
        return true;
    }

    private boolean sampleEndToEndScenario() {
        if (!usersSignupAndApprove()) {
            return false;
        }
        if (!posOpenStationAndChargeCard()) {
            return false;
        }

        final List<BookingToken> bookings = usersBookMeal();
        if (bookings.isEmpty()) {
            return false;
        }

        if (!posDeliver(bookings)) {
            return false;
        }
        return true;
    }

    private boolean usersSignupAndApprove() {
        final Action[] actions = {
                // sample cafeteria users
                cafeteriaUserBootstrapper, };
        return bootstrap(actions);
    }

    private List<BookingToken> usersBookMeal() {
        final Action[] actions = { bookingBootstrapper };
        if (!bootstrap(actions)) {
            return new ArrayList<>();
        } else {
            return bookingBootstrapper.bookings();
        }
    }

    private boolean posOpenStationAndChargeCard() {
        final Action[] actions = {
                // open POS
                openDeliveryStationWorkSessionBootStrapper,
                // recharge a user card
                rechargeUserCardBootstrapper,
        };
        return bootstrap(actions);
    }

    private boolean posDeliver(final List<BookingToken> bookings) {
        final Action[] actions = { deliverMealsBootstrapper.withBookings(bookings) };
        return bootstrap(actions);
    }
}
