/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.dishmanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishRepository;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.framework.actions.Action;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.representations.builders.JsonRepresentationBuilder;
import eapli.framework.strings.PrettyJsonString;
import eapli.framework.strings.PrettyXmlString;

/**
 *
 * @author Paulo Gandra de Sousa 29/04/2020
 *
 */
@Component
public class BuildDishRepresentationSmokeTester implements Action {
	private static final Logger LOGGER = LogManager.getLogger(BuildDishRepresentationSmokeTester.class);

	@Autowired
	private DishRepository repo;

	@Override
	public boolean execute() {
		final var dish = repo.ofIdentity(Designation.valueOf(DemoDataConstants.DISH_NAME_BACALHAU_A_BRAZ))
				.orElseThrow(IllegalStateException::new);

		// build a JSON representation of one dish
		testDishJsonRepresentation(dish);

		// map an object to JSON
		testDishJsonMapping(dish);

		// map an object to XML
		testDishXmlMapping(dish);

		// nothing else to do
		return true;
	}

	/**
	 * Get an XML representation based on JAX-B annotations. This is ideal for
	 * scenarios where there is only one representation needed for all use cases and
	 * we want to take advantage of a library to avoid coding errors.
	 */
	private void testDishXmlMapping(final Dish dish) {
		final var xml = PrettyXmlString.of(dish, Dish.class);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("\n-- DISH Mapping to XML --");
			LOGGER.info(xml.toString());
		}
	}

	/**
	 * Get a JSON representation based on jackson annotations. This is ideal for
	 * scenarios where there is only one representation needed for all use cases and
	 * we wan't to take advantage of a library to avoid coding errors.
	 */
	private void testDishJsonMapping(final Dish dish) {
		final var json = PrettyJsonString.of(dish);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("\n-- DISH Mapping to JSON --");
			LOGGER.info(json.toString());
		}
	}

	/**
	 * Get a JSON representation using the
	 * {@link eapli.framework.representations.Representationable Representationable}
	 * interface. This is ideal for scenarios where there might be the need to get
	 * different representations for different use cases. In such scenarios we
	 * cannot rely on a single DTO or automatic mapping thru Jackson/JAX-B.
	 *
	 * @param dish
	 */
	private void testDishJsonRepresentation(final Dish dish) {
		final var json = dish.buildRepresentation(new JsonRepresentationBuilder());
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("\n-- DISH JSON --");
			LOGGER.info(json);
			LOGGER.info("\n-- DISH Pretty JSON --");
			LOGGER.info(PrettyJsonString.of(json).toString());
		}
	}
}
