/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.cafeteriauser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import eapli.ecafeteria.cafeteriausermanagement.application.AcceptRefuseSignupRequestController;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.SignupRequest;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mycafeteriauser.application.SignupController;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.ecafeteria.orgmanagement.domain.repositories.OrganicUnitRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Designation;

/**
 *
 * @author Paulo Sousa
 */
@Component
public class CafeteriaUserBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(CafeteriaUserBootstrapper.class);

    @Autowired
    private OrganicUnitRepository unitRepo;

    @Autowired
    private SignupController signupController;

    @Autowired
    private AcceptRefuseSignupRequestController acceptController;

    @Override
    public boolean execute() {
        final var unit = unitRepo.ofIdentity(Designation.valueOf(DemoDataConstants.OU_ISEP))
                .orElseThrow(IllegalStateException::new);

        // signup a couple of users and accept them so they can be used immediately in
        // the User app
        signupAndApprove(DemoDataConstants.USER_TEST1, DemoDataConstants.PASSWORD1, "John", "Smith", "john@smith.com",
                unit, DemoDataConstants.USER_TEST1);
        signupAndApprove(DemoDataConstants.USER_TEST2, DemoDataConstants.PASSWORD1, "Mary", "Smith", "mary@smith.com",
                unit, DemoDataConstants.USER_TEST2);

        // make a bunch of signups but leave them for approval so the backoffice user
        // will approve them
        signup("isep111", DemoDataConstants.PASSWORD1, "John", "Smith One", "john1@smith.com", unit, "isep111");
        signup("isep222", DemoDataConstants.PASSWORD1, "Mary", "Smith Two", "mary2@smith.com", unit, "isep222");
        signup("isep333", DemoDataConstants.PASSWORD1, "Mary", "Smith Three", "mary3@smith.com", unit, "isep333");

        return true;
    }

    private SignupRequest signupAndApprove(final String username, final String password, final String firstName,
            final String lastName, final String email, final OrganicUnit organicUnit,
            final String mecanographicNumber) {
        return doSignupAndApprove(username, password, firstName, lastName, email, organicUnit, mecanographicNumber,
                true);
    }

    private SignupRequest signup(final String username, final String password, final String firstName,
            final String lastName, final String email, final OrganicUnit organicUnit,
            final String mecanographicNumber) {
        return doSignupAndApprove(username, password, firstName, lastName, email, organicUnit, mecanographicNumber,
                false);
    }

    private SignupRequest doSignupAndApprove(final String username, final String password, final String firstName,
            final String lastName, final String email, final OrganicUnit organicUnit, final String mecanographicNumber,
            final boolean approve) {
        SignupRequest request = null;
        try {
            request = signupController.signup(username, password, firstName, lastName, email, organicUnit,
                    mecanographicNumber);
            if (approve) {
                acceptController.acceptSignupRequest(request);
            }
            LOGGER.debug(username);
        } catch (final ConcurrencyException | IntegrityViolationException | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", username);
            LOGGER.trace("Assuming existing record", e);
        }
        return request;
    }
}
