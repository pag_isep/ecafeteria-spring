/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.smoketests.cafeteriauser.ConcurrentBookingSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.dishmanagement.AllergenImageSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.dishmanagement.BuildDishRepresentationSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.dishmanagement.DishExportSmokeTester;
import eapli.ecafeteria.infrastructure.smoketests.dishmanagement.DishesAndAllergensSmokeTester;
import eapli.framework.actions.Action;

/**
 * Execute simple smoke tests on the application layer. We are assuming that the
 * bootstrappers mainly test the "register" use cases and some of the "finders"
 * to support those "register", so these smoke tests will focus mainly on
 * executing the other application methods.
 * <p>
 * "smoke testers" make a walkthru of the main use cases of the system so that
 * the system is "smoke tested" in an end-to-end manner.
 *
 * @author Paulo Gandra de Sousa
 */
@Component
public class ECafeteriaDemoSmokeTester implements Action {
    @Autowired
    private DishExportSmokeTester dishExportSmokeTester;

    @Autowired
    private BuildDishRepresentationSmokeTester buildDishRepresentationSmokeTester;

    @Autowired
    private DishesAndAllergensSmokeTester dishesAndAllergensSmokeTester;

    @Autowired
    private AllergenImageSmokeTester allergenImageSmokeTester;

    @Autowired
    private ConnectionPoolingSmokeTester connectionPoolingSmokeTester;

    @Autowired
    private ConcurrentBookingSmokeTester concurrentBookingSmokeTester;

    @Override
    public boolean execute() {
        buildDishRepresentationSmokeTester.execute();
        dishExportSmokeTester.execute();
        dishesAndAllergensSmokeTester.execute();
        allergenImageSmokeTester.execute();
        connectionPoolingSmokeTester.execute();
        concurrentBookingSmokeTester.execute();

        return true;
    }
}
