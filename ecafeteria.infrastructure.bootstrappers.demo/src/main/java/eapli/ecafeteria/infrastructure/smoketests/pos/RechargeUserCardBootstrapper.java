/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.pos;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CardMovementRepository;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.sales.application.RechargeUserCardController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.functional.Functions;
import eapli.framework.money.domain.model.Money;
import eapli.framework.validations.Invariants;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class RechargeUserCardBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(RechargeUserCardBootstrapper.class);

    @Autowired
    private RechargeUserCardController svc;

    @Autowired
    private CardMovementRepository movementRepo;

    @Override
    public boolean execute() {
        var b = recharge(DemoDataConstants.USER_TEST1, 750);
        b &= recharge(DemoDataConstants.USER_TEST2, 750);
        b &= recharge(DemoDataConstants.USER_TEST1, 15);
        b &= recharge(DemoDataConstants.USER_TEST2, 20);
        b &= recharge(DemoDataConstants.USER_TEST1, 30);
        return b;
    }

    private boolean recharge(final String mecNumber, final double amount) {

        try {
            final var u = findCafeteriaUser(mecNumber, svc).orElseThrow(() -> new IllegalStateException(
                    "Cannot recharge the card of user " + mecNumber + " as it does not exist"));

            // get current balance
            final Money currentBalance = movementRepo.balanceOf(u.mecanographicNumber());

            svc.rechargeUserCard(u, amount);
            LOGGER.info("»»» {} @ {}", amount, mecNumber);

            // assert the recharge has been registered
            final Money newBalance = movementRepo.balanceOf(u.mecanographicNumber());
            Invariants.ensure(newBalance.equals(currentBalance.add(Money.euros(amount))),
                    "Balance has not been properly updated when recharging");

        } catch (final IllegalStateException e) {
            LOGGER.error("Cannot recharge the card of user {} as it does not exist", mecNumber);
        } catch (IntegrityViolationException | ConcurrencyException e) {
            LOGGER.error("While recharging {}€ for user {}", amount, mecNumber, e);
            return false;
        }
        return true;
    }

    private Optional<CafeteriaUser> findCafeteriaUser(final String mecNumber, final RechargeUserCardController svc) {
        // since we are using events, the actual cafeteria user may not yet be
        // created, so lets give it a time and wait
        return Functions.retry(() -> svc.findCafeteriaUserByMecNumber(mecNumber), 500, 300);
    }
}
