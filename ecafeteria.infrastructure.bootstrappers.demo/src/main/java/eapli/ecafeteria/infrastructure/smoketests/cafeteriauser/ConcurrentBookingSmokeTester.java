/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.cafeteriauser;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayController;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.framework.actions.Action;
import eapli.framework.actions.TimedActions;

/**
 * Simulate a lot of concurrent users booking meals check how the custom sequence generator for card
 * movements handles it.
 *
 * @author Paulo Gandra Sousa 19/05/20213
 */
@Component
public class ConcurrentBookingSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(ConcurrentBookingSmokeTester.class);

    private static final AtomicInteger COUNT_OF_ERRORS = new AtomicInteger();
    private static final AtomicInteger COUNT_OF_SUCESSFUL = new AtomicInteger();

    /**
     * The repository is hold on a class member variable. so when the run method
     * ends, there is nothing holding the thread from ending and being cleanup by
     * the JVM.
     *
     * @author Paulo Gandra Sousa 19/05/2023
     */
    @Component
    private static class ConcurrentBookingThread implements Runnable {

        @Autowired
        private BookAMealForADayController controller;

        @Autowired
        private MealRepository mealRepo;

        @Autowired
        private CafeteriaUserRepository userRepo;

        @Override
        public void run() {
            try {
                LOGGER.debug("Thread {} booking meal", Thread.currentThread().getName());

                final var meal = mealRepo
                        .findByDayAndType(DemoDataConstants.ISEP_CAFE, DemoDataConstants.DATE_TO_BOOK,
                                DemoDataConstants.MEAL_TYPE_TO_BOOK)
                        .iterator()
                        .next();

                final var number = eapli.framework.math.util.Math.heads() ? DemoDataConstants.USER_TEST1
                        : DemoDataConstants.USER_TEST2;

                final var booker = userRepo.ofIdentity(MecanographicNumber.valueOf(number))
                        .orElseThrow(IllegalStateException::new);
                final var token = controller.bookMeal(meal, booker);

                LOGGER.debug("Thread {} booked : {}", Thread.currentThread().getName(), token);
                COUNT_OF_SUCESSFUL.incrementAndGet();
            } catch (final Exception e) {
                LOGGER.error("...", e);
                COUNT_OF_ERRORS.incrementAndGet();
            }
        }
    }

    @Override
    public boolean execute() {
        final var NTHREADS = 100;

        final var initialThreadCount = initDebugLogging();

        // create threads
        LOGGER.info("Starting {} threads", NTHREADS);
        for (var i = 0; i < NTHREADS; i++) {
            // obtain properly configured bean
            final var bean = Application.getBean(ConcurrentBookingThread.class);
            // create thread
            new Thread(bean).start();
        }
        LOGGER.info("Started {} threads", NTHREADS);

        endDebugLogging(NTHREADS, initialThreadCount);

        return true;
    }

    private void endDebugLogging(final int NTHREADS, final int initialThreadCount) {
        // Let's wait a while and check
        while (NTHREADS > COUNT_OF_ERRORS.get() + COUNT_OF_SUCESSFUL.get()) {
            TimedActions.delay(500);
        }
        LOGGER.info("In the end we had {} sucessful calls and {} errors", COUNT_OF_SUCESSFUL.get(),
                COUNT_OF_ERRORS.get());

        // helper debug - SHOULD NOT BE USED IN PRODUCTION CODE!!!
        if (LOGGER.isDebugEnabled()) {
            final var finalThreadCount = Thread.activeCount();
            LOGGER.debug("Ending thread tester - final thread count: {} (initially were {})", finalThreadCount,
                    initialThreadCount);
            final var t = new Thread[finalThreadCount];
            final var n = Thread.enumerate(t);
            for (var i = 0; i < n; i++) {
                LOGGER.debug("T {}: {}", t[i].getId(), t[i].getName());
            }
        }
    }

    private int initDebugLogging() {
        // helper debug - SHOULD NOT BE USED IN PRODUCTION CODE!!!
        var initialThreadCount = 0;
        if (LOGGER.isDebugEnabled()) {
            initialThreadCount = Thread.activeCount();
            LOGGER.debug("Starting thread tester - initial thread count: {}", initialThreadCount);
        }
        return initialThreadCount;
    }

}
