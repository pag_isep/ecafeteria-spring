/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.ecafeteria.traceability.application.RegisterUsedMaterialController;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.repositories.MaterialRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class UsedMaterialsBootstrapper implements Action {

    @Autowired
    private RegisterUsedMaterialController controller;
    @Autowired
    private MealRepository mealRepo;
    @Autowired
    private MaterialRepository materialRepo;

    private static final Logger LOGGER = LogManager.getLogger(MaterialBootstrapper.class);

    @Override
    public boolean execute() {
        if (!usedAtIsep(DemoDataConstants.DATE_TO_BOOK, DemoDataConstants.MATERIAL_RICE, "LOT-123")) {
            return false;
        }
        return usedAtIsep(DemoDataConstants.DATE_TO_BOOK, DemoDataConstants.MATERIAL_EGGS, "AB-56-Z");
    }

    private boolean usedAtIsep(final Calendar when, final String material, final String batch) {
        final Meal meal = mealRepo.findByDayAndType(DemoDataConstants.ISEP_CAFE, when, DemoDataConstants.MEAL_TYPE_TO_BOOK)
                .iterator().next();
        final Material theMaterial = materialRepo.ofIdentity(material).orElseThrow(IllegalStateException::new);
        try {
            controller.registerUsedMaterial(meal, theMaterial, batch);
            LOGGER.debug("{} used on {} at {}", material, batch, when);
        } catch (IntegrityViolationException | ConcurrencyException e) {
            LOGGER.error("This should not happen", e);
            return false;
        }
        return true;
    }
}
