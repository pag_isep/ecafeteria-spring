/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealmanagement.application.RegisterMenuController;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.repositories.CafeteriaRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.time.util.Calendars;

/**
 *
 * @author losa
 */
@Component
public class MenuBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(MenuBootstrapper.class);

    @Autowired
    private CafeteriaRepository cafeteriaRepo;

    @Autowired
    private RegisterMenuController controller;

    @Override
    public boolean execute() {
        final var cafeteria = cafeteriaRepo.ofIdentity(DemoDataConstants.ISEP_CAFE)
                .orElseThrow(IllegalStateException::new);

        register(cafeteria, "1ª semana abril 2017", Calendars.of(2017, 4, 2),
                Calendars.of(2017, 4, 8));

        return true;
    }

    private void register(final Cafeteria cafeteria, final String description,
            final Calendar beginDay,
            final Calendar endDay) {
        try {
            final Menu m = controller.registerMenu(cafeteria, description, beginDay, endDay);
            LOGGER.debug(m);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            LOGGER.error("This should not happen", e);
        }
    }
}
