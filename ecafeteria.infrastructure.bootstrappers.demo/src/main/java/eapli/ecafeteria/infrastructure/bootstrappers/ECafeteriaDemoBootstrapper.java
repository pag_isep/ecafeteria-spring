/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.BackofficeUsersBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.CafeteriaBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.CookedMealsBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.DeliveryStationBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.DishBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.DishTypesBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.MaterialBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.MealBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.MenuBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.OrganicUnitBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.PlanedMealsBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.backoffice.UsedMaterialsBootstrapper;
import eapli.framework.actions.Action;

/**
 * Create some sample data for faster testing of the application to make sure
 * whenever we start the application we already have a couple of entities in the
 * database.
 *
 * <p>
 * There are two types of bootstrap data:
 * <ul>
 * <li>master data, e.g., dish types,
 * <li>a complete end to end usage scenario
 * </ul>
 */
@Component
@SuppressWarnings("squid:S1126")
public class ECafeteriaDemoBootstrapper extends BootstrapperBase {

	@Autowired
	private DishBootstrapper dishBootstrapper;

	@Autowired
	private MenuBootstrapper menuBootstrapper;

	@Autowired
	private MealBootstrapper mealBootstrapper;

	@Autowired
	private PlanedMealsBootstrapper planedMealsBootstrapper;

	@Autowired
	private UsedMaterialsBootstrapper usedMaterialsBootstrapper;

	@Autowired
	private CookedMealsBootstrapper cookedMealsBootstrapper;

	@Autowired
	private OrganicUnitBootstrapper organicUnitBootstrapper;

	@Autowired
	private CafeteriaBootstrapper cafeteriaBootstrapper;

	@Autowired
	private DeliveryStationBootstrapper deliveryStationBootstrapper;

	@Autowired
	private BackofficeUsersBootstrapper backofficeUsersBootstrapper;

	@Autowired
	private MaterialBootstrapper materialBootstrapper;

	@Autowired
	private DishTypesBootstrapper dishTypesBootstrapper;

	@Override
	public boolean execute() {
		authenticateForBootstrapping();

		if (!bootstrapDemoData()) {
			return false;
		}

		// nothing more to do; everything went well
		return true;
	}

	private boolean bootstrapDemoData() {
		if (!backofficeDemoData() || !backofficeMeals()) {
			return false;
		}
		return true;
	}

	private boolean backofficeMeals() {
		final Action[] actions = {
				// sample dishes
				dishBootstrapper,
				// sample menu
				menuBootstrapper,
				// sample meals
				mealBootstrapper,
				// planned meals
				planedMealsBootstrapper,
				// used materials
				usedMaterialsBootstrapper,
				// cooked meals
				cookedMealsBootstrapper, };
		return bootstrap(actions);
	}

	private boolean backofficeDemoData() {
		final Action[] actions = {
				// O.U.
				organicUnitBootstrapper,
				// CAFETERIAS
				cafeteriaBootstrapper,
				// P.O.S.
				deliveryStationBootstrapper,
				// users
				backofficeUsersBootstrapper,
				// available materials
				materialBootstrapper,
				// available dish types
				dishTypesBootstrapper, };
		return bootstrap(actions);
	}
}
