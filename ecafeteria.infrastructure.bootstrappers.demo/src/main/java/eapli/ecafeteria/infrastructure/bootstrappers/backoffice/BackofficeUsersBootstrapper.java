/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.infrastructure.bootstrappers.MasterDataConstants;
import eapli.ecafeteria.infrastructure.bootstrappers.masterdata.UsersBootstrapperBase;
import eapli.framework.actions.Action;

/**
 * @author Paulo Gandra Sousa
 */
@Component
public class BackofficeUsersBootstrapper extends UsersBootstrapperBase implements Action {

    @SuppressWarnings("squid:S2068")
    private static final String PASSWORD1 = "Password1";

    @Override
    public boolean execute() {
        registerAdmin("admin", MasterDataConstants.PASSWORD1, "Jane", "Doe Admin", "jane.doe@email.local");
        registerCashier("cashier", PASSWORD1, "Johny", "Cash", "johny.doe@emai.l.com");
        registerKitchenManager("kitchen", PASSWORD1, "Oven", "Stove", "Oven.and.stove@emai.l.com");
        registerMenuManager("chef", PASSWORD1, "Master", "Chef", "master.chef@emai.l.com");
        return true;
    }

    private void registerAdmin(final String username, final String password, final String firstName,
            final String lastName, final String email) {
        registerUser(username, password, firstName, lastName, email, CafeteriaRoles.ADMIN);
    }

    private void registerCashier(final String username, final String password, final String firstName,
            final String lastName, final String email) {
        registerUser(username, password, firstName, lastName, email, CafeteriaRoles.CASHIER);
    }

    private void registerKitchenManager(final String username, final String password, final String firstName,
            final String lastName, final String email) {
        registerUser(username, password, firstName, lastName, email, CafeteriaRoles.KITCHEN_MANAGER);
    }

    private void registerMenuManager(final String username, final String password, final String firstName,
            final String lastName, final String email) {
        registerUser(username, password, firstName, lastName, email, CafeteriaRoles.MENU_MANAGER);
    }
}
