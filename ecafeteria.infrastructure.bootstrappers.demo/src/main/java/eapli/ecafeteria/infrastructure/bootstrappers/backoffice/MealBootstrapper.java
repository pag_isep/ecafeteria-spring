/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishRepository;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealmanagement.application.RegisterMealController;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.model.Menu;
import eapli.ecafeteria.mealmanagement.domain.repositories.MenuRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 *
 * @author losa
 */
@Component
public class MealBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(MealBootstrapper.class);

    @Autowired
    private RegisterMealController controller;

    @Autowired
    private DishRepository repo;

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public boolean execute() {
        final Dish bacalhau = repo
                .ofIdentity(Designation.valueOf(DemoDataConstants.DISH_NAME_BACALHAU_A_BRAZ))
                .orElseThrow(IllegalStateException::new);
        final Dish picanha = repo
                .ofIdentity(Designation.valueOf(DemoDataConstants.DISH_NAME_PICANHA))
                .orElseThrow(IllegalStateException::new);
        final Dish lentilhas = repo
                .ofIdentity(Designation.valueOf(DemoDataConstants.DISH_NAME_LENTILHAS_SALTEADAS))
                .orElseThrow(IllegalStateException::new);

        final Calendar afterTomorrow = CurrentTimeCalendars.tomorrow();
        afterTomorrow.add(Calendar.DATE, 1);

        final Menu theMenu = menuRepository.findAll().iterator().next();

        register(MealType.LUNCH, CurrentTimeCalendars.now(), bacalhau, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.now(), bacalhau, theMenu);

        register(MealType.LUNCH, CurrentTimeCalendars.tomorrow(), bacalhau, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.tomorrow(), bacalhau, theMenu);

        register(MealType.LUNCH, afterTomorrow, bacalhau, theMenu);
        register(MealType.DINNER, afterTomorrow, bacalhau, theMenu);

        register(MealType.LUNCH, CurrentTimeCalendars.now(), picanha, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.now(), picanha, theMenu);

        register(MealType.LUNCH, CurrentTimeCalendars.tomorrow(), picanha, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.tomorrow(), picanha, theMenu);

        register(MealType.LUNCH, afterTomorrow, lentilhas, theMenu);
        register(MealType.DINNER, afterTomorrow, lentilhas, theMenu);

        register(MealType.LUNCH, CurrentTimeCalendars.now(), lentilhas, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.now(), lentilhas, theMenu);

        register(MealType.LUNCH, CurrentTimeCalendars.tomorrow(), lentilhas, theMenu);
        register(MealType.DINNER, CurrentTimeCalendars.tomorrow(), lentilhas, theMenu);

        register(MealType.LUNCH, afterTomorrow, picanha, theMenu);
        register(MealType.DINNER, afterTomorrow, picanha, theMenu);

        register(DemoDataConstants.MEAL_TYPE_TO_BOOK, DemoDataConstants.DATE_TO_BOOK, bacalhau, theMenu);

        return true;
    }

    private void register(final MealType mealType, final Calendar ofDay, final Dish dish, final Menu theMenu) {
        try {
            final Meal m = controller.registerMeal(mealType, ofDay, dish, theMenu);
            LOGGER.debug(m);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            LOGGER.error("this should not happen", e);
        }
    }
}
