/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.ecafeteria.mealexecution.application.RegisterCookedMealController;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class CookedMealsBootstrapper implements Action {
	private static final Logger LOGGER = LogManager.getLogger(CookedMealsBootstrapper.class);

	@Autowired
	private RegisterCookedMealController controller;

	@Autowired
	private MealRepository mealRepo;

	@Override
	public boolean execute() {

		return cookedAtIsep(DemoDataConstants.DATE_TO_BOOK, DemoDataConstants.COOKED_QUANTITY);
	}

	private boolean cookedAtIsep(final Calendar when, final int qty) {
		final var i = mealRepo.findByDayAndType(DemoDataConstants.ISEP_CAFE, when, DemoDataConstants.MEAL_TYPE_TO_BOOK)
				.iterator();
		assert i.hasNext();
		final var meal = i.next();
		try {
			controller.registerCookedMeal(meal, qty);
		} catch (IntegrityViolationException | ConcurrencyException e) {
			LOGGER.error("This should not happen", e);
			return false;
		}
		return true;
	}
}
