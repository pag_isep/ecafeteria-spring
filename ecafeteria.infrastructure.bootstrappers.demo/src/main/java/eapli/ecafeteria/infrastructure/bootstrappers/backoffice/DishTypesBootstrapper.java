/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers.backoffice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import eapli.ecafeteria.dishmanagement.application.RegisterDishTypeController;
import eapli.ecafeteria.infrastructure.bootstrappers.DemoDataConstants;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;

/**
 *
 * @author mcn
 */
@Component
public class DishTypesBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishTypesBootstrapper.class);

    @Autowired
    private RegisterDishTypeController controller;

    @Override
    public boolean execute() {
        register(DemoDataConstants.DISH_TYPE_VEGIE, "vegetarian dish");
        register(DemoDataConstants.DISH_TYPE_FISH, "fish dish");
        register(DemoDataConstants.DISH_TYPE_MEAT, "meat dish");
        return true;
    }

    public void register(final String acronym, final String description) {
        try {
            controller.registerDishType(acronym, description);
            LOGGER.debug(acronym);
        } catch (final IntegrityViolationException | ConcurrencyException | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})", acronym,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
        }
    }
}
