/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.model.NutricionalInfo;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.money.domain.model.Money;
import eapli.framework.validations.Preconditions;

/**
 *
 * @author PRP
 */
@UseCaseController
@Component
public class ChangeDishController {
    @Autowired
    private ListDishService svc;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private AuthorizationService authorizationService;

    /**
     *
     * @return
     */
    public Iterable<Dish> allDishes() {
        return svc.allDishes();
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<Dish> searchDish(final String id) {
        return dishRepository.ofIdentity(Designation.valueOf(id));
    }

    /**
     * Updates all the data of the dish. This is a typical use case approach where
     * there is no notion of "user task" and everything is read/updated as in a data
     * grid. In this case, since the UI receives the dish object, the UI will call
     * the update method to set the different values of the dish and then call this
     * controller method.
     *
     * @param dish
     * @return
     */
    @Transactional
    public Dish updateDish(final Dish dish) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);
        if (dish == null) {
            throw new IllegalArgumentException();
        }

        return dishRepository.save(dish);
    }

    /**
     * This is an example of a "task based" approach where individual changes are
     * explicitly ordered by the user instead of following a data grid mentality.
     *
     * @param dish
     * @param newPrice
     * @return
     */
    @Transactional
    public Dish changeDishPrice(final Dish dish, final Money newPrice) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        Preconditions.nonNull(dish);

        dish.changePriceTo(newPrice);
        return dishRepository.save(dish);
    }

    /**
     * This is an example of a "task based" approach where individual changes are
     * explicitly ordered by the user instead of following a data grid mentality.
     *
     * @param dish
     * @param newNutricionalInfo
     * @return
     */
    @Transactional
    public Dish changeDishNutricionalInfo(final Dish dish, final NutricionalInfo newNutricionalInfo) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        Preconditions.nonNull(dish);

        dish.changeNutricionalInfoTo(newNutricionalInfo);
        return dishRepository.save(dish);
    }
}
