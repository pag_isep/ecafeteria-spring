/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.application;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.dishmanagement.domain.model.Allergen;
import eapli.ecafeteria.dishmanagement.domain.model.Dish;
import eapli.ecafeteria.dishmanagement.domain.model.DishBuilder;
import eapli.ecafeteria.dishmanagement.domain.model.DishType;
import eapli.ecafeteria.dishmanagement.domain.model.NutricionalInfo;
import eapli.ecafeteria.dishmanagement.domain.repositories.DishRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.money.domain.model.Money;

/**
 * The controller for the use case "register new dish" using the domain objects.
 *
 * @see eapli.ecafeteria.dishmanagement.application.viadto.RegisterDishViaDTOController
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Component
@UseCaseController
public class RegisterDishController {

    @Autowired
    private ListDishTypeService svc;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private ListAllergenService svcAllg;

    @Autowired
    private AuthorizationService authorizationService;

    @Transactional
    public Dish registerDish(final DishType dishType, final String name, final Integer calories, final Integer salt,
            final double price) {

        return registerDish(dishType, name, calories, salt, price, null);
    }

    @Transactional
    public Dish registerDish(final DishType dishType, final String name, final Integer calories, final Integer salt,
            final double price, final Set<Allergen> allergens) {

        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.MENU_MANAGER);

        final Dish newDish = new DishBuilder().ofType(dishType).named(Designation.valueOf(name))
                .costing(Money.euros(price)).withNutricionalInfo(new NutricionalInfo(calories, salt))
                .withAllergens(allergens).build();

        return dishRepository.save(newDish);
    }

    public Iterable<DishType> dishTypes() {
        return svc.activeDishTypes();
    }

    public Iterable<Allergen> allAllergens() {
        return svcAllg.allAllergens();
    }
}
