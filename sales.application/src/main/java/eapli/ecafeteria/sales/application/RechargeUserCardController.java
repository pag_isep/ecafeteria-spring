/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.sales.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.application.CafeteriaUserService;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.CardMovement;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MovementType;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CardMovementRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.sales.domain.model.CreditRecharge;
import eapli.ecafeteria.sales.domain.repositories.CreditRechargeRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.money.domain.model.Money;

/**
 *
 * @author mcn
 */
@Component
@UseCaseController
public class RechargeUserCardController {

    @Autowired
    private CafeteriaUserService svc;

    @Autowired
    private CreditRechargeRepository rechargeRepo;

    @Autowired
    private CardMovementRepository movementRepo;

    @Autowired
    private AuthorizationService authorizationService;

    /**
     *
     * @param mecNumber
     * @return
     */
    public Optional<CafeteriaUser> findCafeteriaUserByMecNumber(final String mecNumber) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.CASHIER);
        return svc.findCafeteriaUserByMecNumber(mecNumber);
    }

    /**
     * Note: Follows a Transactional approach.
     *
     * @param user
     * @param ammount
     * @return
     */
    @Transactional
    public CardMovement rechargeUserCard(final CafeteriaUser user, final double ammount) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.CASHIER);

        // credit recharge (POS)
        rechargeRepo.save(new CreditRecharge(user, Money.euros(ammount)));

        // credit recharge (cafeteria user's account)
        final var creditMovement = new CardMovement(MovementType.RECHARGE, Money.euros(ammount), user);
        return movementRepo.save(creditMovement);
    }
}
