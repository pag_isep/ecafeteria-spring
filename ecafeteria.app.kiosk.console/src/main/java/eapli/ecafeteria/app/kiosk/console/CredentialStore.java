/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.app.kiosk.console;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.authz.CredentialHandler;
import eapli.framework.infrastructure.authz.domain.model.Role;

/**
 * Credential store to hold in memory the username and password collected during
 * login.
 *
 * @author Paulo Gandra de Sousa 2022.12.15
 */
@Component
@Qualifier("AuthzHandler.Holder")
public class CredentialStore implements CredentialHandler {
    // holder of credential data
    // this is a global variable which is a bad smell
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean authenticate(final String username, final String password, final Role onlyWithThis) {
        this.username = username;
        this.password = password;
        return true;
    }
}
