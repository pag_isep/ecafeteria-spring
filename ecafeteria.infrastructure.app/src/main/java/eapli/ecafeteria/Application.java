/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.spring.SpringContext;
import eapli.framework.util.Utility;

/**
 * A "global" static class with the application registry of well known objects
 *
 * @author Paulo Gandra Sousa
 *
 */
@Utility
public final class Application {
	private static final Logger LOGGER = LogManager.getLogger(Application.class);

	/**
	 * Application version.
	 */
	public static final String VERSION = "V7 (Using Spring as IoC container)";

	/**
	 * Application copyright message.
	 */
	public static final String COPYRIGHT = "(C) 2013-2024, ISEP's Professors of EAPLI";

	private Application() {
		// private visibility to ensure singleton & utility
	}

	/**
	 * Gets a specific component according to the dependency injection
	 * configuration. This method is to be used whenever you don't want/can't use
	 * the {@code @Autowired} annotation.
	 *
	 * @param <T>
	 * @param class1
	 * @return the component
	 */
	public static <T> T getBean(final Class<T> class1) {
		LOGGER.debug("Explicitly loading bean {}", class1.getSimpleName());
		return SpringContext.getBean(class1);
	}
}
