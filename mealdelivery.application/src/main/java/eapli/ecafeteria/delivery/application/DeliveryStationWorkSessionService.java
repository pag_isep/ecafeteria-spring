/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.delivery.application;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.delivery.domain.repositories.DeliveryStationWorkSessionRepository;
import eapli.ecafeteria.deliverystationmanagement.domain.model.DeliveryStation;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.framework.application.ApplicationService;

/**
 *
 * @author arocha
 */
@Component
@ApplicationService
public class DeliveryStationWorkSessionService extends DeliveryBase {

    @Autowired
    private DeliveryStationWorkSessionRepository repo;

    @Transactional
    public DeliveryStationWorkSession openDeliveryStation(final DeliveryStation station, final MealType mealType,
            final Calendar day) {

        return repo.findBySession(station, day, mealType).orElseGet(() -> {
            // no Delivery Station Opened for that day / meal type
            final DeliveryStationWorkSession newDeliveryStationWorkSession = new DeliveryStationWorkSession(station,
                    getChashier(), mealType, day);
            return repo.save(newDeliveryStationWorkSession);
        });
    }
}
