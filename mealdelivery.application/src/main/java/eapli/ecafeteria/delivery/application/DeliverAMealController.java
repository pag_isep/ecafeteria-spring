/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.delivery.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.delivery.domain.model.DeliveryRecord;
import eapli.ecafeteria.delivery.domain.model.DeliveryStationWorkSession;
import eapli.ecafeteria.delivery.domain.repositories.DeliveryRecordRepository;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealbooking.domain.repositories.BookingRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.validations.Preconditions;

/**
 * @author Paulo Gandra de Sousa
 */
@Component
@UseCaseController
public class DeliverAMealController extends DeliveryBase {

    @Autowired
    private BookingRepository bookingRepo;
    @Autowired
    private DeliveryRecordRepository deliveryRepo;

    /**
     * @throws IntegrityViolationException
     * @throws ConcurrencyException
     */
    @Transactional
    public void deliver(final DeliveryStationWorkSession session, final String token) {
        deliver(session, BookingToken.valueOf(token));
    }

    // FIXME this method is changing two aggregates which directly violates a
    // rule of thumb. at least it should have explicit transactional control
    @Transactional
    public void deliver(final DeliveryStationWorkSession session, final BookingToken token) {
        Preconditions.nonNull(token, "Unknown token: " + token);
        Preconditions.nonNull(session, "Unknown session: " + session);

        final Booking booking = bookingRepo.ofIdentity(token)
                .orElseThrow(() -> new IllegalArgumentException("Unknown token " + token));

        final DeliveryRecord delivery = new DeliveryRecord(session, booking);
        deliveryRepo.save(delivery);

        booking.deliver(getChashier());
        bookingRepo.save(booking);
    }
}
