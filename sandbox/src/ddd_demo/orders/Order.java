/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ddd_demo.orders;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntityBase;
import eapli.framework.time.util.CurrentTimeCalendars;
import eapli.framework.validations.Invariants;
import eapli.framework.validations.Preconditions;

/**
 * An order placed by a customer for <i>n</i> items.
 * <p>
 * The typical UI for entering rows of an order is not actually manipulating the
 * order entity - it is manipulating an "order specification" (a DTO or a
 * Builder). Only when the user presses the "create/submit" button the Order
 * object is created based on that specification.
 * </p>
 *
 * @TODO set the order number
 *
 * @author pgsou_000
 *
 *         <!--
 * @startuml simple.svg
 *           class Order
 *           class Customer
 *           Order -> "1" Customer:placedBy
 * @enduml
 *         -->
 */
@SuppressWarnings("unused")
public class Order extends DomainEntityBase<OrderNumber> implements AggregateRoot<OrderNumber> {
    private static final long serialVersionUID = 1L;

    private OrderNumber orderNumber;
    private final Calendar issuedOn;
    private final Customer customer;
    private final Address billingAddress;
    private Address shippingAddress;
    private final Map<SKU, OrderItem> items = new HashMap<>();
    private OrderStatus status;

    /**
     * Internal constructor to create an Order without any items. This is a
     * violation of the domain rules, so this constructor is only available for
     * the sake of the {@link OrderBuilder}.
     *
     * @param customer
     * @param billingAddress
     * @param shippingAddress
     */
    /* package */ Order(final Customer customer, final Address billingAddress,
            final Address shippingAddress) {
        Preconditions.noneNull(customer, billingAddress, shippingAddress);

        issuedOn = CurrentTimeCalendars.now();
        this.customer = customer;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        status = OrderStatus.ORDERED;
    }

    /**
     * Internal method for the sake of OrderBuilder to allow simplified code.
     * According to the domain rules, there is no use case for adding an item to
     * an order. There is a use to add an item to a "shopping basket" or similar
     * but conceptually an Order is created instantly for a set of items and is
     * given the identity (order number) at that time.
     *
     * @param qty
     * @param item
     * @return
     */
    /* package */ Order addItem(final int qty, final Product item) {
        items.merge(item.identity(), new OrderItem(qty, item), (x, y) -> {
            throw new IllegalArgumentException("An item cannot be ordered more than once");
        });
        return this;
    }

    private void addItem(final OrderItem item) {
        items.merge(item.product().identity(), item, (x, y) -> {
            throw new IllegalArgumentException("An item cannot be ordered more than once");
        });
    }

    /**
     * "Regular" constructor of an order. If using this constructor, there is no
     * need to use the builder but the client code needs to build the item list
     * prior to be able to create an order object.
     *
     * @param customer
     * @param billingAddress
     * @param shippingAddress
     * @param items
     */
    public Order(final Customer customer, final Address billingAddress,
            final Address shippingAddress,
            final List<OrderItem> items) {
        this(customer, billingAddress, shippingAddress);
        Preconditions.nonEmpty(items);

        // copy the items to a collection that is managed by this object to keep proper
        // encapsulation. we do not simply assign the reference to the provided collection object.
        // we are not copying each Product as Products are a different aggregate
        items.forEach(this::addItem);
    }

    public void changeShippingAddress(final Address addr) {
        Invariants.ensure(status != OrderStatus.SHIPPED);
        Preconditions.nonNull(addr);

        // keeping a reference to a provided value object is ok since the object is immutable
        shippingAddress = addr;
    }

    @Override
    public boolean sameAs(final Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public OrderNumber identity() {
        // returning a reference to a value object is ok since the returned object is immutable
        return orderNumber;
    }

    public Address shippingAddress() {
        // returning a reference to a value object is ok since the returned object is immutable
        return shippingAddress;
    }

    public Address billingAddress() {
        // returning a reference to a value object is ok since the returned object is immutable
        return billingAddress;
    }

    public Collection<OrderItem> items() {
        // keep proper encapsulation: don't allow the client code to change the collection of itens
        return Collections.unmodifiableCollection(items.values());
    }

    /**
     * The customer has changed her mind and wants to cancel the order.
     */
    public void cancel() {
        Invariants.ensure(status != OrderStatus.SHIPPED);

        // TODO ...

        status = OrderStatus.CANCELED;

        throw new UnsupportedOperationException("not implemented yet");
    }

    /**
     * The customer has changed her mind and wants increase/decrease the
     * quantity of an ordered product. If the quantity is 0, this is equivalent
     * to {@link #disregardItem(SKU)}.
     *
     * @param item
     * @param newQuantity
     */
    public void changeQuantityOfItem(final SKU item, final int newQuantity) {
        Invariants.ensure(status == OrderStatus.BEING_PREPARED || status == OrderStatus.ORDERED);
        Preconditions.ensure(items.containsKey(item));
        Preconditions.nonNegative(newQuantity);

        if (newQuantity == 0) {
            items.remove(item);
        } else {
            final OrderItem old = items.get(item);
            items.put(item, new OrderItem(newQuantity, old.product()));
        }
    }

    /**
     * The customer has changed her mind and wants to remove an item from the
     * order.
     *
     * @param item
     */
    public void disregardItem(final SKU item) {
        Invariants.ensure(status == OrderStatus.BEING_PREPARED || status == OrderStatus.ORDERED);
        Invariants.ensure(items.containsKey(item));

        items.remove(item);
    }

    /**
     * The customer has changed her mind and wants to replace an item by another.
     *
     * @param existing
     * @param substitute
     * @param quantity
     */
    public void replaceItem(final SKU existing, final Product substitute, final int quantity) {
        Invariants.ensure(status == OrderStatus.BEING_PREPARED || status == OrderStatus.ORDERED);
        Preconditions.ensure(items.containsKey(existing));

        final OrderItem item = new OrderItem(quantity, substitute);
        items.remove(existing);
        items.put(substitute.identity(), item);
    }

    /**
     * The customer wants to include an additional item and leverage the same
     * shipping.
     *
     * @param quantity
     * @param item
     */
    public void includeAdditionalItem(final int quantity, final Product item) {
        Invariants.ensure(status == OrderStatus.BEING_PREPARED || status == OrderStatus.ORDERED);

        addItem(quantity, item);
    }

    public Optional<OrderItem> item(final SKU sku) {
        return Optional.ofNullable(items.get(sku));
    }
}
