/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ddd_demo.orders;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntityBase;
import eapli.framework.quantities.domain.model.Money;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@SuppressWarnings("unused")
public class Product extends DomainEntityBase<SKU> implements AggregateRoot<SKU> {
    private static final long serialVersionUID = 1L;

    private final SKU sku;
    private String designation;
    private String description;
    private Money currentPrice;

    public Product(final SKU sku) {
        this.sku = sku;
    }

    @Override
    public boolean sameAs(final Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public SKU identity() {
        return sku;
    }
}
