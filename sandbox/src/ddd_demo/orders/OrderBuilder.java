/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ddd_demo.orders;

import eapli.framework.domain.model.DomainFactory;
import eapli.framework.validations.Invariants;
import eapli.framework.validations.Preconditions;

/**
 * Constructs an order by simplifying the interaction with client code and
 * simulating a partial order. Implementing the builder this way allows the code
 * be simplified for the client (still thinks of "set" mentality) but keeping
 * the object reference as a private part until the object is really completed
 * defined.
 *
 * @author Paulo Gandra de Sousa
 *
 */
public class OrderBuilder implements DomainFactory<Order> {
    private Order theOrder;

    private Customer customer;
    private Address billingAddress;
    private Address shippingAddress;

    public OrderBuilder withCustomer(final Customer c) {
        Invariants.ensure(theOrder == null);
        customer = c;
        return this;
    }

    public OrderBuilder withShippingAddress(final Address addr) {
        if (theOrder == null) {
            shippingAddress = addr;
        } else {
            theOrder.changeShippingAddress(addr);
        }
        return this;
    }

    public OrderBuilder withBillingAddress(final Address addr) {
        Preconditions.ensure(theOrder == null);
        billingAddress = addr;
        return this;
    }

    public OrderBuilder withShippingAndBillingAddress(final Address addr) {
        withShippingAddress(addr);
        withBillingAddress(addr);
        return this;
    }

    private Order getOrder() {
        if (theOrder == null) {
            theOrder = new Order(customer, billingAddress, shippingAddress);
        }
        return theOrder;
    }

    public OrderBuilder withItem(final int quantity, final Product item) {
        Preconditions.noneNull(customer, billingAddress, shippingAddress);
        getOrder().addItem(quantity, item);
        return this;
    }

    /**
     * Returns the internal Order ensuring that it is consistent and this can be
     * exposed to the outside. In this case, since we only create the order
     * object when an item is added we can safely return the object as long as
     * it exists.
     *
     * @return the order
     */
    @Override
    public Order build() {
        Preconditions.nonNull(theOrder);
        return theOrder;
    }
}
