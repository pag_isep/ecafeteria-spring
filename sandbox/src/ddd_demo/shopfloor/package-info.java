/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 *
 * <p>
 * <img src="shopfloor-composition.svg">
 * </p>
 * <p>
 * <img src="shopfloor-specialization.svg">
 * </p>
 * <p>
 * <img src="shopfloor-composite.svg">
 * </p>
 *
 * @author Paulo Gandra de Sousa 27/04/2020
 *
 *         <!--
 * @startuml shopfloor-composition.svg
 *
 *           package Product <<aggregate>> {
 *
 *           class BOMItem <<value object>>{
 *           -quantity
 *           +BOMItem(Product p, Quantity q)
 *           +BOMItem(Material m, Quantity q)
 *           +Optional<Product> product()
 *           +Optional<Material> material()
 *           }
 *
 *           class Product <<entity>><<root>>
 *           class ProductCode <<value object>>
 *
 *           Product -> "1" ProductCode
 *           Product *-down- "1..*" BOMItem:BillOfMaterials
 *           BOMItem -> "1" Material
 *           Product "1" <-- BOMItem
 *
 *           note bottom of BOMItem
 *           BOMItem <b>either</b> has
 *           a reference to Material or Product object.
 *           I.e., one of the references will be null
 *           end note
 *           }
 *
 *           package Material <<aggregate>> {
 *           class Material <<entity>><<root>>
 *           class MaterialCode <<value object>>
 *
 *           Material -> "1" MaterialCode
 *           }
 *
 *           ProductRepository ..> Product
 *           MaterialRepository ..> Material
 *
 * @enduml
 *         -->
 *
 *         * <!--
 * @startuml shopfloor-specialization.svg
 *
 *           package Item <<aggregate>> {
 *
 *           class BOMItem <<value object>> {
 *           -quantity
 *           +BOMItem(Item i, Quantity q)
 *           +Item component()
 *           }
 *
 *           class ItemCode <<value object>>
 *
 *           class Item <<entity>><<root>>
 *           class Product <<entity>>
 *           class Material <<entity>>
 *
 *           Item -> "1" ItemCode
 *           Item <|-- Product
 *           Item <|-- Material
 *
 *           Product *-down- "1..*" BOMItem:BillOfMaterials
 *           BOMItem -> "1" Item
 *           }
 *
 *           Item <.right. ItemRepository
 * 
 * @enduml
 *         -->
 *
 *         <!--
 * @startuml shopfloor-composite.svg
 *
 *           package ProductionSheet <<aggregate>> {
 *
 *           class ProductionSheet <<entity>><<root>>
 *
 *           class ProductCode <<value object>>
 *           class ProductionSheetVersion <<value object>>
 *           class ProductionSheetID <<value object>>
 *
 *           ProductionSheet -> "1" ProductionSheetID
 *           ProductionSheetID -> "1" ProductCode
 *           ProductionSheetID -> "1" ProductionSheetVersion
 *
 *           abstract class BOMItem <<value object>>{
 *           quantity
 *           }
 *
 *           ProductionSheet *-- "1..*" BOMItem:BillOfMaterials
 *
 *           BOMItem <|-down- CompoundBOMItem
 *           BOMItem <|-down- SingleBOMItem
 *
 *           ProductCode "1" <- SingleBOMItem
 *
 *           CompoundBOMItem -up-> "1..*" BOMItem
 *           }
 *
 *
 * @enduml
 *         -->
 */
package ddd_demo.shopfloor;