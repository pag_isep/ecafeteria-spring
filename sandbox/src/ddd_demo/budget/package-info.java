/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * <p>
 * <img src="composite-budget.svg">
 * </p>
 *
 * @author Paulo Gandra de Sousa 02/05/2020
 *
 *         <!--
 * @startuml composite-budget.svg
 *
 *           class BudgetName <<value object>>
 *
 *           class Budget <<root>>{
 *           summary
 *           description
 *           proposedOn
 *           approvedOn
 *           }
 *
 *           Budget -left-> BudgetName
 *
 *           abstract class BudgetItem <<value object>> {
 *           -label
 *           +label()
 *           +amount()
 *           }
 *
 *           class GroupBudgetItem {
 *           +amount()
 *           }
 *
 *           class SimpleBudgetItem {
 *           -amount
 *           amount()
 *           }
 *
 *           Budget *-right- "1..*" BudgetItem
 *
 *           BudgetItem <|-- SimpleBudgetItem
 *           BudgetItem <|-- GroupBudgetItem
 *
 *           GroupBudgetItem *--- "1..*" BudgetItem: items
 *
 *           note left of GroupBudgetItem
 *           Money amount() {
 *           return items.sum();
 *           }
 *           end note
 * @enduml
 *         -->
 */
package ddd_demo.budget;