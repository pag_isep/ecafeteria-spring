/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ddd_demo.person;

import java.time.LocalDate;
import java.time.Period;

/**
 *
 * <img src="associations.svg">
 * <p>
 * <img src="person-aggregate.svg">
 *
 * @author Paulo Gandra de Sousa 21/04/2020
 *
 *         <!--
 *
 * @startuml associations.svg
 *
 *           class Person <<entity>><<root>>
 *           class Name <<value object>>
 *           Person -left-> Name
 *           Person -> "1" Address: livesAt
 *
 *           Person ..> Vehicle:"rides"
 * @enduml
 *
 *         -->
 *
 *
 *         <!--
 *
 * @startuml person-aggregate.svg
 *           package Person <<aggregate>> {
 *
 *           class Person <<Entity>><<root>>{
 *           }
 *           class Period <<value object>>
 *
 *           Person -down-> "1" Period: <<calculated>> age
 *
 *           class LocalDate <<value object>>
 *
 *           LocalDate "1" <-- Person: bornOn
 *           LocalDate "0..1" <-- Person: diedOn
 *
 *           class Name <<value object>>
 *
 *           Person -left-> "1" Name
 *
 *           class Address <<value object>>
 *           Address "1" <-right- Person: livesAt
 *
 *           class Contact <<value object>>
 *           Contact "*" --* Person: contacts
 *           Contact "1" <-- Person:preferedContact
 *
 *           enum ContactType <<value object>>
 *           note right of ContactType: personal, work, other
 *
 *           Contact -> ContactType
 *
 *           class PhoneNumber <<value object>>
 *           class EmailAddress <<value object>>
 *
 *           Contact <|-up- PhoneNumber
 *           Contact <|-up- EmailAddress
 *           }
 * @enduml
 *
 *         -->
 *
 */
@SuppressWarnings("unused")
public class Person {
    private LocalDate dateOfBirth;
    private LocalDate dateOfDeath;

    public Period age() {
        return Period.between(dateOfBirth, LocalDate.now());
    }
}
