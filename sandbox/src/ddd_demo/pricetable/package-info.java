/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * <p>
 * <img src="pricetable.svg">
 * <p>
 *
 * @author Paulo Gandra de Sousa 27/04/2020
 *
 *         <!--
 * @startuml pricetable.svg
 *
 *           class Product <<entity>>
 *           class PriceTable <<entity>> {
 *
 *           }
 *           class Season <<entity>>
 *           class SalesRegion <<entity>>
 *           class PriceTableID <<value object>>
 *
 *           PriceTable -up-> PriceTableID
 *           PriceTable -left-> "1" Season
 *           PriceTable -> "1" SalesRegion
 *           PriceTable -> "1" LocalDateTime:applicableFrom
 *           PriceTable -> "0..1" LocalDateTime:until
 *
 *           class PriceDetail <<value object>>
 *           class Color <<value object>>
 *           class Size <<value object>>
 *
 *           PriceTable *-- "1..*" ProductPriceDetail
 *           ProductPriceDetail -> "1" Product
 *           ProductPriceDetail *-- "1..*" PriceDetail
 *           PriceDetail -left-> "1" Color
 *           PriceDetail -down-> "1" Size
 *           PriceDetail -> "1" Money: retailPrice
 *           PriceDetail -> "1" Money: grossPrice
 *
 * @enduml
 *         -->
 */
package ddd_demo.pricetable;