/**
 *
 */
package sandbox;

/**
 * @author Paulo Gandra de Sousa 02/05/2020
 *
 *         <!--
 * @startuml
 *
 *           class User <<root>> {
 *           -password
 *           -encode()
 *           }
 *
 *           interface PasswordEncoder <<Strategy>>{
 *           encode(string)
 *           }
 *
 *           User ..> PasswordEncoder
 *
 *           PasswordEncoder <|-- MyFancyEncoder
 *
 *           PasswordEncoder <|-- MD5EncoderAdapter
 *           class MyFancyEncoder{
 *           encode()
 *           }
 *
 *           MD5EncoderAdapter ..> MD5Encoder
 *
 *           class MD5EncoderAdapter {
 *           -MD5_OPTIONS
 *           encode(String)
 *           }
 *
 *           note right of MyFancyEncoder
 *           This is an implementation of the strategy
 *
 *           encode(String t) {
 *           // very complex code to encode the string
 *           . . .
 *           // more complex stuff
 *           . . .
 *           }
 *           end note
 *
 *           note left of MD5EncoderAdapter
 *           This is an implementation of the strategy.
 *           Since there is already a library that performs
 *           the required functionality we are just <b>adapting</b> its
 *           interface to conform to the expected Strategy
 *
 *           encode(String txt) {
 *           return MD5Encoder.encode(txt)
 *           }
 *           end note
 *
 *           class MD5Encoder {
 *           {static} encode(String)
 *           }
 * @enduml
 *         -->
 */
public interface Cipher {

}
