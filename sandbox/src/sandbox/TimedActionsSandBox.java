/*
 * Copyright (c) 2013-2020 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sandbox;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Paulo Gandra de Sousa
 *
 */
public class TimedActionsSandBox {

    @FunctionalInterface
    public interface Action {
        /**
         *
         * @return true if this "scope" should end or to signal OK; false
         *         otherwise, e.g., signal an error
         */
        boolean execute();
    }

    private static class RecurringTask extends TimerTask {
        private final Action cmd;

        public RecurringTask(final Action cmd) {
            this.cmd = cmd;
        }

        @Override
        public void run() {
            cmd.execute();
        }
    }

    private static final Timer timer = new Timer();

    /**
     * runs a periodic task each <em>interval</em> miliseconds
     *
     * consider using the method schedule which makes use of Java 5+ concurrent
     * Thread Pool Executors
     *
     * @param cmd
     * @param interval
     */
    public static void periodicaly(final Action cmd, final int interval) {

        timer.scheduleAtFixedRate(new RecurringTask(cmd), 0, interval);
    }

    private static ScheduledExecutorService scheduledThreadPool = Executors
            .newScheduledThreadPool(5);

    /**
     * schedules a periodic action at a specified interval in miliseconds
     *
     * @param cmd
     *            the action to execute
     * @param period
     *            the period in miliseconds
     */
    public static void atFixedRate(final Action cmd, final int period) {
        scheduledThreadPool.scheduleAtFixedRate(cmd::execute, 5000, period, TimeUnit.MILLISECONDS);
    }

    public static void shutdownRecurringTasks() {
        scheduledThreadPool.shutdownNow();
        timer.cancel();
    }

    /**
     * @param args
     */
    @SuppressWarnings("squid:S106")
    public static void execute(final String[] args) {
        atFixedRate(() -> {
            System.out.println("hello");
            return true;
        }, 1000);

        try {
            Thread.sleep(20000);
        } catch (final InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        System.out.println("shutingdown...");
        shutdownRecurringTasks();
    }
}
