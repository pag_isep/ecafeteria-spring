package ddd_demo.orders.domain.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import ddd_demo.orders.Address;
import ddd_demo.orders.Customer;
import ddd_demo.orders.CustomerNumber;
import ddd_demo.orders.Order;
import ddd_demo.orders.OrderBuilder;
import ddd_demo.orders.Product;
import ddd_demo.orders.SKU;

public class OrderTest {
    private static final ddd_demo.orders.SKU SKU2 = new SKU("Y-3-AB");
    private static final SKU SKU = new SKU("X-0-01");
    private OrderBuilder builder;
    private Order subject;

    @Before
    public void setUp() throws Exception {
        builder = new OrderBuilder();
        builder.withCustomer(new Customer(new CustomerNumber("A001")));
        builder.withShippingAndBillingAddress(new Address("Rua", "Cidade"));
        builder.withItem(3, new Product(SKU));
        builder.withItem(5, new Product(SKU2));
        subject = builder.build();
    }

    @Test
    public void ensureCanChangeQuantityOfItem() {
        subject.changeQuantityOfItem(SKU, 7);

        assertEquals(7, subject.item(SKU).orElseThrow(IllegalStateException::new).quantity());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeQuantityOfUnorderedItem() {
        subject.changeQuantityOfItem(new SKU("DUMMY"), 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeQuantityToNegative() {
        subject.changeQuantityOfItem(SKU, -2);
    }

    @Test
    public void ensureChangeQuantityToZeroDisregardsTheItem() {
        subject.changeQuantityOfItem(SKU, 0);

        assertFalse(subject.item(SKU).isPresent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotIncludeAditionalExistingItem() {
        subject.includeAdditionalItem(1, new Product(SKU));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotReplaceUnexistingItem() {
        subject.replaceItem(new SKU("DUMMY"), new Product(SKU), 1);
    }

    @Test
    public void ensureReplaceItem() {
        subject.replaceItem(SKU, new Product(new SKU("DUMMY")), 1);

        assertFalse(subject.item(SKU).isPresent());
        assertTrue(subject.item(new SKU("DUMMY")).isPresent());
        assertEquals(1, subject.item(new SKU("DUMMY")).orElseThrow(IllegalStateException::new).quantity());
    }
}
