package ddd_demo.orders.domain.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import ddd_demo.orders.Address;
import ddd_demo.orders.Customer;
import ddd_demo.orders.CustomerNumber;
import ddd_demo.orders.Order;
import ddd_demo.orders.OrderBuilder;
import ddd_demo.orders.Product;
import ddd_demo.orders.SKU;

public class OrderBuilderTest {
    private OrderBuilder subject;

    @Before
    public void setUp() throws Exception {
        subject = new OrderBuilder();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureJustWithCustomerItIsNotPossibleToHaveAnOrder() {
        subject.withCustomer(new Customer(new CustomerNumber("A001")));
        subject.build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureJustWithAddressItIsNotPossibleToHaveAnOrder() {
        subject.withShippingAndBillingAddress(new Address("Rua", "Cidade"));
        subject.build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureJustWithItemItIsNotPossibleToHaveAnOrder() {
        subject.withItem(1, new Product(new SKU("X-0-01")));
        subject.build();
    }

    private void setUpProperOrder() {
        subject = new OrderBuilder();
        subject.withCustomer(new Customer(new CustomerNumber("A001")));
        subject.withShippingAndBillingAddress(new Address("Rua", "Cidade"));
        subject.withItem(1, new Product(new SKU("X-0-01")));
    }

    @Test
    public void ensureWithAtLeastOneItemItIsPossibleToHaveAnOrder() {
        setUpProperOrder();
        assertTrue(subject.build() != null);
    }

    @Test
    public void ensureWithSameShippingAndBillingAddress() {
        setUpProperOrder();
        final Order order = subject.build();
        assertEquals(order.shippingAddress(), order.billingAddress());
    }

    @Test
    public void ensureWith1Item() {
        setUpProperOrder();
        final Order order = subject.build();
        assertEquals(1, order.items().size());
    }

    @Test
    public void ensureWithItems() {
        setUpProperOrder();
        subject.withItem(1, new Product(new SKU("Y-0-AB")));
        final Order order = subject.build();
        assertEquals(2, order.items().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureTheSameItemCannotBeAddedMoreThanOnce() {
        setUpProperOrder();
        subject.withItem(1, new Product(new SKU("X-0-01")));
        subject.build();
    }
}
