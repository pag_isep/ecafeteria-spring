/**
 *
 */
package eapli.ecafeteria.dishmanagement.domain.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import eapli.framework.money.domain.model.Money;
import eapli.framework.time.util.Calendars;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 * @author Paulo Gandra Sousa
 *
 */
class PriceOnPeriodTest {

    /**
     * Test method for
     * {@link eapli.ecafeteria.dishmanagement.domain.model.PriceOnPeriod#PriceOnPeriod(java.util.Calendar, java.util.Calendar, eapli.framework.general.domain.model.Money)}.
     */
    @Test
    void testPriceOnPeriodCalendarCalendarMoney() {
        assertNotNull(new PriceOnPeriod(CurrentTimeCalendars.yesterday(), CurrentTimeCalendars.now(), Money.euros(1)));
    }

    /**
     * Test method for
     * {@link eapli.ecafeteria.dishmanagement.domain.model.PriceOnPeriod#PriceOnPeriod(java.util.Calendar, eapli.framework.general.domain.model.Money)}.
     */
    @Test
    void testPriceOnPeriodCalendarMoney() {
        assertNotNull(new PriceOnPeriod(CurrentTimeCalendars.yesterday(), Money.euros(1)));
    }

    @Test
    void ensurePriceOnPeriodDoesntAllowNullPrice() {
        final var yesterday = CurrentTimeCalendars.yesterday();
        assertThrows(IllegalArgumentException.class, () -> new PriceOnPeriod(yesterday, null));
    }

    @Test
    void ensurePriceOnPeriodDoesntAllowNullPrice2() {
        final var yesterday = CurrentTimeCalendars.yesterday();
        final var today = CurrentTimeCalendars.now();
        assertThrows(IllegalArgumentException.class, () -> new PriceOnPeriod(yesterday, today, null));
    }

    /**
     * Test method for
     * {@link eapli.ecafeteria.dishmanagement.domain.model.PriceOnPeriod#price()}.
     */
    @Test
    void ensureIncludes() {
        final var instance = new PriceOnPeriod(Calendars.of(2017, 2, 2), Calendars.of(2017, 3, 2),
                Money.euros(1));
        assertTrue(instance.includes(Calendars.of(2017, 2, 15)));
    }

    /**
     * Test method for
     * {@link eapli.ecafeteria.dishmanagement.domain.model.PriceOnPeriod#includes(java.util.Calendar)}.
     */
    @Test
    void ensureNotIncludes() {
        final var instance = new PriceOnPeriod(Calendars.of(2017, 2, 2), Calendars.of(2017, 3, 2),
                Money.euros(1));
        assertFalse(instance.includes(Calendars.of(2017, 4, 25)));
    }
}
