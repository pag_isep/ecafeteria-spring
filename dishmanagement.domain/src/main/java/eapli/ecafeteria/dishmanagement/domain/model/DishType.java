/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.ecafeteria.dishmanagement.dto.DishTypeDTO;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.strings.util.StringPredicates;
import eapli.framework.validations.Preconditions;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.Version;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * A dish type, e.g., vegetarian or fish or meat.
 * <p>
 * This class is implemented in a more traditional way than DDD, by using
 * primitive types for the attributes instead of value objects. This means that
 * some semantic is lost and potential code duplication may rise if the same
 * concept is used as an attribute in more than one class. However, the learning
 * curve is smoother when compared to full DDD.
 *
 * @author MCN 29/03/2016.
 */
@XmlRootElement
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "acronym" }) })
public class DishType implements AggregateRoot<String>, Serializable, DTOable<DishTypeDTO> {

    private static final long serialVersionUID = 1L;

    /**
     * ORM primary key. This is an implementation detail and is never exposed to the outside of the
     * class.
     */
    @Id
    @GeneratedValue
    private Long pk;

    @Version
    private Long version;

    /**
     * Business identity. Needs to be unique, thus we add the unique constraint on the table.
     */
    @XmlElement
    @JsonProperty
    @Column(nullable = false)
    private String acronym;

    @XmlElement
    @JsonProperty
    private String description;

    @XmlElement
    @JsonProperty
    private boolean active;

    protected DishType() {
        // for ORM
    }

    /**
     * DishType constructor.
     *
     * @param name
     *            Mandatory
     * @param description
     *            Mandatory
     */
    public DishType(final String name, final String description) {
        setName(name);
        setDescription(description);
        active = true;
    }

    /**
     * Sets and validates newDescription.
     *
     * @param newDescription
     */
    private void setDescription(final String newDescription) {
        if (!descriptionMeetsMinimumRequirements(newDescription)) {
            throw new IllegalArgumentException("Invalid Description");
        }
        description = newDescription;
    }

    /**
     * Sets and validates newName.
     *
     * @param newName
     *            The new DishType name.
     */
    private void setName(final String newName) {
        if (!nameMeetsMinimumRequirements(newName)) {
            throw new IllegalArgumentException("Invalid Name");
        }
        acronym = newName;
    }

    /**
     * Ensure name is not null or empty.
     *
     * @param name
     *            The name to assess.
     *
     * @return True if name meets minimum requirements. False if name does not meet
     *         minimum requirements.
     */
    private static boolean nameMeetsMinimumRequirements(final String name) {
        return !StringPredicates.isNullOrEmpty(name);
    }

    /**
     * Ensure description is not null or empty.
     *
     * @param description
     *            The description to assess.
     *
     * @return True if description meets minimum requirements. False if description
     *         does not meet minimum requirements.
     */
    private static boolean descriptionMeetsMinimumRequirements(final String description) {
        return !StringPredicates.isNullOrEmpty(description);
    }

    /**
     *
     * @return
     */
    public String description() {
        return description;
    }

    /**
     *
     * @return
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Toggles the state of the dish type, activating it or deactivating it
     * accordingly.
     *
     * @return whether the dish type is active or not
     */
    public boolean toogleState() {
        active = !active;
        return isActive();
    }

    /**
     * Change DishType description
     *
     * @param newDescription
     *            New description.
     */
    public void changeDescriptionTo(final String newDescription) {
        Preconditions.ensure(!StringPredicates.isNullOrWhiteSpace(newDescription));

        description = newDescription;
    }

    @Override
    public String identity() {
        return acronym;
    }

    @Override
    public boolean hasIdentity(final String id) {
        return id.equalsIgnoreCase(acronym);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof DishType)) {
            return false;
        }

        final DishType that = (DishType) other;
        return new EqualsBuilder().append(active, that.active).append(description, that.description)
                .append(acronym, that.acronym).isEquals();
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public String toString() {
        return acronym;
    }

    @Override
    public DishTypeDTO toDTO() {
        return new DishTypeDTO(acronym, description, active);
    }
}
