/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.application;

import org.springframework.beans.factory.annotation.Autowired;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.repositories.BatchSearchReportingRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
class WarnAffectedUsersOfRecalledBatch {

    @Autowired
    private BatchSearchReportingRepository repo;

    public void execute(final Material material, final String lotNumber) {
        final Iterable<CafeteriaUser> affectedUsers = repo.whoConsumedMealsWhichUsedBatch(material,
                BatchNumber.valueOf(lotNumber));
        final String emailTemplate = buildBatchRecallEmailTemplate();
        final EmailService emailSvc = new EmailService();
        for (final CafeteriaUser user : affectedUsers) {
            final String emailText = buildEmailText(emailTemplate, material.identity(), lotNumber,
                    user.identity().toString(), user.user().name().toString());
            emailSvc.send(user.user().email(), "", emailText);
        }
    }

    private String buildEmailText(final String emailTemplate, final String material,
            final String batchNumber, final String mecanographicNumber, final String name) {
        return emailTemplate.replaceAll("\\$\\{name\\}", name)
                .replaceAll("\\$\\{number\\}", mecanographicNumber)
                .replaceAll("\\$\\{lotNumber\\}", batchNumber)
                .replaceAll("\\$\\{material\\}", material);
    }

    private String buildBatchRecallEmailTemplate() {
        final var template = new StringBuilder();
        template.append("Dear ${name} (${number})\n\n")
                .append("One or more of the meals you consumed used the lot #${lotNumber} of ${material} which was recalled by the manufacturer. Please contact your physician urgently.\n\n")
                .append("We regret the inconvient and will act accordingly towards our providers and their quality control.\n")
                .append("Respectfully,\nthe eCafeteria Team.");
        return template.toString();
    }
}
