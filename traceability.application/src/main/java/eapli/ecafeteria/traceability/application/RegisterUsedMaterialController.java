/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealmanagement.application.ListMealService;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.model.UsedMaterialBatch;
import eapli.ecafeteria.traceability.domain.repositories.UsedMaterialBatchRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 *
 * @author pag
 */
@Component
@UseCaseController
public class RegisterUsedMaterialController {

    @Autowired
    private UsedMaterialBatchRepository repository;
    @Autowired
    private ListMealService mealSvc;
    // TODO extract to a new service instead of controller
    @Autowired
    private ListMaterialController materialController;
    @Autowired
    private AuthorizationService authorizationService;

    @Transactional
    public UsedMaterialBatch registerUsedMaterial(final Meal meal, final Material material,
            final String lotNumber) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER,
                CafeteriaRoles.KITCHEN_MANAGER);

        final UsedMaterialBatch lot = new UsedMaterialBatch(meal, material,
                BatchNumber.valueOf(lotNumber));
        return repository.save(lot);
    }

    public Iterable<Meal> meals(final Cafeteria cafeteria) {
        return mealSvc.allMeals(cafeteria);
    }

    public Iterable<Material> materials() {
        return materialController.all();
    }
}
