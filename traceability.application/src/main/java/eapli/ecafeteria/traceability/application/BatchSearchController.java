/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.traceability.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.traceability.domain.model.BatchNumber;
import eapli.ecafeteria.traceability.domain.model.Material;
import eapli.ecafeteria.traceability.domain.model.UsedMaterialBatch;
import eapli.ecafeteria.traceability.domain.repositories.BatchSearchReportingRepository;
import eapli.ecafeteria.traceability.domain.repositories.MaterialRepository;
import eapli.ecafeteria.traceability.domain.repositories.UsedMaterialBatchRepository;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class BatchSearchController {

    @Autowired
    private UsedMaterialBatchRepository usedMaterialRepo;

    @Autowired
    private BatchSearchReportingRepository batchSearch;

    @Autowired
    private MaterialRepository materialRepo;

    public Iterable<Meal> mealsWhichUsedBatch(final Material material, final String lotNumber) {
        return batchSearch.mealsWhichUsedBatch(material, BatchNumber.valueOf(lotNumber));
    }

    public Iterable<UsedMaterialBatch> usedMaterialAndBatch(final Material material,
            final String lotNumber) {
        return usedMaterialRepo.findByMaterialAndBatchNumber(material,
                BatchNumber.valueOf(lotNumber));
    }

    public Iterable<Material> materials() {
        return materialRepo.findAll();
    }

    public void warnAffectedUsers(final Material material, final String batchNumber) {
        new WarnAffectedUsersOfRecalledBatch().execute(material, batchNumber);
    }
}
