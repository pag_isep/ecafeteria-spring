/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.application;

import java.util.Calendar;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;

/**
 * In this case we are defining an interface for the controller in order to afterwards mock a
 * controller during tests.
 *
 * @author Paulo Gandra Sousa 05/06/2020
 *
 */
public interface BookAMealForADayController {

    /**
     *
     * @return the list of cafeterias
     */
    Iterable<Cafeteria> getCafeterias();

    /**
     *
     * @param where
     * @param when
     * @param forMeal
     * @return the meals of a certain type of the specified meal type
     */
    Iterable<Meal> getMealsOfADay(final CafeteriaName where, Calendar when, MealType forMeal);

    /**
     *
     * @param where
     * @param when
     * @param forMeal
     * @return the meals of a certain type of the specified meal type
     */
    Iterable<Meal> getMealsOfADay(final Cafeteria where, Calendar when, MealType forMeal);

    /**
     *
     * @param mealId
     * @param booker
     * @return the booking token confirming the booking
     */
    BookingToken bookMeal(Long mealId, MecanographicNumber booker);

    /**
     *
     * @param meal
     * @param booker
     * @return the booking token confirming the booking
     */
    BookingToken bookMeal(Meal meal, CafeteriaUser booker);
}
