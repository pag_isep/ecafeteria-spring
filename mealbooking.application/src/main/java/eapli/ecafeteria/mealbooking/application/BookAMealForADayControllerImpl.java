/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.application;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.mealmanagement.domain.repositories.MealRepository;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.ecafeteria.orgmanagement.domain.repositories.CafeteriaRepository;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Component
public class BookAMealForADayControllerImpl implements BookAMealForADayController {

    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private CafeteriaRepository cafeteriaRepo;

    @Autowired
    private MealRepository mealRepo;

    @Autowired
    private BookAMealApplicationService bookingService;

    @Autowired
    private CafeteriaUserRepository userRepo;

    @Override
    public Iterable<Cafeteria> getCafeterias() {
        return cafeteriaRepo.findAll();
    }

    @Override
    public Iterable<Meal> getMealsOfADay(final Cafeteria where, final Calendar when,
            final MealType forMeal) {
        return mealRepo.findByDay(where, when, forMeal);
    }

    /**
     *
     * @param mealId
     * @param booker
     * @return the booking token of the newly created booking
     */
    @Override
    @Transactional
    public BookingToken bookMeal(final Long mealId, final MecanographicNumber booker) {
        final Meal meal = mealRepo.ofIdentity(mealId).orElseThrow(IllegalArgumentException::new);
        final CafeteriaUser user = userRepo.ofIdentity(booker).orElseThrow(IllegalArgumentException::new);
        return bookMeal(meal, user);
    }

    @Override
    @Transactional
    public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.POWER_USER, CafeteriaRoles.CAFETERIA_USER);

        return bookingService.bookMeal(meal, booker);
    }

    @Override
    public Iterable<Meal> getMealsOfADay(final CafeteriaName where, final Calendar when, final MealType forMeal) {
        final Cafeteria cafe = cafeteriaRepo.ofIdentity(where).orElseThrow(IllegalArgumentException::new);
        return getMealsOfADay(cafe, when, forMeal);
    }
}
