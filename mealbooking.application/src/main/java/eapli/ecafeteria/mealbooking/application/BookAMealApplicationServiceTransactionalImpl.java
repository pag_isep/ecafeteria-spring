/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.MovementBuilder;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CardMovementRepository;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealbooking.domain.repositories.BookingRepository;
import eapli.ecafeteria.mealbooking.domain.services.BookingService;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.framework.application.ApplicationService;

/**
 * A transactional implementation of the Application service to book a meal.
 *
 * @author Paulo Gandra Sousa 2021/02/23
 *
 */
@ApplicationService
@Component
@Profile("UseCaseDemos.Transactional")
/* package */ class BookAMealApplicationServiceTransactionalImpl implements BookAMealApplicationService {

    @Autowired
    private BookingRepository bookingsRepo;

    @Autowired
    private CardMovementRepository movementsRepo;

    @Autowired
    private BookingService bookingService;

    /**
     *
     * @param meal
     * @param booker
     * @return the booking token of the newly created booking
     * @see MyBookingsController#bookMealForMe(Meal)
     */
    @Override
    @Transactional
    public BookingToken bookMeal(final Meal meal, final CafeteriaUser booker) {
        // register the booking
        var theBooking = bookingService.bookMeal(meal, booker, movementsRepo.balanceOf(booker.identity()));
        theBooking = bookingsRepo.save(theBooking);

        // update movements to reflect new balance
        final var mov = new MovementBuilder().cafeteriaUser(booker).purchases(theBooking.cost());
        movementsRepo.save(mov);

        return theBooking.identity();
    }
}
