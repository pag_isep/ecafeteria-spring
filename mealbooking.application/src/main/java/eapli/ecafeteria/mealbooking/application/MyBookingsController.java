/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mealbooking.application;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.infrastructure.authz.CafeteriaRoles;
import eapli.ecafeteria.mealbooking.domain.model.Booking;
import eapli.ecafeteria.mealbooking.domain.model.BookingToken;
import eapli.ecafeteria.mealbooking.domain.repositories.BookingRepository;
import eapli.ecafeteria.mealmanagement.application.ListMealService;
import eapli.ecafeteria.mealmanagement.domain.model.Meal;
import eapli.ecafeteria.mealmanagement.domain.model.MealType;
import eapli.ecafeteria.orgmanagement.domain.model.Cafeteria;
import eapli.ecafeteria.orgmanagement.domain.model.CafeteriaName;
import eapli.ecafeteria.orgmanagement.domain.repositories.CafeteriaRepository;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
@Component
public class MyBookingsController {

    @Autowired
    private BookingRepository bookingRepo;

    @Autowired
    private CafeteriaUserRepository userRepo;

    @Autowired
    private AuthorizationService authz;

    @Autowired
    private BookAMealApplicationService bookingService;

    @Autowired
    private ListMealService svcMeals;

    @Autowired
    private CafeteriaRepository cafeRepo;

    /**
     * Returns the bookings of the authenticated cafeteria user.
     *
     * @return the bookings of the authenticated cafeteria user
     */
    public Iterable<Booking> myBookings() {
        authz.ensureAuthenticatedUserHasAnyOf(CafeteriaRoles.CAFETERIA_USER);

        return authz.session().map(s -> bookingRepo.findByUsername(s.authenticatedUser().username()))
                .orElseThrow(IllegalStateException::new);
    }

    public Iterable<Meal> allMealsByDateAndType(final CafeteriaName where, final Calendar theDay,
            final MealType mealType) {
        return svcMeals.mealsByDayAndType(where, theDay, mealType);
    }

    /**
     *
     * @param theDay
     * @param mealType
     * @return
     */
    public Optional<Booking> myBookingFor(final Calendar theDay, final MealType mealType) {
        final var user = currentUser().orElseThrow(IllegalStateException::new);
        return bookingRepo.findByUserForADayAndMealType(user, theDay, mealType);
    }

    private Optional<CafeteriaUser> currentUser() {
        return authz.session().flatMap(s -> userRepo.findByUsername(s.authenticatedUser().username()));
    }

    @Transactional
    public BookingToken bookMealForMe(final Meal meal) {
        return bookingService.bookMeal(meal, currentUser().orElseThrow(IllegalStateException::new));
    }

    /**
     * @return
     */
    public Iterable<Cafeteria> cafeterias() {
        return cafeRepo.findAll();
    }
}
