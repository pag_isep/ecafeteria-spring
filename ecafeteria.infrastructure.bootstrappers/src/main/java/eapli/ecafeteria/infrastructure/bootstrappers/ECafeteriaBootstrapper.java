/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.ecafeteria.infrastructure.bootstrappers.masterdata.AllergenBootstrapper;
import eapli.ecafeteria.infrastructure.bootstrappers.masterdata.MasterUsersBootstrapper;
import eapli.framework.actions.Action;

/**
 * Create master/reference data of the application.
 *
 */
@Component
@SuppressWarnings("squid:S1126")
public class ECafeteriaBootstrapper extends BootstrapperBase {

	@Autowired
	private AllergenBootstrapper allergenBootstrapper;

	@Autowired
	private MasterUsersBootstrapper masterUsersBootstrapper;

	@Override
	public boolean execute() {
		if (!createPowerUser()) {
			return false;
		}

		authenticateForBootstrapping();
		if (!bootstrapMasterData()) {
			return false;
		}

		// nothing more to do; everything went well
		return true;
	}

	private boolean createPowerUser() {
		return bootstrap(masterUsersBootstrapper);
	}

	private boolean bootstrapMasterData() {
		if (!applicationMasterData()) {
			return false;
		}

		return true;
	}

	private boolean applicationMasterData() {
		final Action[] masterData = { allergenBootstrapper, };
		return bootstrap(masterData);
	}
}
