/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrappers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import eapli.framework.actions.Action;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.strings.util.Strings;

/**
 * Base class for all bootstrappers.
 *
 * @author Paulo Gandra de Sousa
 *
 */
public abstract class BootstrapperBase implements Action {

    private static final Logger LOGGER = LogManager.getLogger(BootstrapperBase.class);

    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Execute all bootstrapping returning false if any of the bootstrapper actions signals to end the
     * bootstraping.
     *
     * @param actions
     * @return
     */
    protected boolean bootstrap(final Action... actions) {
        boolean ret = true;
        for (final Action boot : actions) {
            LOGGER.info("Bootstrapping {}...", () -> nameOfEntity(boot));
            ret &= boot.execute();
        }
        return ret;
    }

    /**
     * Authenticate a super user to be able to register new users.
     *
     */
    protected void authenticateForBootstrapping() {
        authenticationService.authenticate(MasterDataConstants.POWERUSER,
                MasterDataConstants.PASSWORD1);
    }

    protected String nameOfEntity(final Action boot) {
        final String name = boot.getClass().getSimpleName();
        return Strings.left(name, name.length() - "Bootstrapper".length());
    }
}