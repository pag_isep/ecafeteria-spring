## User Story ##

**Meal Delivery**

As a cashier, I use the meal booking QRCode provided by the user, to mark meal as delivered.

## Analysis ##

2017/04/28 11h00

Q: Which information should be described in the QRCode?
A: An arbitraty token.

Q: Which information should be registered in the meal delivery?
A: Datetime, Local, Cashier

A QRCode or an equivalent code must be presented to the cashier. 

## Business Rules ##
- QRCode must have a token
- Booking can only be delivered if booked
- A Booking that has been delivered can not be delivered again

## Design ##
Booking should have a deliver() method.

Create QRCode class.

Create MealDeliveredInformation with properties:
- datetime
- cashier information
- local

A Booking must have:
* a deliver() method - This method allow the meal to be stated as Delivered

## Test Planning ##
- ensureBookingIsDelivered()
- ensureQRCodeisValid()

## Development and Testing ##

## Integration ##

## TODO ##
There is no way to get the place yet.