/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.ecafeteria.mycafeteriauser.application;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import eapli.ecafeteria.cafeteriausermanagement.domain.model.SignupRequest;
import eapli.ecafeteria.cafeteriausermanagement.domain.model.SignupRequestBuilder;
import eapli.ecafeteria.cafeteriausermanagement.domain.repositories.SignupRequestRepository;
import eapli.ecafeteria.orgmanagement.domain.model.OrganicUnit;
import eapli.ecafeteria.orgmanagement.domain.repositories.OrganicUnitRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.time.util.CurrentTimeCalendars;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Component
@UseCaseController
public class SignupController {

    @Autowired
    private SignupRequestRepository signupRequestRepository;

    @Autowired
    private OrganicUnitRepository organicUnitRepository;

    @Autowired
    private SignupRequestBuilder signupRequestBuilder;

    /**
     *
     * @param username
     * @param password
     * @param firstName
     * @param lastName
     * @param email
     * @param organicUnit
     * @param mecanographicNumber
     * @param createdOn
     * @return
     */
    @Transactional
    public SignupRequest signup(final String username, final String password, final String firstName,
            final String lastName, final String email, final OrganicUnit organicUnit, final String mecanographicNumber,
            final Calendar createdOn) {

        // there is no need for authorisation check in this method as even
        // unauthenticated users may request a signup

        signupRequestBuilder.withUsername(username).withPassword(password).withName(firstName, lastName)
                .withEmail(email).createdOn(createdOn).withOrganicUnit(organicUnit)
                .withMecanographicNumber(mecanographicNumber);

        final var newSignupRequest = signupRequestBuilder.build();
        return signupRequestRepository.save(newSignupRequest);
    }

    @Transactional
    public SignupRequest signup(final String username, final String password, final String firstName,
            final String lastName, final String email, final OrganicUnit organicUnit,
            final String mecanographicNumber) {

        return signup(username, password, firstName, lastName, email, organicUnit, mecanographicNumber,
                CurrentTimeCalendars.now());
    }

    public Iterable<OrganicUnit> organicUnits() {
        return organicUnitRepository.findAll();
    }

    /**
     * Returns the organic unit with the desired ID or an empty optional if there is
     * no organic unit with that ID
     *
     * @param organicUnitID
     * @return the organic unit with the desired ID or an empty optional if there is
     *         no organic unit with that ID
     */
    public Optional<OrganicUnit> findOrganicUnit(final Designation organicUnitID) {
        return organicUnitRepository.ofIdentity(organicUnitID);
    }
}
